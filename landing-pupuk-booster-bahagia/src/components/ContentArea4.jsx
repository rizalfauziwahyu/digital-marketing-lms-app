import React from "react";

const ContentArea4 = () => {
  return (
    <>
      <div class="content-area-l-11-2">
        <div class="container">
          <div class="row align-items-center justify-content-lg-between justify-content-center">
            <div
              class="col-xxl-4 col-xl-5 col-lg-6 col-md-9 order-lg-1 order-1"
              data-aos="fade-right"
              data-aos-duration="800"
              data-aos-once="true"
            >
              <div class="content section-heading-5">
                <h2>
                  Cara Penggunaan
                  Booster Pupuk Bahagia
                  Pada Tanaman Cabai,
                  Terong, Dan Tomat ?
                </h2>
              </div>
            </div>
            <div
              class="col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
              data-aos="fade-left"
              data-aos-duration="800"
              data-aos-once="true"
            >
              <div class="hero-video-l12 position-relative text-center">
                <img
                  src="assets/image/l6/thumbnail3.jpg"
                  alt=""
                  class="w-100"
                />
                <a
                  data-fancybox=""
                  href="https://www.youtube.com/watch?v=g4nYczElpVQ"
                >
                  <div class="d-inline-block video-icon d-inline-block">
                    <i class="fas fa-play align-center"></i>
                  </div>
                </a>
                <div class="video-area-shape-l-12">
                  <img
                    src="assets/image/l6/shape-2.svg"
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-area-l-11-3 position-relative">
        <div class="container">
          <div class="row align-items-center justify-content-center justify-content-lg-start">
            <div
              class="col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
              data-aos="fade-left"
              data-aos-duration="800"
              data-aos-once="true"
            >
              <div class="hero-video-l12 position-relative text-center">
                <img
                  src="assets/image/l6/thumbnail4.jpg"
                  alt=""
                  class="w-100"
                />
                <a
                  data-fancybox=""
                  href="https://www.youtube.com/watch?v=ywqZW-U-yMU"
                >
                  <div class="d-inline-block video-icon d-inline-block">
                    <i class="fas fa-play align-center"></i>
                  </div>
                </a>
                <div class="video-area-shape-l-12">
                  <img
                    src="assets/image/l6/shape-2.svg"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div
              class="offset-xl-1 col-xl-5 col-lg-6 col-md-9 order-lg-1 order-1"
              data-aos="fade-up"
              data-aos-delay="300"
              data-aos-duration="1000"
            >
              <div class="content section-heading-5">
                <h2>
                  Masih ragu dengan
                  Produk Pupuk Booster
                  Bahagia ini ?
                </h2>
                <ul class="list-unstyled pl-0">
                  <li class="d-flex align-items-center">
                    Yuk simak video
                    youtubenya yah,
                    supaya kamu dapat
                    mengerti dan paham
                    akan segudang
                    manfaat yang ada
                    pada Pupuk Booster
                    Bahagia ini.
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContentArea4;
