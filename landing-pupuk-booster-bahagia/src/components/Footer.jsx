import React from 'react'

const Footer = () => {
  return (
    <footer className="footer-area-l6 position-relative">
    <div className="container">
      <div className="row justify-content-center text-center text-lg-start footer-l6-area-items">
        <div className="col-lg-7 col-md-12">
          <div className="footer-content-l6">
            <div className="d-sm-flex align-items-center justify-content-lg-start justify-content-center">
              <div className="logo">
                <img src="assets/image/logo/logobob.png" alt="image" />
              </div>
              <div className="copyright-text">
                <p>© 2023 Bisnis Online Bahagia, All Rights Reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  )
}

export default Footer