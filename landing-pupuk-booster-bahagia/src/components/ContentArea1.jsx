import React from "react";

const ContentArea1 = () => {
  return (
    <div class="content-area-l-11-1">
      <div class="container">
        <div class="row align-items-center justify-content-lg-start justify-content-center">
          <div
            class="col-xl-6 col-md-5 col-md-10"
            data-aos="fade-right"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content-img position-relative z-index-1">
              <img
                src="assets/image/l6/produkbox.jpg"
                alt=""
                class="w-100"
              />
            </div>
          </div>
          <div
            class="offset-xxl-1 col-xxl-5 col-xl-6 col-lg-7 col-md-9"
            data-aos="fade-left"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content section-heading-5">
              <h2>
                Booster Pupuk Tetes
                Ber-Bioteknologi Nomor
                Satu di Dunia
              </h2>
              <p>
                Teknologi Transgenik,
                slah satu hasil
                Bioteknologi dalam
                bentuk tanaman unggul
                yang mempunyai
                produktivitas yang lebih
                baik. <br />
                Bioteknologi dalam
                pembentukan tanaman
                transgenik dapat
                memberikan banyak
                manfaat bagi sektor
                pertanian
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea1;
