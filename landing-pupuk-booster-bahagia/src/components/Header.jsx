import React, {
  useState,
  useEffect,
} from "react";
import { useSearchParams } from "react-router-dom";

const Header = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <header class="site-header site-header--menu-center landing-12-menu site-header--absolute site-header--sticky">
      <div class="container">
        <nav class="navbar site-navbar">
          <div class="brand-logo">
            <a href="#">
              <img
                src="assets/image/logo/logobob.png"
                alt=""
                class="light-version-logo"
              />

              <img
                src="assets/image/logo/logobob.png"
                alt=""
                class="dark-version-logo"
              />
            </a>
          </div>
          <div class="menu-block-wrapper">
            <div class="menu-overlay"></div>
            <nav
              class="menu-block"
              id="append-menu-header"
            ></nav>
          </div>
          {user && (
            <div class="header-btn header-btn-l-12 ms-auto d-none d-xs-inline-flex">
              <a
                target="_blank"
                class="btn btn sign-in-btn focus-reset"
                href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20membeli%20produk%20Booster%20Pupuk%20Bahagia`}
              >
                Beli Sekarang
              </a>
            </div>
          )}
        </nav>
      </div>
    </header>
  );
};

export default Header;
