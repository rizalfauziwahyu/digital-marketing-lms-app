import React from "react";

const ContentArea3 = () => {
  return (
    <div class="content-area-l-11-3 position-relative">
      <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-start">
          <div
            class="col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
            data-aos="fade-left"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="hero-video-l12 position-relative text-center">
              <img
                src="assets/image/l6/thumbnail2.jpg"
                alt=""
                class="w-100"
              />
              <a
                data-fancybox=""
                href="https://www.youtube.com/watch?v=tV0uVkk4IVQ"
              >
                <div class="d-inline-block video-icon d-inline-block">
                  <i class="fas fa-play align-center"></i>
                </div>
              </a>
              <div class="video-area-shape-l-12">
                <img
                  src="assets/image/l6/shape-2.svg"
                  alt=""
                />
              </div>
            </div>
          </div>
          <div
            class="offset-xl-1 col-xl-5 col-lg-6 col-md-9 order-lg-1 order-1"
            data-aos="fade-up"
            data-aos-delay="300"
            data-aos-duration="1000"
          >
            <div class="content section-heading-5">
              <h2>
                Cara Penggunaan Booster
                Pupuk Bahagia Pada
                Tanaman Padi
              </h2>
              <ul class="list-unstyled pl-0">
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Selama air menggenang,
                  tiap pojokan diberikan
                  1 botol ukuran @30ml
                  Per Ha nya (Jadi per
                  sudut 1 botol) <br />{" "}
                  Karena ada 4 pojokan,
                  maka digunakan 4 botol
                  untuk rendaman irigasi
                  (Selama 1 bulan)
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Apabila sudah mulai
                  besar, dilakukan
                  penyemprotan. <br />
                  Disemprot 1 Ha = 30ml
                  booster pupuk Ajaib =
                  1 botol Per minggu
                  disemprot.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  1 Bulan menjelang padi
                  panen, dilakukan
                  penyemprotan lagi,
                  Disemprot 1 Ha = 60ml
                  pupuk Ajaib = 2 botol
                  Per minggu disemprot
                  (Memerlukan 8 botol
                  per bulannya untuk
                  penyemprotan)
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea3;
