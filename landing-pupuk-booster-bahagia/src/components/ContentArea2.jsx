import React from "react";

const ContentArea2 = () => {
  return (
    <div class="content-area-l-11-2">
      <div class="container">
        <div class="row align-items-center justify-content-lg-between justify-content-center">
          <div
            class="col-xxl-4 col-xl-5 col-lg-6 col-md-9 order-lg-1 order-1"
            data-aos="fade-right"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content section-heading-5">
              <h2>
                Manfaat Bioteknologi
                dalam pembentukan
                tanaman transgenik
              </h2>
              <div class="d-flex content-l-11-3-card">
                <img
                  src="assets/image/l6/bioteknologi1.png"
                  alt="icon"
                  style={{
                    width: "100px",
                  }}
                />
                <div class="content-body">
                  <h5>
                    Peningkatan
                    ketahanan tanaman
                    terhadap hama dan
                    penyakit
                  </h5>
                </div>
              </div>
              <div class="d-flex content-l-11-3-card">
                <img
                  src="assets/image/l6/bioteknologi2.png"
                  alt="icon"
                  style={{
                    width: "100px",
                  }}
                />
                <div class="content-body">
                  <h5>
                    Peningkatan hasil
                    pertanian, perubahan
                    sifat tanaman yang
                    diinginkan menjadi
                    lebih baik.
                  </h5>
                </div>
              </div>
            </div>
          </div>
          <div
            class="col-xxl-6 offset-xxl-2 col-xl-6 col-lg-5 col-md-9 order-lg-1 order-0"
            data-aos="fade-left"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content-img">
              <img
                src="assets/image/l6/bioteknologi.jpg"
                alt=""
                class="w-lg-auto w-100"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea2;
