import React, {
  useState,
  useEffect,
} from "react";
import {
  useSearchParams,
} from "react-router-dom";

const CtaArea = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div className="cta-area-l6">
      <div className="container">
        <div className="row">
          <div
            className="col-lg-12"
            data-aos="fade-up"
            data-aos-delay="600"
            data-aos-duration="1000"
          >
            <div
              className="cta-area-l6-bg background-property"
              style={{
                background: `url(assets/image/landing-6/cta-bg.png)`,
              }}
            >
              <div className="row justify-content-center align-items-center">
                <div className="col-xl-7 col-lg-9">
                  <div className="cta-area-l6-content text-center">
                    <h2>
                      Yuk tunggu apa lagi ? Segera dapatkan Pupuk Booster Bahagia ini, Order Sekarang Juga!
                    </h2>
                    {user && (
                      <div className="google-chrome-btn">
                        <a
                          href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20membeli%20produk%20Booster%20Pupuk%20Bahagia`}
                          className="btn btn-style-03 focus-reset"
                        >
                          Beli Sekarang
                        </a>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CtaArea;
