import React from "react";

const Hero = () => {
  return (
    <div class="hero-area-l-12 position-relative z-index-1 overflow-hidden">
      <div class="container">
        <div class="row justify-content-center">
          <div
            class="col-xl-6 col-lg-7 col-md-8 col-sm-12 order-lg-1 order-1"
            data-aos="fade-right"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content">
              <h1>
                Pupuk Booster Bahagia
              </h1>
              <p>
                Booster pupuk Ajaib ini
                berfungsi sebagai
                suplemen untuk tanah dan
                tumbuhan. <br />
                Rata-rata pupuk pada
                umumnya hanya untuk
                memberi makan tanaman
                saja, namun dengan
                booster pupuk Ajaib ini
                memberi makan sampai ke
                tanah. <br />
                Lebih mudah diserap oleh
                akar serabut, seperti
                hara, vitamin dan
                nutrisi tanah.
              </p>
            </div>
          </div>
          <div
            class="col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
            data-aos="fade-left"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="hero-video-l12 position-relative text-center">
              <img
                src="assets/image/l6/thumbnailhero.jpg"
                alt=""
                class="w-100"
              />
              <a
                data-fancybox=""
                href="https://www.youtube.com/watch?v=CW2wWMwKdmY"
              >
                <div class="d-inline-block video-icon d-inline-block">
                  <i class="fas fa-play align-center"></i>
                </div>
              </a>
              <div class="video-area-shape-l-12">
                <img
                  src="assets/image/l6/shape-2.svg"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="hero-shape-l12-1 d-none d-sm-block">
        <img
          src="assets/image/l6/shape-1.png"
          alt=""
        />
      </div>
      <div class="hero-shape-l12-2 d-none d-sm-block">
        <img
          src="assets/image/l6/shape-3.png"
          alt=""
        />
      </div>
    </div>
  );
};

export default Hero;
