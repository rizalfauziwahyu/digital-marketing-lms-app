<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testUserIsLoginSuccessfully()
    {
        $user = User::factory()->create();
        $payload = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $this->json('post', 'api/auth/login', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'user' => [
                    'token',
                    'id',
                    'name',
                    'email',
                    'whatsapp',
                    'status',
                    'created_at',
                    'updated_at'
                ]
            ]);

        $this->assertAuthenticated();
    }

    public function testUserIsRegisteredSuccessfully()
    {
        $payload = [
            'name' => 'usertest',
            'username' => 'usertest',
            'email' => 'usertest@gmail.com',
            'password' => 'password',
            'whatsapp' => '08520999999',
            'status' => 'PLATINUM'
        ];

        $this->json('post', 'api/auth/register', $payload)->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'user' => [
                    'token',
                    'id',
                    'name',
                    'email',
                    'whatsapp',
                    'status',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }
}
