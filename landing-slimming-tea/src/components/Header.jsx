import React, {
  useState,
  useEffect,
} from "react";
import { useSearchParams } from "react-router-dom";

const Header = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <header className="site-header site-header--menu-center landing-6-menu site-header--absolute site-header--sticky">
      <div className="container">
        <nav className="navbar site-navbar">
          <div className="brand-logo">
            <a href="#">
              <img
                src="assets/image/logo/logobob.png"
                alt=""
                className="light-version-logo"
              />
              <img
                src="assets/image/logo/logobob.png"
                alt=""
                className="dark-version-logo"
              />
            </a>
          </div>
          {user && (
            <div className="header-btn l6-header-btn  ms-auto d-none d-xs-inline-flex">
              <a
                target="_blank"
                className="btn btn btn-style-03 focus-reset"
                href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20membeli%20produk%20Slimming%20Tea%20Bahagia`}
              >
                Beli Sekarang!
              </a>
            </div>
          )}

          <div className="mobile-menu-trigger">
            <span></span>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
