import React from "react";

const Hero = () => {
    return (
        <div className="hero-area-l6 ">
            <div className="container">
                <div className="row justify-content-center">
                    <div
                        className="col-xxl-5 col-xl-6 col-lg-7 col-md-10 order-lg-1 order-1"
                        data-aos="fade-right"
                        data-aos-delay="300"
                        data-aos-duration="1000"
                    >
                        <div className="content-herb-tea" >
                            <h1>PRODUK ANTIOKSIDAN <br /> Herb Slimming Tea</h1>

                            <p>
                            Temukan kebahagiaan dalam bentuk tubuh sehat dan ramping dengan Herb Slimming Tea, teh herbal yang mengandung bahan alami untuk membantu menurunkan berat badan.
                            </p>
                        </div>
                    </div>
                    <div
                        className="offset-xxl-1 col-xxl-6 col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
                        data-aos="fade-left"
                        data-aos-delay="300"
                        data-aos-duration="1000"
                    >
                        <div className="mt-4" >
                            <img
                                src="assets/image/landing-6/photo-hero2.jpeg"
                                className="w-100 m-2"
                                style={{
                                    borderRadius: `30px`,
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Hero;
