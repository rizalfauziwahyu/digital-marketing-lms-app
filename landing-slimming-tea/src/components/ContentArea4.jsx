import React from "react";

const ContentArea4 = () => {
    return (
        <div className="content-area-l6-1">
            <div className="container">
                <div className="row align-items-center justify-content-center justify-content-lg-start">
                    <div
                        className="offset-xxl-1 col-xxl-4 col-xl-5 col-lg-5 col-md-8 order-2 order-lg-1"
                        data-aos="fade-right"
                        data-aos-delay="500"
                        data-aos-duration="1000"
                    >
                        <div className="content-box-l6-1 section__heading-3 text-lg-start text-center">
                            <h2 className="">
                                Green Tea
                            </h2>
                            <p>
                                Green tea telah lama dikenal sebagai minuman
                                yang kaya akan antioksidan dan memiliki beragam
                                manfaat bagi kesehatan, termasuk membantu
                                menurunkan berat badan. Kandungan katekin dalam
                                green tea, terutama epigallocatechin gallate
                                (EGCG), diyakini dapat meningkatkan metabolisme
                                dan membantu membakar lemak. Selain itu, green
                                tea juga mengandung kafein, yang dapat
                                meningkatkan energi dan mengurangi nafsu makan.{" "}
                                <br /> <br />
                                Beberapa penelitian menunjukkan bahwa
                                mengonsumsi green tea secara teratur dapat
                                membantu menurunkan berat badan dan lemak tubuh.
                                Sebuah studi tahun 2008 yang diterbitkan dalam
                                jurnal "The American Journal of Clinical
                                Nutrition" menemukan bahwa mengonsumsi green tea
                                yang mengandung EGCG dan kafein secara teratur
                                dapat meningkatkan pembakaran kalori dan
                                membantu menurunkan berat badan.
                            </p>
                        </div>
                    </div>
                    <div
                        className="offset-xl-1 col-xl-5 offset-lg-1 col-lg-5 col-md-9 order-1 order-lg-2"
                        data-aos="fade-left"
                        data-aos-delay="700"
                        data-aos-duration="1000"
                    >
                        <div className="content-image-group-l6-1 position-relative text-center">
                            <div className="bg-image">
                                <img
                                    src="assets/image/landing-6/content-1-bg.png"
                                    alt="image"
                                />
                            </div>
                            <div className="main-image-group">
                                <div className="image-1">
                                    <img
                                        src="assets/image/landing-6/model3.jpeg"
                                        alt="image"
                                        style={{
                                            borderRadius: `30px`,
                                            maxWidth: "400px",
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContentArea4;
