import React from "react";

const ContentArea1 = () => {
    return (
        <div className="content-area-l6-1">
            <div className="container">
                <div className="row align-items-center justify-content-center justify-content-lg-start">
                    <div
                        className="offset-xxl-1 col-xxl-4 col-xl-5 col-lg-5 col-md-8 order-2 order-lg-1"
                        data-aos="fade-right"
                        data-aos-delay="500"
                        data-aos-duration="1000"
                    >
                        <div className="content-box-l6-1 section__heading-3 text-lg-start text-center">
                            <h3 className="">
                                <br /><br />
                                Raih tubuh impianmu dengan Slimming Tea Bahagia,
                                teh herbal yang efektif menurunkan berat badan
                                tanpa efek samping.
                            </h3>
                            <p>
                                Setiap Produk Slimming Tea Bahagia Mengandung : <br />
                                <strong>Jati Belanda</strong> <br />
                                <strong>Jati Cina</strong> <br />
                                <strong>Green Tea</strong> <br />
                                <strong>Jeruk Purut</strong> <br />
                            </p>
                        </div>
                    </div>
                    <div
                        className="offset-xl-1 col-xl-5 offset-lg-1 col-lg-5 col-md-9 order-1 order-lg-2"
                        data-aos="fade-left"
                        data-aos-delay="700"
                        data-aos-duration="1000"
                    >
                        <div className="content-image-group-l6-1 position-relative text-center">
                            <div className="bg-image">
                                <img
                                    src="assets/image/landing-6/content-1-bg.png"
                                    alt="image"
                                />
                            </div>
                            <div className="main-image-group">
                                <div className="image-1">
                                    <img
                                        src="assets/image/landing-6/photo-hero.jpeg"
                                        alt="image"
                                        style={{
                                            borderRadius: `30px`,
                                            maxWidth: '400px',
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContentArea1;
