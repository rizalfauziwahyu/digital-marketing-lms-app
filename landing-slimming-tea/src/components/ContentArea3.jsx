import React from "react";

const ContentArea3 = () => {
    return (
        <div className="content-area-l6-3">
            <div className="container">
                <div className="row align-items-center justify-content-center justify-content-lg-start">
                    <div
                        className="offset-xxl-1 col-xxl-4 col-xl-5 col-lg-5 col-md-8 order-2 order-lg-1"
                        data-aos="fade-right"
                        data-aos-delay="500"
                        data-aos-duration="1000"
                    >
                        <div className="content-box-l6-3 section__heading-3 text-lg-start text-center">
                            <h2>Daun Jati Cina</h2>
                            <p>
                                Daun jati cina (Guava leaves) merupakan bahan
                                alami yang telah digunakan sebagai obat
                                tradisional di berbagai negara, termasuk
                                Indonesia. Selain itu, daun jati cina juga
                                diketahui memiliki manfaat untuk membantu
                                program diet. <br /><br />
                                Beberapa penelitian menunjukkan bahwa daun jati
                                cina mengandung senyawa antioksidan dan
                                anti-inflamasi yang dapat membantu meningkatkan
                                metabolisme tubuh dan mengurangi peradangan pada
                                tubuh. Hal ini dapat membantu meningkatkan
                                pembakaran kalori dan menurunkan berat badan.
                                <br /><br />
                                Selain itu, daun jati cina juga dapat membantu
                                mengurangi kadar gula darah dan meningkatkan
                                sensitivitas insulin. Hal ini dapat membantu
                                mengurangi nafsu makan dan mempercepat
                                pembakaran lemak di dalam tubuh.
                            </p>
                            <p></p>
                        </div>
                    </div>
                    <div
                        className="offset-xl-1 col-xl-5 offset-lg-1 col-lg-5 col-md-9 order-1 order-lg-2"
                        data-aos="fade-left"
                        data-aos-delay="700"
                        data-aos-duration="1000"
                    >
                        <div className="content-image-group-l6-3 position-relative text-center">
                            <div className="bg-image">
                                <img
                                    src="assets/image/landing-6/content-2-bg.png"
                                    alt="image"
                                />
                            </div>
                            <div className="main-image-group">
                                <div className="image-1">
                                    <img
                                        src="assets/image/landing-6/daun-jati-cina.jpg"
                                        alt="image"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContentArea3;
