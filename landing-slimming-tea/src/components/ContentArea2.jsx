import React from "react";

const ContentArea2 = () => {
    return (
        <div className="content-area-l6-2">
            <div className="container">
                <div className="row align-items-center justify-content-center justify-content-lg-start">
                    <div
                        className="offset-xl-1 col-xl-5 offset-lg-1 col-lg-5 col-md-9"
                        data-aos="fade-right"
                        data-aos-delay="500"
                        data-aos-duration="1000"
                    >
                        <div className="content-image-group-l6-2 position-relative text-center">
                            <div className="bg-image">
                                <img
                                    src="assets/image/landing-6/content-3-bg.png"
                                    alt="image"
                                />
                            </div>
                            <div className="main-image-group">
                                <div className="image-1 d-none d-sm-block">

                                </div>
                                <div className="image-2">
                                    <img
                                        src="assets/image/landing-6/daun-jati-belanda.jpg"
                                        alt="image"
                                    />
                                </div>
                                <div className="image-3 d-none d-sm-block">
                                    <img
                                        src="assets/image/landing-6/model-2.jpeg"
                                        alt="image"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        className="offset-xxl-1 col-xxl-4 offset-xl-1 col-xl-5 col-lg-5 col-md-8"
                        data-aos="fade-left"
                        data-aos-delay="700"
                        data-aos-duration="1000"
                    >
                        <div className="content-box-l6-2 section__heading-3 text-lg-start text-center">
                            <h2>Manfaat Daun Jati Belanda</h2>
                            <p>
                                Daun jati belanda telah lama dikenal memiliki
                                banyak manfaat bagi kesehatan dan kecantikan.
                                Kandungan senyawa alami dalam daun jati belanda
                                seperti flavonoid, asam fenolat, dan asam kafeat
                                telah terbukti memiliki sifat antioksidan dan
                                anti-inflamasi yang tinggi. Hal ini membuat daun
                                jati belanda menjadi salah satu tanaman obat
                                herbal yang cukup populer dalam pengobatan
                                tradisional. <br />
                                <br />
                                Daun jati belanda juga dapat digunakan sebagai
                                bahan alami untuk mengurangi kadar gula darah
                                pada penderita diabetes. Kandungan senyawa aktif
                                dalam daun jati belanda dapat membantu mengatur
                                kadar gula darah dengan cara meningkatkan
                                produksi insulin dan menurunkan resistensi
                                insulin.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContentArea2;
