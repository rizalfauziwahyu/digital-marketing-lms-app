import React, {
  useState,
  useEffect,
} from "react";
import { useSearchParams } from "react-router-dom";

const ContentArea3 = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div class="content-area-l10-3">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div
            class="col-lg-5 col-md-8"
            data-aos="fade-up"
            data-aos-delay="300"
            data-aos-duration="1000"
          >
            <div class="content-img">
              <img
                src="assets/image/l1/photo4.jpg"
                alt=""
                class="w-100"
                style={{
                  borderRadius: `30px`,
                }}
              />
            </div>
          </div>
          <div
            class="offset-xl-1 col-xl-5 col-lg-7 col-md-9"
            data-aos="fade-up"
            data-aos-delay="500"
            data-aos-duration="1000"
          >
            <div class="content section-heading-4">
              <h2>
                Sudahkah kalian mencoba
                UFace?
              </h2>
              <p>
                Dengan menggunakan UFace
                secara teratur, kita
                dapat merasakan
                perbedaan yang
                signifikan pada kulit
                wajah kita. Kulit wajah
                kita akan terlihat lebih
                segar, lebih cerah, dan
                lebih sehat. Jadi,
                jangan ragu untuk
                mencoba UFace dan
                rasakan manfaatnya pada
                kulit wajah kita.
              </p>
              {user && (
                <div class="btn-area">
                  <a
                    href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20membeli%20produk%20UFace%20Skincare`}
                    class="btn focus-reset"
                  >
                    Beli Sekarang
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea3;
