import React from "react";
import ReactPlayer from "react-player";

const ContentArea1 = () => {
  return (
    <div class="content-area-l10-1">
      <div class="container">
        <div class="row align-items-center justify-content-lg-start justify-content-center">
          <div
            class="col-lg-5 col-md-8"
            data-aos="fade-up"
            data-aos-delay="300"
            data-aos-duration="1000"
          >
            <div class="content-img pr-9 pl-13">
              <img
                src="assets/image/l1/photo2.jpg"
                alt=""
                class="w-100"
                style={{
                  borderRadius: `30px`,
                }}
              />
            </div>
          </div>
          <div
            class="offset-xxl-1 col-xxl-4 offset-xl-1 col-xl-5 col-lg-7 col-md-9"
            data-aos="fade-up"
            data-aos-delay="500"
            data-aos-duration="1000"
          >
            <div class="content section-heading-4">
              <h2>
                Kenapa Harus UFace Skincare ?
              </h2>
              <p>
                UFace Skincare dengan
                Probiotik dan Prebiotik
                adalah produk terbaik
                untuk merawat kulit
                wajahmu. Probiotik
                adalah mikroorganisme
                baik yang dapat membantu
                mengembalikan
                keseimbangan bakteri
                pada kulit wajah dan
                meningkatkan pertahanan
                kulit terhadap bakteri
                jahat. Sementara itu,
                Prebiotik adalah nutrisi
                yang berguna untuk
                menjaga kestabilan
                lingkungan mikroba pada
                kulit wajah, sehingga
                kulitmu tetap terjaga
                kesehatannya.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row justify-content-center">
          <ReactPlayer
            url="https://www.youtube.com/watch?v=LdVHRwfBumI"
            width="100%"
            controls={true}
          />
        </div>
      </div>
    </div>
  );
};

export default ContentArea1;
