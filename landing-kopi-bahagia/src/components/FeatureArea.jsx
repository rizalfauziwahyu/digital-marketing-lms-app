import React from "react";

const FeatureArea = () => {
  return (
    <div class="feature-area-l0 position-relative">
      <div class="shape-bottom-right w-100 z-index-1 ">
        <img
          src="assets/image/l1/l1-featrues-bg-shape.svg"
          alt=""
          class="w-100"
        />
      </div>
      <div class="container">
        <div class="row">
          <div class="section-heading-4 col-md-12 text-center">
            <h2>
              Kandungan UFace Skincare
            </h2>
          </div>
        </div>
        <div className="row feature-area-l-17-items justify-content-center text-center">
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <h3>
                Prebiotik & Probiotik
              </h3>
              <p>
                Probiotik prebiotik
                adalah salah satu
                kandungan utama UFace
                yang dapat membantu
                menjaga kesehatan kulit
                kita dari dalam dan
                luar. Probiotik
                berfungsi menjaga
                keseimbangan bakteri
                baik dan buruk pada
                kulit wajah, sehingga
                kulit tampak lebih sehat
                dan cerah. Sedangkan
                prebiotik mengandung
                nutrisi yang dapat
                memperkuat sistem
                kekebalan kulit dan
                menjaga kesehatan kulit
                kita dari dalam.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <h3>
                Dimethylmethoxy
                Chromanyl Palmitate
              </h3>
              <p>
                Senyawa yang terbukti
                efektif untuk
                mencerahkan kulit wajah
                dan mengurangi tampilan
                bintik-bintik hitam
                serta noda bekas
                jerawat. UFace hadir
                untuk memberikan solusi
                bagi kamu yang ingin
                memiliki wajah yang
                cerah dan bersinar
                sepanjang hari.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
              <h4>
                Hydrolyzed Pea Protein
              </h4>
              <p>
                Bahan alami yang berasal
                dari kacang polong.
                Bahan ini memiliki sifat
                yang mampu memberikan
                kelembapan dan kehalusan
                pada kulit wajah. Selain
                itu, kandungan ini juga
                mampu membantu
                merangsang produksi
                kolagen yang sangat
                penting untuk menjaga
                elastisitas kulit.
              </p>
            </div>
          </div>
        </div>
        <div className="row feature-area-l-17-items justify-content-center text-center">
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <h3>
                Acetyl Hexapeptide
              </h3>
              <p>
                salah satu jenis peptida
                yang bekerja dengan cara
                memblokir sinyal
                neurotrasmitter yang
                memicu otot wajah untuk
                berkontraksi. Dengan
                begitu, kulit wajah kita
                tidak akan terlihat
                kusam dan keriput karena
                kontraksi otot yang
                berlebihan. Selain itu,
                Acetyl Hexapeptide juga
                dapat membantu
                meningkatkan produksi
                kolagen dan elastin pada
                kulit wajah.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <h3>Nonapeptide</h3>
              <p>
                Peptida yang terdiri
                dari sembilan asam
                amino. Kandungan ini
                mampu melindungi kulit
                dari stres oksidatif,
                yang disebabkan oleh
                polusi dan sinar UV yang
                merusak kulit kita.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
              <h4>Plankton Extract</h4>
              <p>
                Bahan alami yang sangat
                berguna untuk merawat
                kulit. Kandungannya yang
                kaya akan nutrisi dan
                zat antioksidan dapat
                membantu kulitmu dalam
                memperbaiki sel-sel
                kulit yang rusak dan
                meredakan peradangan
                pada kulitmu. Selain
                itu, Plankton Extract
                juga dikenal sebagai
                bahan yang dapat
                membantu mencerahkan
                kulit dan mengurangi
                tanda-tanda penuaan.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureArea;
