import React from "react";
import ReactPlayer from "react-player";

const Hero = () => {
  return (
    <div>
      <div className="hero-area-l0 position-relative">
        <div className="container">
          <ReactPlayer
            url="https://www.youtube.com/watch?v=kda0Ofnxl6Q"
            width="100%"
            controls={true}
          />
        </div>
      </div>
      <div className="hero-area-l0 position-relative">
        <div className="container">
          <div className="row position-relative justify-content-center">
            <div
              className="col-xxl-5 col-xl-6 col-lg-7 col-md-10 order-lg-1 order-1"
              data-aos="fade-right"
              data-aos-delay="300"
              data-aos-duration="1000"
            >
              <div className="content">
                <h1>
                  Hai sahabat UFace!
                </h1>
                <p>
                  Kini hadir produk
                  kecantikan terbaru
                  dari UFace yang dapat
                  membantu merawat kulit
                  wajahmu dengan baik,
                  yaitu Skincare dengan
                  kandungan utama
                  Probiotik dan
                  Prebiotik. Kamu pasti
                  penasaran, kan? Yuk,
                  simak deskripsi produk
                  ini dengan cermat dan
                  detail!
                </p>
              </div>
            </div>
            <div
              className="offset-xxl-1 col-xxl-6 col-xl-6 col-lg-5 col-md-8 order-lg-1 order-0"
              data-aos="fade-left"
              data-aos-delay="300"
              data-aos-duration="1000"
            >
              <div className="">
                <img
                  src="assets/image/l1/hero-photo.jpg"
                  alt=""
                  className="w-100"
                  style={{
                    borderRadius: `30px`,
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
