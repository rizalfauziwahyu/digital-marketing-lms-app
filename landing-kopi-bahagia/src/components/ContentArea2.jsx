import React from "react";

const ContentArea2 = () => {
  return (
    <div class="content-area-l10-2 position-relative">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div
            class="col-xl-5 col-lg-7 col-md-9 order-lg-1 order-1"
            data-aos="fade-up"
            data-aos-delay="300"
            data-aos-duration="1000"
          >
            <div class="content section-heading-4">
              <h2>
                Rasakan Manfaat UFace
              </h2>
              <p>
                UFACE diformulasi khusus
                untuksegala jenis kulit
                dan membatu mengatasi
                segala macam keluhan
                kulit
              </p>
              <ul class="list-unstyled pl-0">
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Mengandung bahan aktif
                  Prebiotik & Probiotik.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Menggunakan
                  Brightening dari
                  kastatertinggi yaitu
                  Chromabright
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Bisa dan aman
                  digunakan mulai
                  dariremaja, manula,
                  bumil, busui.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Cocok untuk all skin
                  typekering,berminyak,
                  sensitve.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Memiliki perlindungan
                  Anti UV, IR &Blue
                  Light
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Penyerapan yang
                  sempurna dancepat ke
                  kulit.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Memiliki kecepatan
                  reaksi positivedan
                  manfaat nyata dalam
                  beberapahari pemakaian
                  saja.
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  Pabrik standar GMP &
                  HACCP
                </li>
                <li class="d-flex align-items-center">
                  <i class="fas fa-check"></i>
                  BPOM & HALAL
                </li>
              </ul>
            </div>
          </div>
          <div
            class="offset-xl-1 col-xl-5 col-lg-5 col-md-8 order-lg-1 order-0"
            data-aos="fade-up"
            data-aos-delay="500"
            data-aos-duration="1000"
          >
            <div class="content-img">
              <img
                src="assets/image/l1/photo3.jpg"
                alt=""
                class="w-100"
                style={{
                  borderRadius: `30px`,
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <div class="content-area-l10-2-shape-1">
        <img
          src="assets/image/l1/shape2.svg"
          alt=""
          class="w-100"
        />
      </div>
    </div>
  );
};

export default ContentArea2;
