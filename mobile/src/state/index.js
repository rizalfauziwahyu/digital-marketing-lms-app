import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  token: null,
};

export const authSLice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.user = action.payload.user;
      state.token =
        action.payload.user.token;
    },
    setLogout: (state, action) => {
      state.user = null;
      state.token = null;
    },
  },
});

export const { setLogin, setLogout } =
  authSLice.actions;

export default authSLice.reducer;
