import {
  createApi,
  fetchBaseQuery,
} from "@reduxjs/toolkit/query/react";

export const api = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl:
      process.env.REACT_APP_BASE_URL,
    prepareHeaders: (
      headers,
      { getState }
    ) => {
      const token =
        getState().auth.token || null;
      if (token) {
        headers.set(
          "Authorization",
          `Bearer ${token}`
        );
      }

      return headers;
    },
  }),
  reducerPath: "adminApi",
  tagTypes: [
    "User",
    "ContentCategories",
    "Content",
    "Contents",
    "ContentByCategory",
    "Classroom",
    "Classrooms",
    "Module",
    "Modules",
    "Event",
    "Events",
    "ProductKnowledge",
    "ProductKnowledges",
    "LandingPage",
    "LandingPages",
    "Users",
  ],
  endpoints: (build) => ({
    getUser: build.query({
      query: () => `api/user/profile`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["User"],
    }),
    updateUser: build.mutation({
      query: (values) => {
        return {
          url: `api/users/${values.get(
            "userId"
          )}?_method=PUT`,
          method: "POST",
          body: values,
        };
      },
      invalidatesTags: ["User"],
    }),
    updateUserPassword: build.mutation({
      query: (values) => {
        return {
          url: `api/user/update-password`,
          method: "PUT",
          body: values,
        };
      },
      invalidatesTags: ["User"],
    }),
    loginUser: build.mutation({
      query: (values) => ({
        url: "api/auth/login",
        method: "POST",
        body: values,
      }),
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
    }),
    registerUser: build.mutation({
      query: (values) => ({
        url: "api/auth/register",
        method: "POST",
        body: values,
      }),
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
    }),
    enrollClassroom: build.mutation({
      query: (slug) => ({
        url: `api/classrooms/${slug}/enroll`,
        method: "POST",
      }),
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      invalidatesTags: ["Classroom"],
    }),
    registerEvent: build.mutation({
      query: (slug) => ({
        url: `api/events/${slug}/register`,
        method: "POST",
      }),
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      invalidatesTags: ["Event"],
    }),
    getContentCategories: build.query({
      query: () =>
        `api/contents/categories`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data
          .categories;
      },
      providesTags: [
        "ContentCategories",
      ],
    }),
    getContents: build.query({
      query: () => `api/contents`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data.contents;
      },
      providesTags: ["Contents"],
    }),
    getContentBySlug: build.query({
      query: (slug) =>
        `api/contents/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["Content"],
    }),
    getContentByCategory: build.query({
      query: (slug) =>
        `api/contents/category/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: [
        "ContentByCategory",
      ],
    }),
    getClassrooms: build.query({
      query: () => "api/classrooms",
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data
          .classrooms;
      },
      providesTags: ["Classrooms"],
    }),
    getClassroomBySlug: build.query({
      query: (slug) =>
        `api/classrooms/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["Classroom"],
    }),
    getModules: build.query({
      query: () => "api/modules",
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data.modules;
      },
      providesTags: ["Modules"],
    }),
    getModuleBySlug: build.query({
      query: (slug) =>
        `api/modules/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["Module"],
    }),
    getEvents: build.query({
      query: () => "api/events",
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data.events;
      },
      providesTags: ["Events"],
    }),
    getEventBySlug: build.query({
      query: (slug) =>
        `api/events/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["Event"],
    }),
    getProductKnowledges: build.query({
      query: () =>
        "api/product-knowledges",
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data
          .productKnowledges;
      },
      providesTags: [
        "ProductKnowledges",
      ],
    }),
    getProductKnowledge: build.query({
      query: (slug) =>
        `api/product-knowledges/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: [
        "ProductKnowledge",
      ],
    }),
    getLandingPages: build.query({
      query: () => "api/landing-pages",
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data
          .landingPages;
      },
      providesTags: ["LandingPages"],
    }),
    getLandingPage: build.query({
      query: (slug) =>
        `api/landing-pages/${slug}`,
      transformResponse: (
        rawResult,
        meta
      ) => {
        return rawResult.data;
      },
      providesTags: ["LandingPage"],
    }),
  }),
});

export const {
  useGetUserQuery,
  useUpdateUserMutation,
  useUpdateUserPasswordMutation,
  useGetContentCategoriesQuery,
  useGetContentBySlugQuery,
  useGetContentByCategoryQuery,
  useGetClassroomBySlugQuery,
  useGetClassroomsQuery,
  useGetModuleBySlugQuery,
  useGetModulesQuery,
  useGetEventBySlugQuery,
  useGetEventsQuery,
  useRegisterEventMutation,
  useLoginUserMutation,
  useRegisterUserMutation,
  useEnrollClassroomMutation,
  useGetLandingPageQuery,
  useGetLandingPagesQuery,
  useGetProductKnowledgeQuery,
  useGetProductKnowledgesQuery,
} = api;
