import {
  BrowserRouter,
  Navigate,
  Routes,
  Route,
} from "react-router-dom";
import { useSelector } from "react-redux";
import Layout from "scenes/layout";
import HomePage from "scenes/homePage";
import ProfilePage from "scenes/profilePage";
import LoginPage from "scenes/loginPage";
import WelcomePage from "scenes/welcomePage";
import ClassroomPage from "scenes/classroomPage";
import DetailClassroom from "scenes/classroomPage/DetailClassroom";
import EventPage from "scenes/eventPage";
import DetailEvent from "scenes/eventPage/DetailEvent";
import ContentPage from "scenes/contentPage";
import ContentByCategory from "scenes/contentPage/ContentByCategory";
import DetailContent from "scenes/contentPage/DetailContent";
import DetailModule from "scenes/classroomPage/DetailModule";
import ShowProfile from "scenes/profilePage/ShowProfile";
import LandingPages from "scenes/landingPage";
import DetailLandingPage from "scenes/landingPage/DetailLandingPage";
import ProductKnowledges from "scenes/productKnowledge";
import DetailProductKnowledge from "scenes/productKnowledge/DetailProductKnowledge";
import UpdateProfile from "scenes/profilePage/updateProfile";
import UpdatePassword from "scenes/profilePage/updatePassword";
import RegisterPage from "scenes/registerPage";
import WaitingVerificationPage from "scenes/waitingVerificationPage";

function App() {
  const isAuth = Boolean(
    useSelector(
      (state) => state.auth.token
    )
  );
  return (
    <div className="app">
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              isAuth ? (
                <Navigate
                  to="/home"
                  replace
                />
              ) : (
                <Navigate
                  to="/welcome"
                  replace
                />
              )
            }
          />
          <Route
            path="/welcome"
            element={<WelcomePage />}
          />
          <Route
            path="/proses-verifikasi"
            element={<WaitingVerificationPage />}
          />
          <Route
            path="/daftar"
            element={<RegisterPage />}
          />
          <Route
            path="/login"
            element={
              isAuth ? (
                <Navigate
                  to="/home"
                  replace
                />
              ) : (
                <LoginPage />
              )
            }
          />
          <Route
            element={
              isAuth ? (
                <Layout />
              ) : (
                <Navigate
                  to="/login"
                  replace
                />
              )
            }
          >
            <Route
              path="/home"
              element={<HomePage />}
            />
            <Route
              path="/kelas-marketing"
              element={
                <ClassroomPage />
              }
            />
            <Route
              path="/kelas-marketing/:slug"
              element={
                <DetailClassroom />
              }
            />
            <Route
              path="/belajar/:slug"
              element={<DetailModule />}
            />
            <Route
              path="/konten-marketing"
              element={<ContentPage />}
            />
            <Route
              path="/konten-marketing/:slug"
              element={
                <DetailContent />
              }
            />
            <Route
              path="/konten-berdasarkan-kategori/:slug"
              element={
                <ContentByCategory />
              }
            />
            <Route
              path="/event"
              element={<EventPage />}
            />
            <Route
              path="/event/:slug"
              element={<DetailEvent />}
            />
            <Route
              path="/product-knowledge"
              element={
                <ProductKnowledges />
              }
            />
            <Route
              path="/product-knowledge/:slug"
              element={
                <DetailProductKnowledge />
              }
            />
            <Route
              path="/landing-page"
              element={<LandingPages />}
            />
            <Route
              path="/landing-page/:slug"
              element={
                <DetailLandingPage />
              }
            />
            <Route
              path="/profil"
              element={
                isAuth ? (
                  <ProfilePage />
                ) : (
                  <Navigate to="/" />
                )
              }
            />
            <Route
              path="/profil/lihat"
              element={<ShowProfile />}
            />
            <Route
              path="/profil/update"
              element={
                <UpdateProfile />
              }
            />
            <Route
              path="/profil/update-password"
              element={
                <UpdatePassword />
              }
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
