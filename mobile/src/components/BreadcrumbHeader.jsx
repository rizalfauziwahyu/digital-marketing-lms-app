import React from "react";
import { Link } from "react-router-dom";

const BreadcrumbHeader = ({
  route,
  title,
}) => {
  return (
    <div
      class="header-area"
      id="headerArea"
    >
      <div class="container">
        <div className="header-content position-relative d-flex align-items-center justify-content-between">
          <div className="back-button">
            <Link to={route}>
              <svg
                className="bi bi-arrow-left-short"
                width="32"
                height="32"
                viewBox="0 0 16 16"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"
                ></path>
              </svg>
            </Link>
          </div>

          <div className="page-heading">
            <h6 className="mb-0">
              {title}
            </h6>
          </div>

          <div
            className="navbar--toggler"
            id="affanNavbarToggler"
            data-bs-toggle="offcanvas"
            data-bs-target="#affanOffcanvas"
            aria-controls="affanOffcanvas"
          >
            <span className="d-block"></span>
            <span className="d-block"></span>
            <span className="d-block"></span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BreadcrumbHeader;
