import React from "react";
import { Link } from "react-router-dom";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import { useGetClassroomsQuery } from "state/api";

const HeroSlider = () => {
  const { data } =
    useGetClassroomsQuery();
  return (
    <div className="tiny-slider-one-wrapper">
      <div className="tiny-slider-one">
        <Slide arrows={false}>
          {data && data[0] && (
            <div
              className="single-hero-slide each-slide-effect bg-overlay"
              style={{
                "background-image": `url('${data[0].image}')`,
              }}
            >
              <div className="h-100 d-flex align-items-center text-center">
                <div className="container">
                  <h3 className="text-white mb-1">
                    {data[0].title}
                  </h3>
                  <Link className="btn btn-creative btn-warning">
                    Pelajari Sekarang
                  </Link>
                </div>
              </div>
            </div>
          )}
          {data && data[1] && (
            <div
              className="single-hero-slide each-slide-effect bg-overlay"
              style={{
                "background-image": `url('${data[1].image}')`,
              }}
            >
              <div className="h-100 d-flex align-items-center text-center">
                <div className="container">
                  <h3 className="text-white mb-1">
                    {data[1].title}
                  </h3>
                  <Link className="btn btn-creative btn-warning">
                    Pelajari Sekarang
                  </Link>
                </div>
              </div>
            </div>
          )}
          {data && data[2] && (
            <div
              className="single-hero-slide each-slide-effect bg-overlay"
              style={{
                "background-image": `url('${data[2].image}')`,
              }}
            >
              <div className="h-100 d-flex align-items-center text-center">
                <div className="container">
                  <h3 className="text-white mb-1">
                    {data[2].title}
                  </h3>
                  <a
                    className="btn btn-creative btn-warning"
                    href="/"
                  >
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          )}
        </Slide>
      </div>
    </div>
  );
};

export default HeroSlider;
