import React from "react";
import {
  Link,
  Navigate,
  useNavigate,
} from "react-router-dom";
import {
  useDispatch,
  useSelector,
} from "react-redux";
import { setLogout } from "state";
import { useGetUserQuery } from "state/api";

const Sidebar = () => {
  const { data, isLoading, isError } =
    useGetUserQuery();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const token = useSelector(
    (state) => state.auth.token
  );

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  if (!isLoading && isError && token) {
    dispatch(setLogout());
    window.location.reload();
  }

  return (
    <div
      className="offcanvas offcanvas-start"
      id="affanOffcanvas"
      data-bs-scroll="true"
      tabindex="-1"
      aria-labelledby="affanOffcanvsLabel"
    >
      <button
        className="btn-close btn-close-white text-reset"
        type="button"
        data-bs-dismiss="offcanvas"
        aria-label="Close"
      ></button>
      {data && (
        <div className="offcanvas-body p-0">
          <div className="sidenav-wrapper">
            <div className="sidenav-profile bg-gradient">
              <div className="sidenav-style1"></div>

              <div className="user-profile">
                <img
                  src={
                    data.photo
                      ? data.photo
                      : "assets/img/core-img/user.png"
                  }
                  alt={`foto profil ${data.name}`}
                />
              </div>

              <div className="user-info">
                <h6 className="user-name mb-0">
                  {data.name}
                </h6>
                <span>
                  {data.status
                    ? data.status
                    : "Member BOB"}
                </span>
              </div>
            </div>

            <ul className="sidenav-nav ps-0">
              <li>
                <Link to="/home">
                  <i className="bi bi-house-door"></i>
                  Home
                </Link>
              </li>
              <li>
                <Link to="/konten-marketing">
                  <i class="bi bi-book"></i>
                  Konten Marketing
                  <span className="badge bg-success rounded-pill ms-2">
                    100+
                  </span>
                </Link>
              </li>
              <li>
                <Link to="/kelas-marketing">
                  <i className="bi bi-collection"></i>
                  Kelas Marketing
                  <span className="badge bg-success rounded-pill ms-2">
                    10+
                  </span>
                </Link>
              </li>
              <li>
                <Link to="/event">
                  <i class="bi bi-calendar2-event"></i>
                  Event BOB
                </Link>
              </li>
              <li>
                <Link to="/product-knowledge">
                  <i class="bi bi-file-earmark-text"></i>
                  Product Knowledge
                </Link>
              </li>
              <li>
                <Link href="/landing-page-marketing">
                  <i className="bi bi-globe2"></i>
                  Landing Page Marketing
                </Link>
              </li>
              <li>
                <Link href="/profil">
                  <i class="bi bi-person-square"></i>
                  Profil Pengguna
                </Link>
              </li>
              <li>
                <Link
                  onClick={() => {
                    dispatch(
                      setLogout()
                    );
                  }}
                >
                  <i className="bi bi-box-arrow-right"></i>
                  Logout
                </Link>
              </li>
            </ul>

            <div className="social-info-wrap">
              <Link href="#">
                <i className="bi bi-facebook"></i>
              </Link>
              <Link href="#">
                <i className="bi bi-twitter"></i>
              </Link>
              <Link href="#">
                <i className="bi bi-linkedin"></i>
              </Link>
            </div>

            <div className="copyright-info">
              <p>
                2023 &copy; Made by
                <Link href="#">
                  BOB
                </Link>
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Sidebar;
