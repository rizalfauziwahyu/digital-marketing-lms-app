import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <>
      <div
        className="header-area"
        id="headerArea"
      >
        <div className="container">
          <div className="header-content header-style-five position-relative d-flex align-items-center justify-content-between">
            <div className="logo-wrapper">
              <Link to="/home">
                <img
                  src="assets/img/core-img/logo-bac.png"
                  alt=""
                />
              </Link>
            </div>

            <div
              className="navbar--toggler"
              id="affanNavbarToggler"
              data-bs-toggle="offcanvas"
              data-bs-target="#affanOffcanvas"
              aria-controls="affanOffcanvas"
            >
              <span className="d-block"></span>
              <span className="d-block"></span>
              <span className="d-block"></span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
