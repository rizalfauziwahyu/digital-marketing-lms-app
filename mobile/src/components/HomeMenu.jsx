import React from "react";
import { Link } from "react-router-dom";

const HomeMenu = () => {
  return (
    <div className="container direction-rtl">
      <div className="card mb-3">
        <div className="card-body">
          <div className="container">
            <div className="element-heading">
              <h4 className="mb-3">
                <span className="text-primary">
                  Digital Marketing
                </span>
              </h4>
              <h6 className="mb-2">
                🎯 Karena produk dan
                jasa bukanlah bola yang
                saat diam saja bakal
                diperebutkan. Jadilah
                striker paling handal
                bagi pertumbuhan bisnis!
              </h6>
            </div>
          </div>
          <div className="row g-3 mb-3">
            <div className="col-4">
              <Link to="/kelas-marketing">
                <div className="feature-card mx-auto text-center">
                  <div className="card mx-auto bg-gray">
                    <img
                      src="assets/img/core-img/lecture.png"
                      alt=""
                    />
                  </div>
                  <p className="mb-0">
                    Kelas Marketing
                  </p>
                </div>
              </Link>
            </div>
            <div className="col-4">
              <Link to="/konten-marketing">
                <div className="feature-card mx-auto text-center">
                  <div className="card mx-auto bg-gray">
                    <img
                      src="assets/img/core-img/social-media-marketing.png"
                      alt=""
                    />
                  </div>
                  <p className="mb-0">
                    Konten Marketing
                  </p>
                </div>
              </Link>
            </div>
            <div className="col-4">
              <Link to="/event">
                <div className="feature-card mx-auto text-center">
                  <div className="card mx-auto bg-gray">
                    <img
                      src="assets/img/core-img/planner.png"
                      alt=""
                    />
                  </div>
                  <p className="mb-0">
                    Event BOB
                  </p>
                </div>
              </Link>
            </div>
          </div>
          <div className="row g-3">
            <div className="col-4">
              <Link to="/landing-page">
                <div className="feature-card mx-auto text-center">
                  <div className="card mx-auto bg-gray">
                    <img
                      src="assets/img/core-img/world-wide-web.png"
                      alt=""
                    />
                  </div>
                  <p className="mb-0">
                    Landing Page <br />{" "}
                    Marketing
                  </p>
                </div>
              </Link>
            </div>
            <div className="col-4">
              <Link to="/product-knowledge">
                <div className="feature-card mx-auto text-center">
                  <div className="card mx-auto bg-gray">
                    <img
                      src="assets/img/core-img/product-development.png"
                      alt=""
                    />
                  </div>
                  <p className="mb-0">
                    Product Knowledge
                  </p>
                </div>
              </Link>
            </div>
            {/* <div className="col-4">
              <div className="feature-card mx-auto text-center">
                <div className="card mx-auto bg-gray">
                  <img
                    src="assets/img/core-img/faq.png"
                    alt=""
                  />
                </div>
                <p className="mb-0">
                  FAQ
                </p>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeMenu;
