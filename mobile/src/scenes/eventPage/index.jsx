import React from "react";
import { Link } from "react-router-dom";
import Header from "components/Header";
import { useGetEventsQuery } from "state/api";
import moment from "moment";
import "moment/locale/id";

const EventPage = () => {
    const { data, isLoading } = useGetEventsQuery();

    if (isLoading) {
        return (
            <div id="preloader">
                <div className="spinner-grow text-primary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        );
    }
    return (
        <>
            <Header />
            <div className="page-content-wrapper py-3">
                <div className="container">
                    {data && data.length == 0 && (
                        <>
                            <img
                                class="card-img-top"
                                src="assets/img/core-img/coming-soon.jpg"
                                alt={`gambar-coming-soon`}
                            />
                        </>
                    )}
                    {data &&
                        data.map(
                            ({ slug, title, image, type, date }, index) => (
                                <div
                                    key={index}
                                    className="row justify-content-center mb-3"
                                >
                                    <div className="col-12">
                                        <div className="card shadow-sm blog-list-card">
                                            <div className="d-flex align-items-center">
                                                <div
                                                    className="card-blog-img position-relative"
                                                    style={{
                                                        "background-image": `url(${image})`,
                                                    }}
                                                >
                                                    <span className="badge bg-warning text-dark position-absolute card-badge">
                                                        {type}
                                                    </span>
                                                </div>
                                                <div className="card-blog-content">
                                                    <span className="badge bg-danger rounded-pill mb-2 d-inline-block">
                                                        {moment(date)
                                                            .locale("id")
                                                            .format(
                                                                "dddd DD-MM-YYYY"
                                                            )}
                                                    </span>
                                                    <a
                                                        className="blog-title d-block mb-3 text-dark"
                                                        href="page-blog-details.html"
                                                    >
                                                        {title}
                                                    </a>
                                                    <Link
                                                        className="btn btn-primary btn-sm"
                                                        to={`/event/${slug}`}
                                                    >
                                                        Lihat
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        )}
                </div>
            </div>
        </>
    );
};

export default EventPage;
