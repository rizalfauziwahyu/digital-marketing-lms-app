import BreadcrumbHeader from "components/BreadcrumbHeader";
import React from "react";
import {
  Link,
  useParams,
} from "react-router-dom";
import moment from "moment";
import "moment/locale/id";
import {
  useGetEventBySlugQuery,
  useRegisterEventMutation,
} from "state/api";

const DetailEvent = () => {
  const { slug } = useParams();
  const { data, isLoading } =
    useGetEventBySlugQuery(slug);
  const [
    registerEvent,
    { isLoading: isRegisterLoading },
  ] = useRegisterEventMutation();

  const handleRegisterEvent = (
    slug
  ) => {
    registerEvent(slug);
  };

  if (isLoading || isRegisterLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Event"
      />
      {data && (
        <div className="page-content-wrapper">
          <div className="container">
            <div className="pt-3 d-block"></div>
            <div className="blog-details-post-thumbnail position-relative">
              <img
                className="w-100 rounded-lg"
                src={data.image}
                alt=""
              />
            </div>
          </div>
          <div className="blog-description py-3">
            <div className="container">
              <Link className="badge bg-primary mb-2 d-inline-block">
                {data.type}
              </Link>
              <div className="row gx-2 align-items-end">
                {data.registered ? (
                  <>
                    <div className="col-12">
                      <div
                        className="alert mt-3 custom-alert-1 alert-success alert-dismissible fade show"
                        role="alert"
                      >
                        <i class="bi bi-check-circle"></i>{" "}
                        Kamu telah
                        terdaftar
                        mengikuti event
                        ini!
                      </div>

                      <div className="image-gallery-text mb-4">
                        <h3 className="mb-0">
                          {data.title}
                        </h3>
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    <div className="col-8">
                      <div className="image-gallery-text mb-4">
                        <h3 className="mb-0">
                          {data.title}
                        </h3>
                      </div>
                    </div>
                    <div className="col-4 text-end">
                      <Link
                        className="btn btn-primary btn-sm mb-4"
                        onClick={() =>
                          handleRegisterEvent(
                            slug
                          )
                        }
                      >
                        Konfirmasi
                        Kehadiran
                      </Link>
                    </div>
                  </>
                )}
              </div>
              <h6>
                Hari & Tanggal :{" "}
                <span className="badge bg-primary ms-2">
                  {moment(data.date)
                    .locale("id")
                    .format(
                      "dddd DD-MM-YYYY"
                    )}
                </span>
              </h6>
              <h6>
                Waktu Mulai :{" "}
                <span className="badge bg-primary ms-2">
                  {moment(
                    data.start_time
                  ).format("HH:mm")}
                </span>
              </h6>
              <h6>
                Waktu Selesai :{" "}
                <span className="badge bg-primary ms-2">
                  {moment(
                    data.end_time
                  ).format("HH:mm")}
                </span>
              </h6>
              <div
                dangerouslySetInnerHTML={{
                  __html:
                    data.description,
                }}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default DetailEvent;
