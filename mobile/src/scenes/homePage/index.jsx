import HomeMenu from "components/HomeMenu";
import React from "react";
import { Link } from "react-router-dom";
import Header from "components/Header";

const HomePage = () => {
  return (
    <>
      <Header />
      <div className="page-content-wrapper">
        <div className="pt-3"></div>
        <HomeMenu />
        <div className="container">
          <div
            className="card card-bg-img bg-img bg-overlay mb-3"
            style={{
              "background-image":
                "url('assets/img/bg-img/social-media-1200x800.jpg')",
            }}
          >
            <div className="card-body direction-rtl p-5">
              <h6>
                <span className="text-white">
                  E-Learning
                </span>
              </h6>
              <h2 className="text-white">
                Belajar Fleksibel
                Ratusan Skill.
              </h2>
              <p className="mb-4 text-white">
                Pilih skill apapun dan
                pelajari kapanpun.
                Dapatkan video materi
                terstruktur, modul
                praktik plus webinar
                series rancangan para
                experts dari top
                companies.
              </p>
              <Link
                className="btn btn-warning"
                to="/kelas-marketing"
              >
                Lihat Semua Kelas
                Tersedia
              </Link>
            </div>
          </div>
        </div>
        <div className="container">
          <div
            className="card bg-primary mb-3 bg-img"
            style={{
              "background-image":
                "url('assets/img/core-img/1.png')",
            }}
          >
            <div className="card-body direction-rtl p-5">
              <h3 className="text-white">
                Dalami Skill Secara
                Online Live, Praktik &
                Intensif
              </h3>
              <p class="lead mb-3 text-white">
                Ikuti Event Workshop
                Online yang berfokus
                pada proyek riil. Dari
                paham konsep sampai bisa
                praktek dipandu experts.
              </p>
              <Link
                className="btn btn-warning"
                to="/event"
              >
                Lihat semua event
              </Link>
            </div>
          </div>
        </div>
        <div className="pb-3"></div>
      </div>
    </>
  );
};

export default HomePage;
