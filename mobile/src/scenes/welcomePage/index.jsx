import React from "react";
import { Link } from "react-router-dom";

const welcomePage = () => {
  return (
    <>
      <div className="hero-block-wrapper bg-primary">
        <div className="hero-block-styles">
          <div
            className="hb-styles1"
            style={{
              "background-image": `url('assets/img/core-img/dot.png')`,
            }}
          ></div>
          <div className="hb-styles2"></div>
          <div className="hb-styles3"></div>
        </div>
        <div className="custom-container">
          <div className="skip-page">
            <Link to="/home">Skip</Link>
          </div>

          <div className="hero-block-content">
            <img
              className="mb-4"
              src="assets/img/core-img/welcome.png"
              alt=""
            />
            <h2 className="display-4 text-white mb-3">
              Selamat datang di Platform
              Digital Marketing UFace
            </h2>
            <p className="text-white">
              “Good marketing makes the
              company look smart. Great
              marketing makes the
              customer feel smart.” –
              Joe Chernov.
            </p>
            <Link
              to="/login"
              className="btn btn-warning btn-lg w-100"
            >
              Yuk kita mulai!
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default welcomePage;
