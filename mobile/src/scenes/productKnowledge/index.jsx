import React from "react";
import { Link } from "react-router-dom";
import { useGetProductKnowledgesQuery } from "state/api";
import BreadcrumbHeader from "components/BreadcrumbHeader";

const ProductKnowledges = () => {
  const { data, isLoading } =
    useGetProductKnowledgesQuery();

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Product Knowledge"
      />
      <div className="page-content-wrapper py-3">
        <div className="container">
          <div className="element-heading">
            <h6>
              Nikmati Update Knowledge
              Baru Setiap Harinya!!
            </h6>
          </div>
        </div>
        <div className="top-products-area">
          <div className="container">
            <div className="row g-3">
              {data &&
                data.map(
                  ({
                    slug,
                    title,
                    image,
                  }) => (
                    <div
                      key={slug}
                      className="col-12 col-sm-6 col-lg-4"
                    >
                      <div className="card single-product-card">
                        <div className="card-body p-3">
                          <Link
                            className="product-thumbnail d-block"
                            to={`/product-knowledge/${slug}`}
                          >
                            <img
                              src={
                                image
                              }
                              alt={`Gambar ${slug}`}
                            />
                          </Link>
                          <Link
                            className="product-title d-block"
                            to={`/product-knowledge/${slug}`}
                          >
                            {title}
                          </Link>
                        </div>
                      </div>
                    </div>
                  )
                )}
            </div>
          </div>
        </div>
        {/* <div className="container">
          <div className="card image-gallery-card">
            <div className="card-body">
              <div className="row g-3">
                {data &&
                  data.map(
                    (
                      { image, slug },
                      index
                    ) => (
                      <div
                        key={index}
                        className="col-6"
                      >
                        <Link
                          className="single-gallery-item"
                          to={`/product-knowledge/${slug}`}
                        >
                          <img
                            src={
                              image
                            }
                            alt=""
                          />
                        </Link>
                      </div>
                    )
                  )}
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </>
  );
};

export default ProductKnowledges;
