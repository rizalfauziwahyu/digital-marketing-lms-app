import React, { useState } from "react";
import {
  Link,
  useParams,
} from "react-router-dom";
import { useSelector } from "react-redux";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useGetProductKnowledgeQuery } from "state/api";

const DetailProductKnowledge = () => {
  const { slug } = useParams();
  const [isCopied, setIsCopied] =
    useState(false);

  const [
    isAttachmentDownloaded,
    setIsAttachmentDownloaded,
  ] = useState(false);

  const token = useSelector(
    (state) => state.auth.token
  );

  const { data, isLoading } =
    useGetProductKnowledgeQuery(slug);

  const handleDownloadAttachment =
    () => {
      fetch(
        process.env.REACT_APP_BASE_URL +
          `/api/product-knowledges/download/${slug}`,
        {
          method: "GET",
          headers: {
            "Content-Type":
              "application/pdf",
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((response) =>
          response.blob()
        )
        .then((blob) => {
          // Create blob link to download
          const url =
            window.URL.createObjectURL(
              new Blob([blob])
            );
          const link =
            document.createElement("a");
          link.href = url;
          link.setAttribute(
            "download",
            `${data.attachment.filename}`
          );

          // Append to html link element page
          document.body.appendChild(
            link
          );

          // Start download
          link.click();

          // Clean up and remove the link
          link.parentNode.removeChild(
            link
          );

          setIsAttachmentDownloaded(
            true
          );
          setTimeout(() => {
            setIsAttachmentDownloaded(
              false
            );
          }, 3000);
        });
    };

  async function copyTextToClipboard(
    text
  ) {
    if ("clipboard" in navigator) {
      return await navigator.clipboard.writeText(
        text
      );
    } else {
      return document.execCommand(
        "copy",
        true,
        text
      );
    }
  }

  // onClick handler function for the copy button
  const handleCopyClick = (
    copyText
  ) => {
    // Asynchronously call copyTextToClipboard
    copyTextToClipboard(copyText)
      .then(() => {
        // If successful, update the isCopied state value
        setIsCopied(true);
        setTimeout(() => {
          setIsCopied(false);
        }, 3000);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        title="Detail Product Knowledge"
        route={-1}
      />
      <div className="page-content-wrapper py-3">
        {data && (
          <div className="container">
            <div class="card position-relative shadow-sm mb-3">
              <img
                class="card-img-top"
                src={data.image}
                alt={`gambar-${slug}`}
              />
            </div>
            <div className="card product-details-card mb-3 direction-rtl">
              <div className="card-body">
                <h3>{data.title}</h3>

                <div class="row gx-2 align-items-end">
                  <div class="col-8">
                    <div class="image-gallery-text mb-4">
                      <h3 class="mb-0">
                        Copywriting
                      </h3>
                    </div>
                  </div>
                  <div class="col-4 text-end">
                    <Link
                      class={`btn ${
                        isCopied
                          ? "btn-success"
                          : "btn-primary"
                      } btn-sm mb-4`}
                      onClick={() =>
                        handleCopyClick(
                          data.content
                            .replace(
                              /(<p>)*/g,
                              ""
                            )
                            .replace(
                              /<(\/)?p[^>]*>/g,
                              ""
                            )
                        )
                      }
                    >
                      {!isCopied ? (
                        <>
                          Copy Teks{" "}
                          <i class="bi bi-clipboard"></i>
                        </>
                      ) : (
                        <>
                          Copy Teks
                          Berhasil!{" "}
                          <i class="bi bi-clipboard-check"></i>
                        </>
                      )}
                    </Link>
                  </div>
                </div>

                <div
                  dangerouslySetInnerHTML={{
                    __html:
                      data.content,
                  }}
                />

                {data.attachment && (
                  <>
                    <div class="image-gallery-text mb-1">
                      <h5 class="mb-0">
                        File Lampiran
                      </h5>
                    </div>
                    <div className="mb-3">
                      <Link
                        className={`btn m-1 ${
                          isAttachmentDownloaded
                            ? "btn-success"
                            : "btn-primary"
                        }`}
                        onClick={() =>
                          handleDownloadAttachment()
                        }
                      >
                        {isAttachmentDownloaded ? (
                          <>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              class="bi bi-file-earmark-check"
                              viewBox="0 0 16 16"
                            >
                              <path d="M10.854 7.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
                              <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z" />
                            </svg>{" "}
                            Download
                            Berhasil!
                          </>
                        ) : (
                          <>
                            <svg
                              className="bi bi-arrow-down me-2"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="currentColor"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"
                              ></path>
                            </svg>
                            Download
                            File
                          </>
                        )}
                      </Link>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailProductKnowledge;
