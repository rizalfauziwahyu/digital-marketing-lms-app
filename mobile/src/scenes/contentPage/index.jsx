import React from "react";
import { useDispatch } from "react-redux";
import { setLogout } from "state";
import { Link } from "react-router-dom";
import { useGetContentCategoriesQuery } from "state/api";
import Header from "components/Header";

const ContentPage = () => {
  const dispatch = useDispatch();
  const {
    data,
    isLoading,
    error,
    isError,
  } = useGetContentCategoriesQuery();

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  if (isError && error.status === 401) {
    dispatch(setLogout());
  }

  return (
    <>
      <Header />
      <div class="page-content-wrapper py-3">
        <div class="container">
          {data &&
            data.map(
              (
                { slug, name, image },
                index
              ) => (
                <Link
                  key={index}
                  to={`/konten-berdasarkan-kategori/${slug}`}
                >
                  <div class="card position-relative shadow-sm mb-3">
                    <img
                      class="card-img-top"
                      src={image}
                      alt={`gambar-${slug}`}
                    />
                    <span class="badge bg-warning text-dark position-absolute card-badge">
                      {name}
                    </span>
                  </div>
                </Link>
              )
            )}
        </div>
      </div>
    </>
  );
};

export default ContentPage;
