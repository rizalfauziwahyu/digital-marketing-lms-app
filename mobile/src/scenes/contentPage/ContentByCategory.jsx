import React from "react";
import {
  Link,
  useParams,
} from "react-router-dom";
import { useGetContentByCategoryQuery } from "state/api";
import BreadcrumbHeader from "components/BreadcrumbHeader";

const ContentByCategory = () => {
  const { slug } = useParams();
  const { data, isLoading } =
    useGetContentByCategoryQuery(slug);

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Galeri Konten"
      />
      <div className="page-content-wrapper py-3">
        <div className="container">
          <div className="element-heading">
            <h6>
              Nikmati Update Konten Baru Setiap Harinya!!
            </h6>
          </div>
        </div>
        <div className="container">
          <div className="card image-gallery-card">
            <div className="card-body">
              <div className="row g-3">
                {data &&
                  data.contents.map(
                    (
                      { image, slug },
                      index
                    ) => (
                      <div
                        key={index}
                        className="col-6"
                      >
                        <Link
                          className="single-gallery-item"
                          to={`/konten-marketing/${slug}`}
                        >
                          <img
                            src={image.url}
                            alt=""
                          />
                        </Link>
                      </div>
                    )
                  )}
              </div>
            </div>
          </div>
        </div>
        {/* <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-8 col-lg-7 col-xl-6">
              <div className="card mt-3">
                <div className="card-body p-3">
                  <nav aria-label="Page navigation example">
                    <ul className="pagination justify-content-center pagination-one direction-rtl">
                      <li className="page-item disabled">
                        <Link
                          className="page-link"
                          href="#"
                          aria-label="Previous"
                        >
                          <svg
                            className="bi bi-chevron-left"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              fill-rule="evenodd"
                              d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
                            ></path>
                          </svg>
                        </Link>
                      </li>
                      <li className="page-item active">
                        <Link
                          className="page-link"
                          href="#"
                        >
                          1
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link
                          className="page-link"
                          href="#"
                        >
                          2
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link
                          className="page-link"
                          href="#"
                        >
                          ...
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link
                          className="page-link"
                          href="#"
                        >
                          9
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link
                          className="page-link"
                          href="#"
                          aria-label="Next"
                        >
                          <svg
                            className="bi bi-chevron-right"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              fill-rule="evenodd"
                              d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
                            ></path>
                          </svg>
                        </Link>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </>
  );
};

export default ContentByCategory;
