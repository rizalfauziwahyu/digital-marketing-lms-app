import React, { useState } from "react";
import { Link, useParams, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useGetContentBySlugQuery } from "state/api";

const DetailContent = () => {
    const { slug } = useParams();
    const [isCopied, setIsCopied] = useState(false);
    const [isImageDownloaded, setIsImageDownloaded] = useState(false);
    const [isVideoDownloaded, setIsVideoDownloaded] = useState(false);
    const { pathname } = useLocation();
    const token = useSelector((state) => state.auth.token);

    const { data, isLoading } = useGetContentBySlugQuery(slug);

    const handleDownloadImage = () => {
        fetch(
            process.env.REACT_APP_BASE_URL + `/api/contents/download/${slug}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/pdf",
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((response) => response.blob())
            .then((blob) => {
                // Create blob link to download
                const url = window.URL.createObjectURL(new Blob([blob]));
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", `${data.image.filename}`);

                // Append to html link element page
                document.body.appendChild(link);

                // Start download
                link.click();

                // Clean up and remove the link
                link.parentNode.removeChild(link);

                setIsImageDownloaded(true);
                setTimeout(() => {
                    setIsImageDownloaded(false);
                }, 3000);
            });
    };

    const handleDownloadVideo = () => {
        fetch(
            process.env.REACT_APP_BASE_URL +
                `/api/contents/download-video/${slug}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/pdf",
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((response) => response.blob())
            .then((blob) => {
                // Create blob link to download
                const url = window.URL.createObjectURL(new Blob([blob]));
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", `${data.videofile}`);

                // Append to html link element page
                document.body.appendChild(link);

                // Start download
                link.click();

                // Clean up and remove the link
                link.parentNode.removeChild(link);

                setIsVideoDownloaded(true);
                setTimeout(() => {
                    setIsVideoDownloaded(false);
                }, 3000);
            });
    };

    async function copyTextToClipboard(text) {
        if ("clipboard" in navigator) {
            return await navigator.clipboard.writeText(text);
        } else {
            return document.execCommand("copy", true, text);
        }
    }

    // onClick handler function for the copy button
    const handleCopyClick = (copyText) => {
        // Asynchronously call copyTextToClipboard
        copyTextToClipboard(copyText)
            .then(() => {
                // If successful, update the isCopied state value
                setIsCopied(true);
                setTimeout(() => {
                    setIsCopied(false);
                }, 3000);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    if (isLoading) {
        return (
            <div id="preloader">
                <div className="spinner-grow text-primary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        );
    }

    return (
        <>
            <BreadcrumbHeader title="Detail Konten" route={-1} />
            <div className="page-content-wrapper py-3">
                {data && (
                    <div className="container">
                        <div className="card product-details-card mb-3">
                            <div className="card-body text-center">
                                <div className="product-gallery-wrapper">
                                    <div className="product-gallery mb-3">
                                        <Link to={data.image.url}>
                                            <img
                                                className="rounded"
                                                src={data.image.url}
                                                alt=""
                                            />
                                        </Link>
                                    </div>
                                    <Link
                                        className={`btn m-1 ${
                                            isImageDownloaded
                                                ? "btn-success"
                                                : "btn-primary"
                                        }`}
                                        onClick={() => handleDownloadImage()}
                                    >
                                        {isImageDownloaded ? (
                                            <>
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width="16"
                                                    height="16"
                                                    fill="currentColor"
                                                    class="bi bi-file-earmark-check"
                                                    viewBox="0 0 16 16"
                                                >
                                                    <path d="M10.854 7.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
                                                    <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z" />
                                                </svg>{" "}
                                                Download Berhasil!
                                            </>
                                        ) : (
                                            <>
                                                <svg
                                                    className="bi bi-arrow-down me-2"
                                                    width="16"
                                                    height="16"
                                                    viewBox="0 0 16 16"
                                                    fill="currentColor"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        fill-rule="evenodd"
                                                        d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"
                                                    ></path>
                                                </svg>
                                                Download Gambar
                                            </>
                                        )}
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="card product-details-card mb-3 direction-rtl">
                            <div className="card-body">
                                <h3>{data.title}</h3>

                                {data.videofile && (
                                    <>
                                        {data.video && (
                                            <div className="ratio ratio-1x1 mb-4">
                                                <iframe
                                                    width="621"
                                                    height="1104"
                                                    src={`https://www.youtube.com/embed/${data.video}`}
                                                    title="Obat kuat pria dari Kopi Bahagia #shorts #filmpendek #miabi"
                                                    frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen
                                                ></iframe>
                                            </div>
                                        )}

                                        <div className="text-center mb-3">
                                            <Link
                                                className={`btn m-1 ${
                                                    isVideoDownloaded
                                                        ? "btn-success"
                                                        : "btn-primary"
                                                }`}
                                                onClick={() =>
                                                    handleDownloadVideo()
                                                }
                                            >
                                                {isVideoDownloaded ? (
                                                    <>
                                                        <svg
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            width="16"
                                                            height="16"
                                                            fill="currentColor"
                                                            class="bi bi-file-earmark-check"
                                                            viewBox="0 0 16 16"
                                                        >
                                                            <path d="M10.854 7.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
                                                            <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z" />
                                                        </svg>{" "}
                                                        Download Berhasil!
                                                    </>
                                                ) : (
                                                    <>
                                                        <svg
                                                            className="bi bi-arrow-down me-2"
                                                            width="16"
                                                            height="16"
                                                            viewBox="0 0 16 16"
                                                            fill="currentColor"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        >
                                                            <path
                                                                fill-rule="evenodd"
                                                                d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"
                                                            ></path>
                                                        </svg>
                                                        Download Video
                                                    </>
                                                )}
                                            </Link>
                                        </div>
                                    </>
                                )}

                                <div class="row gx-2 align-items-end">
                                    <div class="col-8">
                                        <div class="image-gallery-text mb-4">
                                            <h3 class="mb-0">Copywriting</h3>
                                        </div>
                                    </div>
                                    <div class="col-4 text-end">
                                        <Link
                                            class={`btn ${
                                                isCopied
                                                    ? "btn-success"
                                                    : "btn-primary"
                                            } btn-sm mb-4`}
                                            onClick={() =>
                                                handleCopyClick(
                                                    data.content
                                                        .replace(/(<p>)*/g, "")
                                                        .replace(
                                                            /<(\/)?p[^>]*>/g,
                                                            ""
                                                        )
                                                )
                                            }
                                        >
                                            {!isCopied ? (
                                                <>
                                                    Copy Teks{" "}
                                                    <i class="bi bi-clipboard"></i>
                                                </>
                                            ) : (
                                                <>
                                                    Copy Teks Berhasil!{" "}
                                                    <i class="bi bi-clipboard-check"></i>
                                                </>
                                            )}
                                        </Link>
                                    </div>
                                </div>

                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: data.content,
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </>
    );
};

export default DetailContent;
