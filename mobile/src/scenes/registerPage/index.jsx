import React from "react";
import * as yup from "yup";
import { Formik, Field } from "formik";
import { useRegisterUserMutation } from "state/api";
import { useDispatch } from "react-redux";
import {
  useNavigate,
  Link,
} from "react-router-dom";

const registerSchema = yup
  .object()
  .shape({
    name: yup
      .string()
      .required("Nama wajib diisi!"),
    username: yup
      .string()
      .required(
        "Username UFace wajib diisi!"
      ),
    email: yup
      .string()
      .required("Email wajib diisi!"),
    whatsapp: yup
      .string()
      .required(
        "Nomor whatsapp wajib diisi!"
      )
      .matches(
        /^(\+62|62)8[1-9][0-9]{6,9}$/,
        "Format nomor whatsapp wajib awalan 62, Contoh : 628525xxxxx"
      ),
    password: yup
      .string()
      .required(
        "Password wajib diisi!"
      ),
    changepassword: yup
      .string()
      .required(
        "Konfirmasi password wajib diisi!"
      )
      .oneOf(
        [yup.ref("password"), null],
        "Kedua password harus sama persis!"
      ),
  });

const initialValues = {
  name: "",
  username: "",
  email: "",
  whatsapp: "",
  password: "",
  changepassword: "",
};

const RegisterPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [
    registerUser,
    { isLoading, isError, error, data },
  ] = useRegisterUserMutation();

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    console.log(values);
    await registerUser(values);
    onSubmitProps.resetForm();
  };

  if (data) {
    navigate("/proses-verifikasi");
  }

  return (
    <>
      {isLoading && (
        <div id="preloader">
          <div
            className="spinner-grow text-primary"
            role="status"
          >
            <span className="visually-hidden">
              Loading...
            </span>
          </div>
        </div>
      )}
      <div
        className="internet-connection-status"
        id="internetStatus"
      ></div>

      <div className="login-back-button">
        <Link to="/login">
          <svg
            className="bi bi-arrow-left-short"
            width="32"
            height="32"
            viewBox="0 0 16 16"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"
            ></path>
          </svg>
        </Link>
      </div>

      <div className="login-wrapper d-flex align-items-center justify-content-center">
        <div className="custom-container">
          <div className="text-center px-4">
            <img
              className="login-intro-img"
              src="assets/img/core-img/register.jpg"
              alt=""
            ></img>
          </div>

          <div className="register-form mt-4 mb-4">
            <h6 className="mb-3 text-center">
              Silahkan daftar untuk
              dapat mengakses segudang
              fitur DIGITAL MARKETING
              UFace
            </h6>
            {isError && (
              <div
                className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                role="alert"
              >
                <i className="bi bi-x-circle"></i>
                {error.data.message}
              </div>
            )}
            <Formik
              onSubmit={
                handleFormSubmit
              }
              initialValues={
                initialValues
              }
              validationSchema={
                registerSchema
              }
            >
              {({
                values,
                errors,
                touched,
                handleBlur,
                handleChange,
                handleSubmit,
              }) => (
                <form
                  onSubmit={
                    handleSubmit
                  }
                >
                  <div className="form-group">
                    <Field
                      className="form-control"
                      name="name"
                      type="text"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.name
                      }
                      placeholder="Nama lengkap"
                    />
                    {Boolean(
                      touched.name
                    ) &&
                      Boolean(
                        errors.name
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {errors.name}
                        </div>
                      )}
                  </div>

                  <div className="form-group">
                    <Field
                      className="form-control"
                      name="username"
                      type="text"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.username
                      }
                      placeholder="Username UFace"
                    />
                    {Boolean(
                      touched.username
                    ) &&
                      Boolean(
                        errors.username
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {
                            errors.username
                          }
                        </div>
                      )}
                  </div>

                  <div className="form-group">
                    <Field
                      className="form-control"
                      type="email"
                      name="email"
                      placeholder="Email"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.email
                      }
                    />
                    {Boolean(
                      touched.email
                    ) &&
                      Boolean(
                        errors.email
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {errors.email}
                        </div>
                      )}
                  </div>
                  <div className="form-group">
                    <Field
                      className="form-control"
                      name="whatsapp"
                      type="text"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.whatsapp
                      }
                      placeholder="No whatsapp anda"
                    />
                    {Boolean(
                      touched.whatsapp
                    ) &&
                      Boolean(
                        errors.whatsapp
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {
                            errors.whatsapp
                          }
                        </div>
                      )}
                  </div>
                  <div className="form-group">
                    <Field
                      className="form-control"
                      id="psw-input"
                      name="password"
                      type="password"
                      placeholder="Masukkan Password"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.password
                      }
                    />
                    <div
                      className="position-absolute"
                      id="password-visibility"
                    >
                      <i className="bi bi-eye"></i>
                      <i className="bi bi-eye-slash"></i>
                    </div>
                    {Boolean(
                      touched.password
                    ) &&
                      Boolean(
                        errors.password
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {
                            errors.password
                          }
                        </div>
                      )}
                  </div>

                  <div className="form-group mb-3">
                    <Field
                      className="form-control"
                      name="changepassword"
                      type="password"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.changepassword
                      }
                      placeholder="Konfirmasi Password"
                    />
                    {Boolean(
                      touched.changepassword
                    ) &&
                      Boolean(
                        errors.changepassword
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {
                            errors.changepassword
                          }
                        </div>
                      )}
                  </div>

                  <button
                    className="btn btn-primary w-100"
                    type="submit"
                  >
                    Daftar
                  </button>
                </form>
              )}
            </Formik>
          </div>

          <div className="login-meta-data text-center">
            {/* <Link
              to="/lupa-password"
              className="stretched-link forgot-password d-block mt-3 mb-1"
            >
              Lupa Password ?
            </Link> */}
            <p className="mb-0">
              Sudah punya akun ?{" "}
              <Link
                className="stretched-link"
                to="/login"
              >
                Login
              </Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default RegisterPage;
