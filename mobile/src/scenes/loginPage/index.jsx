import React from "react";
import * as yup from "yup";
import { Formik, Field } from "formik";
import { useLoginUserMutation } from "state/api";
import { useDispatch } from "react-redux";
import { setLogin } from "state";
import {
  useNavigate,
  Link,
} from "react-router-dom";

const loginSchema = yup.object().shape({
  email: yup
    .string()
    .required("Email wajib diisi!"),
  password: yup
    .string()
    .required("Password wajib diisi!"),
});

const initialValues = {
  email: "",
  password: "",
};

const LoginPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [
    loginUser,
    { isLoading, isError, error, data },
  ] = useLoginUserMutation();

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    console.log(values);
    await loginUser(values);
    onSubmitProps.resetForm();
  };

  if (data) {
    dispatch(setLogin(data));
    navigate("/home");
  }

  return (
    <>
      {isLoading && (
        <div id="preloader">
          <div
            className="spinner-grow text-primary"
            role="status"
          >
            <span className="visually-hidden">
              Loading...
            </span>
          </div>
        </div>
      )}
      <div
        className="internet-connection-status"
        id="internetStatus"
      ></div>

      <div className="login-back-button">
        <Link to="/welcome">
          <svg
            className="bi bi-arrow-left-short"
            width="32"
            height="32"
            viewBox="0 0 16 16"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"
            ></path>
          </svg>
        </Link>
      </div>

      <div className="login-wrapper d-flex align-items-center justify-content-center">
        <div className="custom-container">
          <div className="text-center px-4">
            <img
              className="login-intro-img"
              src="assets/img/core-img/login.png"
              alt=""
            ></img>
          </div>

          <div className="register-form mt-4 mb-4">
            <h6 className="mb-3 text-center">
              Silahkan login untuk melanjutkan ke
              UfaceSkincare
            </h6>
            {isError && (
              <div
                className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                role="alert"
              >
                <i className="bi bi-x-circle"></i>
                {error.data.message}
              </div>
            )}
            <Formik
              onSubmit={
                handleFormSubmit
              }
              initialValues={
                initialValues
              }
              validationSchema={
                loginSchema
              }
            >
              {({
                values,
                errors,
                touched,
                handleBlur,
                handleChange,
                handleSubmit,
              }) => (
                <form
                  onSubmit={
                    handleSubmit
                  }
                >
                  <div className="form-group">
                    <Field
                      className="form-control"
                      type="email"
                      name="email"
                      placeholder="Email"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.email
                      }
                    />
                    {Boolean(
                      touched.email
                    ) &&
                      Boolean(
                        errors.email
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {errors.email}
                        </div>
                      )}
                  </div>
                  <div className="form-group position-relative">
                    <Field
                      className="form-control"
                      id="psw-input"
                      name="password"
                      type="password"
                      placeholder="Masukkan Password"
                      onChange={
                        handleChange
                      }
                      onBlur={
                        handleBlur
                      }
                      value={
                        values.password
                      }
                    />
                    <div
                      className="position-absolute"
                      id="password-visibility"
                    >
                      <i className="bi bi-eye"></i>
                      <i className="bi bi-eye-slash"></i>
                    </div>
                    {Boolean(
                      touched.password
                    ) &&
                      Boolean(
                        errors.password
                      ) && (
                        <div
                          className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <i className="bi bi-x-circle"></i>
                          {
                            errors.password
                          }
                        </div>
                      )}
                  </div>

                  <button
                    className="btn btn-primary w-100"
                    type="submit"
                  >
                    Login
                  </button>
                </form>
              )}
            </Formik>
          </div>

          <div className="login-meta-data text-center">

            {/* <Link
              to="/lupa-password"
              className="stretched-link forgot-password d-block mt-3 mb-1"
            >
              Lupa Password ?
            </Link> */}
            <p className="mb-0">Belum punya akun ? <Link className="stretched-link" to="/daftar">Daftar Sekarang!</Link></p>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
