import React from "react";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useGetUserQuery } from "state/api";

const ShowProfile = () => {
  const { data, isLoading } =
    useGetUserQuery();

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Profil Pengguna"
      />
      {data && (
        <div class="page-content-wrapper py-3">
          <div class="container">
            <div class="card user-info-card mb-3">
              <div class="card-body d-flex align-items-center">
                <div class="user-profile me-3">
                  <img
                    src={
                      data.photo
                        ? data.photo
                        : "/assets/img/core-img/user.png"
                    }
                    alt=""
                  />
                </div>
                <div class="user-info">
                  <div class="d-flex align-items-center">
                    <h5 class="mb-1">
                      {data.name}
                    </h5>
                  </div>
                  <p class="mb-0">
                    {data.status}
                  </p>
                </div>
              </div>
            </div>

            <div class="card user-data-card">
              <div class="card-body">
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="Username"
                  >
                    Username
                  </label>
                  <input
                    class="form-control"
                    id="Username"
                    type="text"
                    value={
                      data.username
                    }
                    placeholder="Username"
                    readonly
                  />
                </div>
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="fullname"
                  >
                    Nama Lengkap
                  </label>
                  <input
                    class="form-control"
                    id="fullname"
                    type="text"
                    value={data.name}
                    placeholder="Full Name"
                    readonly
                  />
                </div>
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="email"
                  >
                    Alamat Email
                  </label>
                  <input
                    class="form-control"
                    id="email"
                    type="text"
                    value={data.email}
                    placeholder="Email Address"
                    readonly
                  />
                </div>
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="job"
                  >
                    Status
                  </label>
                  <input
                    class="form-control"
                    id="job"
                    type="text"
                    value={data.status}
                    readonly
                  />
                </div>
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="portfolio"
                  >
                    No Whatsapp
                  </label>
                  <input
                    class="form-control"
                    id="portfolio"
                    type="url"
                    value={
                      data.whatsapp
                    }
                    readonly
                  />
                </div>
                <div class="form-group mb-3">
                  <label
                    class="form-label"
                    for="address"
                  >
                    Alamat
                  </label>
                  <input
                    class="form-control"
                    id="address"
                    type="text"
                    value={data.address}
                    readonly
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ShowProfile;
