import React from "react";
import { Link } from "react-router-dom";
import {
  useSelector,
  useDispatch,
} from "react-redux";
import Header from "components/Header";
import { setLogout } from "state";
import { useGetUserQuery } from "state/api";

const ProfilePage = () => {
  const dispatch = useDispatch();
  const { data, isLoading } =
    useGetUserQuery();
  const { name, image, status } =
    useSelector(
      (state) => state.auth.user
    );

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <Header />
      <div className="page-content-wrapper py-3">
        {data && (
          <div className="container">
            <div className="card user-info-card mb-3">
              <div className="card-body d-flex align-items-center">
                <div className="user-profile me-3">
                  <img
                    src={
                      data.photo
                        ? data.photo
                        : "assets/img/core-img/user.png"
                    }
                    alt=""
                  />
                </div>
                <div className="user-info">
                  <div className="d-flex align-items-center">
                    <h5 className="mb-1">
                      {data.name}
                    </h5>
                  </div>
                  <p className="mb-0">
                    {data.status}
                  </p>
                </div>
              </div>
            </div>
            <div className="card mb-3 shadow-sm">
              <div className="card-body direction-rtl">
                <p>Profil Pengguna</p>
                <div className="single-setting-panel">
                  <Link to="/profil/lihat">
                    <div className="icon-wrapper bg-success">
                      <i className="bi bi-person"></i>
                    </div>
                    Lihat Profil
                    Pengguna
                  </Link>
                </div>
                <div className="single-setting-panel">
                  <Link to="/profil/update">
                    <div className="icon-wrapper">
                      <i className="bi bi-pencil"></i>
                    </div>
                    Update Profil
                    Pengguna
                  </Link>
                </div>
                <div className="single-setting-panel">
                  <Link to="/profil/update-password">
                    <div className="icon-wrapper bg-info">
                      <i className="bi bi-lock"></i>
                    </div>
                    Ubah Password
                  </Link>
                </div>
                {/* <div className="single-setting-panel">
                <Link to="page-privacy-policy.html">
                  <div className="icon-wrapper bg-danger">
                    <i className="bi bi-shield-lock"></i>
                  </div>
                  Privacy Policy
                </Link>
              </div> */}
                <div className="single-setting-panel">
                  <Link
                    onClick={() =>
                      dispatch(
                        setLogout()
                      )
                    }
                  >
                    <div className="icon-wrapper bg-danger">
                      <i className="bi bi-box-arrow-right"></i>
                    </div>
                    Logout
                  </Link>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default ProfilePage;
