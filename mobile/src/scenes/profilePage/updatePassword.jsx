import React from "react";
import { Formik, Field } from "formik";
import * as yup from "yup";
import { useUpdateUserPasswordMutation } from "state/api";
import BreadcrumbHeader from "components/BreadcrumbHeader";

const updatePasswordSchema = yup
  .object()
  .shape({
    password: yup
      .string()
      .required("Password wajib diisi!")
      .min(
        6,
        "Password minimal 6 karakter"
      ),
    changepassword: yup
      .string()
      .required()
      .oneOf(
        [yup.ref("password"), null],
        "Kedua password harus sama persis!"
      ),
  });

const initialValues = {
  password: "",
  changepassword: "",
};

const UpdatePassword = () => {
  const [
    updateUserPassword,
    { isLoading, data },
  ] = useUpdateUserPasswordMutation();

  const postUpdatePassword = async (
    values,
    onSubmitProps
  ) => {
    updateUserPassword(values);
  };

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postUpdatePassword(
      values,
      onSubmitProps
    );
  };

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Update Password"
      />
      <Formik
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        validationSchema={
          updatePasswordSchema
        }
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="page-content-wrapper py-3">
              <div className="container">
                <div className="card user-data-card">
                  <div className="card-body">
                    {data && (
                      <div
                        className="alert mt-3 custom-alert-1 alert-success alert-dismissible fade show"
                        role="alert"
                      >
                        <i class="bi bi-check-circle"></i>{" "}
                        Password
                        berhasil diubah!
                      </div>
                    )}
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="fullname"
                      >
                        Password Baru
                      </label>
                      <Field
                        className="form-control"
                        name="password"
                        type="password"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.password
                        }
                        placeholder="Password Baru"
                      />
                      {Boolean(
                        touched.password
                      ) &&
                        Boolean(
                          errors.password
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.password
                            }
                          </div>
                        )}
                    </div>

                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="fullname"
                      >
                        Password Baru
                      </label>
                      <Field
                        className="form-control"
                        name="changepassword"
                        type="password"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.changepassword
                        }
                        placeholder="Konfirmasi Password Baru"
                      />
                      {Boolean(
                        touched.changepassword
                      ) &&
                        Boolean(
                          errors.changepassword
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.changepassword
                            }
                          </div>
                        )}
                    </div>

                    <button
                      className="btn btn-success w-100"
                      type="submit"
                    >
                      Ganti Password
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
};

export default UpdatePassword;
