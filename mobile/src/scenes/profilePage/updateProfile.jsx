import React from "react";
import { Formik, Field } from "formik";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";
import Dropzone from "react-dropzone";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import {
  useGetUserQuery,
  useUpdateUserMutation,
} from "state/api";

const userSchema = yup.object().shape({
  name: yup
    .string()
    .required(
      "Nama pengguna wajib diisi!"
    ),
  email: yup
    .string()
    .required(
      "Email pengguna wajib diisi!"
    ),
  whatsapp: yup
    .string()
    .required(
      "Nomor whatsapp pengguna wajib diisi!"
    ).matches(/^(\+62|62)8[1-9][0-9]{6,9}$/, "Format nomor whatsapp wajib menggunakan prefix 62, Contoh : 628525xxxxx"),
  address: yup
    .string()
    .required(
      "Alamat pengguna wajib diisi!"
    ),
  image: yup
    .mixed()
    .optional()
    .test(
      "fileSize",
      "Ukuran Foto Maksimal 1MB ",
      (value) => {
        if (!value) return true;
        return (
          value && value.size <= 1024 * 1024
        );
      }
    )
    .test(
      "type",
      "Pastikan foto anda sesuai format: .jpeg, .jpg, .bmp",
      (value) => {
        if (!value) return true;
        return (
          value &&
          (value.type ===
            "image/jpeg" ||
            value.type ===
              "image/bmp" ||
            value.type ===
              "image/png")
        );
      }
    ),
});

const initialValues = {
  name: "",
  email: "",
  whatsapp: "",
  address: "",
  image: "",
};

const UpdateProfile = () => {
  const navigate = useNavigate();
  const [
    updateUser,
    {
      isLoading: isUpdateProfileLoading,
    },
  ] = useUpdateUserMutation();

  const { data, isLoading } =
    useGetUserQuery();

  if (data) {
    initialValues.name =
      data.name || "";
    initialValues.email =
      data.email || "";
    initialValues.whatsapp =
      data.whatsapp || "";
    initialValues.address =
      data.address || "";
    initialValues.image = "";
  }

  const postUpdateProfile = async (
    values,
    onSubmitProps
  ) => {
    const formData = new FormData();

    const userId = data.id;
    for (let value in values) {
      formData.append(
        value,
        values[value]
      );
    }

    formData.append(
      "image",
      values.image.name
    );

    formData.append("userId", userId);

    updateUser(formData);

    if (!isUpdateProfileLoading) {
      navigate("/profil/lihat");
    }
  };

  if (
    isLoading ||
    isUpdateProfileLoading
  ) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postUpdateProfile(
      values,
      onSubmitProps
    );
  };

  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Update Profil"
      />
      <Formik
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        validationSchema={userSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
          resetForm,
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="page-content-wrapper py-3">
              <div className="container">
                <div className="card user-info-card mb-3">
                  <div class="card-body d-flex align-items-center">
                    <div class="user-profile me-3">
                      <img
                        src={
                          data.photo
                            ? data.photo
                            : "/assets/img/core-img/user.png"
                        }
                        alt=""
                      />
                    </div>
                    <div class="user-info">
                      <div class="d-flex align-items-center">
                        <h5 class="mb-1">
                          {data.name}
                        </h5>
                        <span class="badge bg-warning ms-2 rounded-pill">
                          {data.status}
                        </span>
                      </div>
                      <p class="mb-0">
                        Member BOB
                      </p>
                    </div>
                  </div>
                </div>

                <div className="card user-data-card">
                  <div className="card-body">
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="image"
                      >
                        Foto Profil
                      </label>
                      <Dropzone
                        acceptedFiles=".jpg,.jpeg,.png"
                        multiple={false}
                        onDrop={(
                          acceptedFiles
                        ) =>
                          setFieldValue(
                            "image",
                            acceptedFiles[0]
                          )
                        }
                      >
                        {({
                          getRootProps,
                          getInputProps,
                        }) => (
                          <div
                            className="file-upload-card"
                            {...getRootProps()}
                          >
                            <div className="form-file">
                              {!values.image ? (
                                <div>
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="30"
                                    height="30"
                                    fill="currentColor"
                                    className="bi bi-person-bounding-box"
                                    viewBox="0 0 16 16"
                                  >
                                    <path d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1h-3zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5zM.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5z" />
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                  </svg>
                                  <h5 className="mt-2 mb-4">
                                    Upload
                                    Foto
                                    Profil
                                  </h5>
                                  <input
                                    className="form-control d-none"
                                    {...getInputProps()}
                                  />
                                  <label
                                    className="form-file-label justify-content-center"
                                    for="customFile"
                                  >
                                    <span className="form-file-button btn btn-primary shadow w-100">
                                      Upload
                                      File
                                    </span>
                                  </label>

                                  <h6 className="mt-4 mb-0">
                                    File
                                    yang
                                    didukung
                                  </h6>
                                  <small>
                                    .jpg
                                    .png
                                    .jpeg
                                  </small>
                                </div>
                              ) : (
                                <div>
                                  <h5>
                                    {
                                      values
                                        .image
                                        .name
                                    }
                                  </h5>
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      </Dropzone>
                      {Boolean(
                        touched.image
                      ) &&
                        Boolean(
                          errors.image
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.image
                            }
                          </div>
                        )}
                    </div>
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="fullname"
                      >
                        Nama Lengkap
                      </label>
                      <Field
                        className="form-control"
                        name="name"
                        type="text"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.name
                        }
                        placeholder="Nama lengkap"
                      />
                      {Boolean(
                        touched.name
                      ) &&
                        Boolean(
                          errors.name
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.name
                            }
                          </div>
                        )}
                    </div>
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="email"
                      >
                        Alamat Email
                      </label>
                      <input
                        className="form-control"
                        name="email"
                        type="email"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.email
                        }
                        placeholder="Alamat email"
                      />
                      {Boolean(
                        touched.email
                      ) &&
                        Boolean(
                          errors.email
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.email
                            }
                          </div>
                        )}
                    </div>
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="portfolio"
                      >
                        No Whatsapp
                      </label>
                      <Field
                        className="form-control"
                        name="whatsapp"
                        type="text"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.whatsapp
                        }
                        placeholder="No whatsapp anda"
                      />
                      {Boolean(
                        touched.whatsapp
                      ) &&
                        Boolean(
                          errors.whatsapp
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.whatsapp
                            }
                          </div>
                        )}
                    </div>
                    <div className="form-group mb-3">
                      <label
                        className="form-label"
                        for="address"
                      >
                        Alamat
                      </label>
                      <Field
                        className="form-control"
                        name="address"
                        type="text"
                        onChange={
                          handleChange
                        }
                        onBlur={
                          handleBlur
                        }
                        value={
                          values.address
                        }
                        placeholder="Alamat"
                      />
                      {Boolean(
                        touched.address
                      ) &&
                        Boolean(
                          errors.address
                        ) && (
                          <div
                            className="alert mt-3 custom-alert-1 alert-danger alert-dismissible fade show"
                            role="alert"
                          >
                            <i className="bi bi-x-circle"></i>
                            {
                              errors.address
                            }
                          </div>
                        )}
                    </div>
                    <button
                      className="btn btn-success w-100"
                      type="submit"
                    >
                      Update
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
};

export default UpdateProfile;
