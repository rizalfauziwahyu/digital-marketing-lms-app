import Navbar from "components/Navbar";
import Sidebar from "components/Sidebar";
import { Outlet } from "react-router-dom";
import React from "react";

const Layout = () => {
  return (
    <>
      <Sidebar />
      <Outlet />
      <Navbar />
    </>
  );
};

export default Layout;
