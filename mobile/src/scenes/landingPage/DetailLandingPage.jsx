import React, { useState } from "react";
import {
  Link,
  useParams,
} from "react-router-dom";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useGetLandingPageQuery } from "state/api";
import { useGetUserQuery } from "state/api";
import { ShareSocial } from "react-share-social";

const DetailLandingPage = () => {
  const { slug } = useParams();
  const {
    data: user,
    isLoading: userIsLoading,
  } = useGetUserQuery();
  const [isCopied, setIsCopied] =
    useState(false);

  const { data, isLoading } =
    useGetLandingPageQuery(slug);

  async function copyTextToClipboard(
    text
  ) {
    if ("clipboard" in navigator) {
      return await navigator.clipboard.writeText(
        text
      );
    } else {
      return document.execCommand(
        "copy",
        true,
        text
      );
    }
  }

  // onClick handler function for the copy button
  const handleCopyClick = (
    copyText
  ) => {
    // Asynchronously call copyTextToClipboard
    copyTextToClipboard(copyText)
      .then(() => {
        // If successful, update the isCopied state value
        setIsCopied(true);
        setTimeout(() => {
          setIsCopied(false);
        }, 3000);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  if (isLoading || userIsLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        title="Detail Landing Page"
        route={-1}
      />
      <div className="page-content-wrapper py-3">
        {data && (
          <div className="container">
            <div class="card position-relative shadow-sm mb-3">
              <img
                class="card-img-top"
                src={data.image}
                alt={`gambar-${data.slug}`}
              />
            </div>
            <div className="card product-details-card mb-3 direction-rtl">
              <div className="card-body">
                <h3>{data.title}</h3>

                <div
                  dangerouslySetInnerHTML={{
                    __html:
                      data.description,
                  }}
                />

                <ShareSocial
                  url={`${data.url}/?u=${user.username}`}
                  socialTypes={[
                    "facebook",
                    "twitter",
                    "whatsapp",
                    "telegram",
                  ]}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailLandingPage;
