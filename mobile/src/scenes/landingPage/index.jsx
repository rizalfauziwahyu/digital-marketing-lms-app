import React from "react";
import { useDispatch } from "react-redux";
import { setLogout } from "state";
import { Link } from "react-router-dom";
import { useGetLandingPagesQuery } from "state/api";
import Header from "components/Header";

const LandingPages = () => {
    const dispatch = useDispatch();
    const { data, isLoading, error, isError } = useGetLandingPagesQuery();

    if (isLoading) {
        return (
            <div id="preloader">
                <div className="spinner-grow text-primary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        );
    }

    if (isError && error.status === 401) {
        dispatch(setLogout());
    }

    return (
        <>
            <Header />
            <div class="page-content-wrapper py-3">
                <div class="container">
                    {data && data.length == 0 && (
                        <>
                            <img
                                class="card-img-top"
                                src="assets/img/core-img/coming-soon.jpg"
                                alt={`gambar-coming-soon`}
                            />
                        </>
                    )}
                    {data &&
                        data.map(({ slug, title, image }, index) => (
                            <Link key={index} to={`/landing-page/${slug}`}>
                                <div class="card position-relative shadow-sm mb-3">
                                    <img
                                        class="card-img-top"
                                        src={image}
                                        alt={`gambar-${slug}`}
                                    />
                                    <span class="badge bg-warning text-dark position-absolute card-badge">
                                        {title}
                                    </span>
                                </div>
                            </Link>
                        ))}
                </div>
            </div>
        </>
    );
};

export default LandingPages;
