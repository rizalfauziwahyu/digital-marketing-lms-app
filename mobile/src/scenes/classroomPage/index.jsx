import React from "react";
import Header from "components/Header";
import { Link } from "react-router-dom";
import { useGetClassroomsQuery } from "state/api";

const ClassroomPage = () => {
  const { data, isLoading } =
    useGetClassroomsQuery();

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }
  return (
    <>
      <Header />
      <div className="page-content-wrapper py-3">
        <div className="top-products-area">
          <div className="container">
            <div className="row g-3">
              {data &&
                data.map(
                  ({
                    slug,
                    title,
                    image,
                    enrolled,
                  }) => (
                    <div
                      key={slug}
                      className="col-12 col-sm-6 col-lg-4"
                    >
                      <div className="card single-product-card">
                        <div className="card-body p-3">
                          <Link
                            className="product-thumbnail d-block"
                            to={`/kelas-marketing/${slug}`}
                          >
                            <img
                              src={
                                image
                              }
                              alt={`Gambar ${slug}`}
                            />
                            {enrolled && (
                              <span className="badge bg-warning">
                                Gratis
                              </span>
                            )}
                          </Link>
                          <Link
                            className="product-title d-block"
                            to={`/kelas-marketing/${slug}`}
                          >
                            {title}
                          </Link>
                          {enrolled ? (
                            <>
                              <p className="sale-price">
                                Belajar
                                Sekarang
                              </p>
                            </>
                          ) : (
                            <p className="sale-price">
                              Gratis
                              <span>
                                Rp.
                                200.000
                              </span>
                            </p>
                          )}
                        </div>
                      </div>
                    </div>
                  )
                )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ClassroomPage;
