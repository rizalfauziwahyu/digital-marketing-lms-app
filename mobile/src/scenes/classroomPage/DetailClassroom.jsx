import React from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useGetClassroomBySlugQuery } from "state/api";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useEnrollClassroomMutation } from "state/api";

const DetailClassroom = () => {
  const { slug } = useParams();
  console.log("this.context:", slug);
  const { data, isLoading } =
    useGetClassroomBySlugQuery(slug);

  const [
    enrollClassroom,
    enrollResponse,
  ] = useEnrollClassroomMutation();

  const handleEnrollClassroom = (
    slug
  ) => {
    enrollClassroom(slug);
  };

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  if (enrollResponse.isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }
  return (
    <>
      <BreadcrumbHeader
        route={-1}
        title="Kelas Marketing"
      />
      <div className="page-content-wrapper py-3">
        {data && (
          <div className="container">
            <div className="card position-relative shadow-sm mb-3">
              <img
                className="card-img-top"
                src={data.image}
                alt={`gambar-${slug}`}
              />
            </div>
            <div className="card product-details-card mb-3 direction-rtl">
              <div className="card-body">
                {enrollResponse.data && (
                  <div
                    class="alert custom-alert-1 alert-success alert-dismissible fade show"
                    role="alert"
                  >
                    <i class="bi bi-check-circle"></i>
                    Selamat, kamu bisa
                    belajar sekarang!
                    <button
                      class="btn btn-close position-relative p-1 ms-auto"
                      type="button"
                      data-bs-dismiss="alert"
                      aria-label="Close"
                    ></button>
                  </div>
                )}

                <div className="row gx-2 align-items-end">
                  <div
                    className={
                      data.enrolled
                        ? "col-12"
                        : "col-8"
                    }
                  >
                    <div className="image-gallery-text mb-4">
                      <h3 className="mb-0">
                        {data.title}
                      </h3>
                    </div>
                  </div>
                  {!data.enrolled ? (
                    <div className="col-4 text-end">
                      {enrollResponse.isLoading ? (
                        <Link
                          className="btn btn-primary btn-sm mb-4"
                          to="#"
                        >
                          <div
                            class="spinner-border text-light"
                            role="status"
                          >
                            <span class="visually-hidden">
                              Loading...
                            </span>
                          </div>
                        </Link>
                      ) : (
                        <Link
                          className="btn btn-primary btn-sm mb-4"
                          onClick={() =>
                            handleEnrollClassroom(
                              slug
                            )
                          }
                        >
                          Ambil Kelas
                          Ini
                        </Link>
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <h5 className="mb-3">
                  {data.enrolled ? (
                    <>
                      Yuk pelajari
                      materi dibawah
                      ini!
                    </>
                  ) : (
                    <>
                      Yang akan anda
                      pelajari
                    </>
                  )}
                </h5>
                <ul
                  className={`list-group ${
                    data.enrolled &&
                    "list-group-flush"
                  } mb-3`}
                >
                  {data.moduleList &&
                    data.moduleList.map(
                      (
                        module,
                        index
                      ) => {
                        if (
                          data.enrolled
                        ) {
                          return (
                            <Link
                              class="list-group-item list-group-item-action active"
                              to={`/belajar/${module.slug}`}
                            >
                              {
                                module.title
                              }
                            </Link>
                          );
                        }
                        return (
                          <li
                            key={index}
                            className="list-group-item"
                          >
                            {
                              module.title
                            }
                          </li>
                        );
                      }
                    )}
                </ul>
                <h5>Deskripsi Kelas</h5>
                <div
                  dangerouslySetInnerHTML={{
                    __html:
                      data.description,
                  }}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailClassroom;
