import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import BreadcrumbHeader from "components/BreadcrumbHeader";
import { useGetModuleBySlugQuery } from "state/api";

const DetailModule = () => {
  const { slug } = useParams();
  const [
    isAttachmentDownloaded,
    setIsAttachmentDownloaded,
  ] = useState(false);

  const token = useSelector(
    (state) => state.auth.token
  );

  const { data, isLoading } =
    useGetModuleBySlugQuery(slug);

  const handleDownloadAttachment =
    () => {
      fetch(
        process.env.REACT_APP_BASE_URL +
          `/api/contents/download/${slug}`,
        {
          method: "GET",
          headers: {
            "Content-Type":
              "application/pdf",
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((response) =>
          response.blob()
        )
        .then((blob) => {
          // Create blob link to download
          const url =
            window.URL.createObjectURL(
              new Blob([blob])
            );
          const link =
            document.createElement("a");
          link.href = url;
          link.setAttribute(
            "download",
            `${data.image.filename}`
          );

          // Append to html link element page
          document.body.appendChild(
            link
          );

          // Start download
          link.click();

          // Clean up and remove the link
          link.parentNode.removeChild(
            link
          );

          setIsAttachmentDownloaded(
            true
          );
          setTimeout(() => {
            setIsAttachmentDownloaded(
              false
            );
          }, 3000);
        });
    };

  if (isLoading) {
    return (
      <div id="preloader">
        <div
          className="spinner-grow text-primary"
          role="status"
        >
          <span className="visually-hidden">
            Loading...
          </span>
        </div>
      </div>
    );
  }

  return (
    <>
      <BreadcrumbHeader
        title="Modul Kelas"
        route={-1}
      />
      <div className="page-content-wrapper py-3">
        {data && (
          <div className="container">
            <div className="card position-relative shadow-sm mb-3">
              <img
                className="card-img-top"
                src={data.image}
                alt={`gambar-${slug}`}
              />
            </div>
            <div className="card product-details-card mb-3 direction-rtl">
              <div className="card-body">
                <h3>{data.title}</h3>

                {data.video && (
                  <>
                    <div className="ratio ratio-16x9 mb-4">
                      <iframe
                        width="2543"
                        height="1063"
                        src={`https://www.youtube.com/embed/${data.video}`}
                        title="Belajar Seputar Algoritma Tiktok yang Wajib kamu Ketahui!"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen
                      ></iframe>
                    </div>
                  </>
                )}

                <div
                  dangerouslySetInnerHTML={{
                    __html:
                      data.content,
                  }}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailModule;
