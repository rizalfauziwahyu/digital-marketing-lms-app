import React from "react";
import { Link } from "react-router-dom";

const WaitingVerificationPage = () => {
  return (
    <>
      <div
        className="internet-connection-status"
        id="internetStatus"
      ></div>

      <div className="login-back-button">
        <Link to="/welcome">
          <svg
            className="bi bi-arrow-left-short"
            width="32"
            height="32"
            viewBox="0 0 16 16"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"
            ></path>
          </svg>
        </Link>
      </div>

      <div className="login-wrapper d-flex align-items-center justify-content-center">
        <div className="custom-container">
          <div className="text-center px-4">
            <img
              className="login-intro-img"
              src="assets/img/core-img/verification-process.jpg"
              alt=""
            ></img>
          </div>

          <div className="register-form mt-4 mb-4">
            <h2>
              Akun kamu sedang kami
              verifikasi
            </h2>
            <h5>
              Kamu akan segera
              mendapatkan notifikasi
              melalui nomer <strong>whatsapp</strong>{" "}     
              kamu, ketika akun sudah
              selesai di verifikasi
              oleh Admin
            </h5>
          </div>
        </div>
      </div>
    </>
  );
};

export default WaitingVerificationPage;
