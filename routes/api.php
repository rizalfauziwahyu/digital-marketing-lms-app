<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ClassroomController;
use App\Http\Controllers\Api\ContentCategoryController;
use App\Http\Controllers\Api\ContentController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\LandingPageController;
use App\Http\Controllers\Api\LearningModuleController;
use App\Http\Controllers\Api\ProductKnowledgeController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::middleware('auth:api')->group(function () {
        Route::get('refresh', [AuthController::class, 'refresh']);
    });
});

Route::get('/username/{username}', [UserController::class, 'getUserByUsername']);

Route::middleware('auth:api')->group(function () {
    Route::prefix('contents')->group(function () {
        Route::apiResource('categories', ContentCategoryController::class);
    });
    Route::get('/contents/category/{category}', [ContentCategoryController::class, 'getContentsByCategory']);
    Route::get('/contents/download/{content}', [ContentController::class, 'download']);
    Route::get('/contents/download-video/{content}', [ContentController::class, 'downloadVideo']);
    Route::apiResource('contents', ContentController::class);
    
    Route::apiResource('classrooms', ClassroomController::class);
    Route::post('classrooms/{classroom}/enroll', [ClassroomController::class, 'enroll']);
    
    Route::apiResource('modules', LearningModuleController::class);
    
    Route::apiResource('events', EventController::class);
    Route::post('events/{event}/register', [EventController::class, 'registerEvent']);
    
    Route::apiResource('product-knowledges', ProductKnowledgeController::class);
    Route::get('/product-knowledges/download/{product_knowledge}', [ProductKnowledgeController::class, 'downloadAttachment']);
    
    Route::apiResource('landing-pages', LandingPageController::class);

    Route::get('/user/classrooms', [UserController::class, 'myClassrooms']);
    Route::get('/user/profile', [UserController::class, 'myProfile']);
    Route::put('/user/update-password', [UserController::class, 'updatePassword']);
    Route::post('/user/{user}/approve', [UserController::class, 'approveUser']);
    
    Route::get('/users', [UserController::class, 'index']);
    Route::put('/users/{user}', [UserController::class, 'updateProfile']);

});
