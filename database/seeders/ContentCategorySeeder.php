<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ContentCategory;
use App\Models\Content;

class ContentCategorySeeder extends Seeder
{
    public function run()
    {
        ContentCategory::create([
            'name' => 'Gambar Promosi',
            'description' => 'Kategori ini berisi gambar promosi'
        ]);

        ContentCategory::create([
            'name' => 'Video Promosi',
            'description' => 'Kategori ini berisi video promosi'
        ]);

        ContentCategory::create([
            'name' => 'Copywriting Promosi',
            'description' => 'Kategori ini berisi copywriting promosi'
        ]);
    }
}
