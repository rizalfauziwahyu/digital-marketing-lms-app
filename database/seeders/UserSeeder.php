<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = User::create([
            'name' => 'Super Admin',
            'username' => 'superadmin99',
            'email' => 'superadmin@bahagiaakademi.id',
            'password' => Hash::make('superadmin99'),
        ]);
        $superAdmin->assignRole('Super Admin');

        $admin = User::create([
            'name' => 'Admin',
            'username' => 'admin99',
            'email' => 'admin@bahagiaakademi.id',
            'password' => Hash::make('admin99'),
        ]);
        $admin->assignRole('Staff');

        $member = User::create([
            'name' => 'member',
            'username' => 'member99',
            'email' => 'member@bahagiaakademi.id',
            'password' => Hash::make('member99'),
        ]);
        $member->assignRole('Member');
    }   
}
