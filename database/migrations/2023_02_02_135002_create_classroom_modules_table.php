<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Classroom;
use App\Models\LearningModule;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_modules', function (Blueprint $table) {
            $table->foreignIdFor(Classroom::class)->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignIdFor(LearningModule::class)->constrained('learning_modules')->onUpdate('cascade')->onDelete('cascade');

            $table->index(['classroom_id', 'learning_module_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_modules');
    }
};
