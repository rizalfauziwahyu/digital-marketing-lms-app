import { Typography, Box, Button, useTheme } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import FlexBetween from "./FlexBetween";

const Header = ({ title, subtitle, buttonTitle, navigatePath }) => {
    const theme = useTheme();
    const navigate = useNavigate();
    return (
        <FlexBetween>
            <Box>
                <Typography
                    variant="h2"
                    color={theme.palette.secondary[100]}
                    fontWeight="bold"
                    sx={{ mb: "5px" }}
                >
                    {title}
                </Typography>
                <Typography variant="h5" color={theme.palette.secondary[300]}>
                    {subtitle}
                </Typography>
            </Box>
            {buttonTitle && (
                <Box>
                    <Button
                        sx={{
                            backgroundColor: theme.palette.secondary[300],
                            color: theme.palette.primary[600],
                        }}
                        onClick={() => {
                            navigate(navigatePath);
                        }}
                    >
                        <Typography>{buttonTitle}</Typography>
                    </Button>
                </Box>
            )}
        </FlexBetween>
    );
};

export default Header;
