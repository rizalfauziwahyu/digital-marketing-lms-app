import React from "react";
import {
    Box,
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Typography,
    useTheme,
} from "@mui/material";

import {
    VideocamOutlined,
    SchoolOutlined,
    LibraryBooksOutlined,
    EventOutlined,
    ChevronLeft,
    ChevronRightOutlined,
    HomeOutlined,
    Person2Outlined,
    AdminPanelSettingsOutlined,
    CategoryOutlined,
    PublicOutlined
} from "@mui/icons-material";

import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setIsSidebarOpen } from "state";
import FlexBetween from "./FlexBetween";

const navItems = [
    {
        text: "Dashboard",
        route: "dashboard",
        icon: <HomeOutlined />,
    },
    {
        text: "Konten Marketing",
        icon: null,
    },
    {
        text: "Kategori Konten",
        route: "kategori-konten",
        icon: <CategoryOutlined />,
    },
    {
        text: "Konten",
        route: "konten",
        icon: <VideocamOutlined />,
    },
    {
        text: "Product Knowledge",
        route: "product-knowledges",
        icon: <LibraryBooksOutlined />,
    },
    {
        text: "Kelas Marketing",
        icon: null,
    },
    {
        text: "Ruang Kelas",
        route: "kelas",
        icon: <SchoolOutlined />,
    },
    {
        text: "Modul Belajar",
        route: "materi-belajar",
        icon: <LibraryBooksOutlined />,
    },
    {
        text: "Event Marketing",
        icon: null,
    },
    {
        text: "Event",
        route: "acara",
        icon: <EventOutlined />,
    },
    {
        text: "Website Marketing",
        icon: null,
    },
    {
        text: "Landing Page",
        icon: <PublicOutlined />,
    },
    {
        text: "Management",
        icon: null,
    },
    {
        text: "Member",
        route: "member",
        icon: <Person2Outlined />,
    },
    {
        text: "Setting",
        icon: <AdminPanelSettingsOutlined />,
    },
];

const Sidebar = ({ user, drawerWidth, isNonMobile }) => {
    const dispatch = useDispatch();
    const isSidebarOpen = useSelector((state) => state.global.isSidebarOpen);
    const { pathname } = useLocation();
    const [active, setActive] = useState("");
    const navigate = useNavigate();
    const theme = useTheme();

    useEffect(() => {
        setActive(pathname.substring(1));
    }, [pathname]);

    return (
        <Box component="nav">
            {isSidebarOpen && (
                <Drawer
                    open={isSidebarOpen}
                    onClose={() => dispatch(setIsSidebarOpen())}
                    variant="persistent"
                    anchor="left"
                    sx={{
                        width: drawerWidth,
                        "& .MuiDrawer-paper": {
                            color: theme.palette.secondary[200],
                            backgroundColor: theme.palette.background.alt,
                            boxSixing: "border-box",
                            borderWidth: isNonMobile ? 0 : "2px",
                            width: drawerWidth,
                        },
                    }}
                >
                    <Box width="100%">
                        <Box m="1.5rem 2rem 2rem 3rem">
                            <FlexBetween color={theme.palette.secondary.main}>
                                <Box
                                    display="flex"
                                    alignItems="center"
                                    gap="0.5rem"
                                >
                                    <Typography variant="h4" fontWeight="bold">
                                        BAHAGIAKADEMI
                                    </Typography>
                                </Box>
                                {!isNonMobile && (
                                    <IconButton
                                        onClick={() =>
                                            dispatch(setIsSidebarOpen())
                                        }
                                    >
                                        <ChevronLeft />
                                    </IconButton>
                                )}
                            </FlexBetween>
                        </Box>
                        <List>
                            {navItems.map(({ text, route, icon }) => {
                                if (!icon) {
                                    return (
                                        <Typography
                                            key={text}
                                            sx={{ m: "2.25rem 0 1rem 3rem" }}
                                        >
                                            {text}
                                        </Typography>
                                    );
                                }
                                
                                return (
                                    <ListItem key={text} disablePadding>
                                        <ListItemButton
                                            onClick={() => {
                                                navigate(`/${route}`);
                                                setActive(route);
                                            }}
                                            sx={{
                                                backgroundColor:
                                                    active === route
                                                        ? theme.palette
                                                              .secondary[300]
                                                        : "transparent",
                                                color:
                                                    active === route
                                                        ? theme.palette
                                                              .primary[600]
                                                        : theme.palette
                                                              .secondary[100],
                                            }}
                                        >
                                            <ListItemIcon
                                                sx={{
                                                    ml: "2rem",
                                                    color:
                                                        active === route
                                                            ? theme.palette
                                                                  .primary[600]
                                                            : theme.palette
                                                                  .secondary[200],
                                                }}
                                            >
                                                {icon}
                                            </ListItemIcon>
                                            <ListItemText primary={text} />
                                            {active === route && (
                                                <ChevronRightOutlined
                                                    sx={{ ml: "auto" }}
                                                />
                                            )}
                                        </ListItemButton>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </Box>
                </Drawer>
            )}
        </Box>
    );
};

export default Sidebar;
