import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { themeSettings } from "theme";
import Layout from "scenes/layout";
import LoginPage from "scenes/loginPage";
import Dashboard from "scenes/dashboard";
import ContentCategories from "scenes/contentCategory";
import CreateContentCategory from "scenes/contentCategory/create";
import Contents from "scenes/content";
import CreateContent from "scenes/content/Create";
import Classrooms from "scenes/classroom";
import CreateClassroom from "scenes/classroom/create";
import Modules from "scenes/learningModule";
import CreateModule from "scenes/learningModule/create";
import Events from "scenes/event";
import CreateEvent from "scenes/event/create";
import ProductKnowledges from "scenes/productKnowledge";
import CreateProductKnowledge from "scenes/productKnowledge/Create";
import Users from "scenes/user";

function App() {
    const mode = useSelector((state) => state.global.mode);
    const theme = useMemo(() => createTheme(themeSettings(mode)), [mode]);
    const isAuth = Boolean(useSelector((state) => state.global.token));

    return (
        <div className="app">
            <BrowserRouter>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <Routes>
                        <Route path="/" element={<LoginPage />} />
                        <Route
                            element={
                                isAuth ? (
                                    <Layout />
                                ) : (
                                    <Navigate to="/" replace />
                                )
                            }
                        >
                            <Route path="/dashboard" element={<Dashboard />} />
                            <Route
                                path="/kategori-konten"
                                element={<ContentCategories />}
                            />
                            <Route
                                path="/buat-kategori-konten"
                                element={<CreateContentCategory />}
                            />
                            <Route path="/konten" element={<Contents />} />
                            <Route
                                path="/buat-konten"
                                element={<CreateContent />}
                            />
                            <Route path="/kelas" element={<Classrooms />} />
                            <Route
                                path="/buat-kelas"
                                element={<CreateClassroom />}
                            />
                            <Route
                                path="/materi-belajar"
                                element={<Modules />}
                            />
                            <Route
                                path="/buat-materi-belajar"
                                element={<CreateModule />}
                            />
                            <Route path="/acara" element={<Events />} />
                            <Route
                                path="/buat-acara"
                                element={<CreateEvent />}
                            />
                            <Route
                                path="/product-knowledges"
                                element={<ProductKnowledges />}
                            />
                            <Route
                                path="/buat-product-knowledge"
                                element={<CreateProductKnowledge />}
                            />
                            <Route path="/member" element={<Users />} />
                        </Route>
                    </Routes>
                </ThemeProvider>
            </BrowserRouter>
        </div>
    );
}

export default App;
