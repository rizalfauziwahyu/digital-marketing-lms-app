import React from "react";
import {
  Box,
  Button,
  Typography,
  TextField,
  MenuItem,
  Switch,
  FormControlLabel,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { LocalizationProvider } from "@mui/x-date-pickers";
import moment from "moment";
import {
  MobileDatePicker,
  MobileTimePicker,
} from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Formik } from "formik";
import Dropzone from "react-dropzone";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import FlexBetween from "components/FlexBetween";
import { useCreateEventMutation } from "state/api";
import BundledEditor from "components/BundledEditor";

const eventSchema = yup.object().shape({
  title: yup
    .string()
    .required(
      "Judul Event Wajib Diisi!"
    ),
  description: yup
    .string()
    .required(
      "Deskripsi Event Wajib Diisi!"
    ),
  image: yup
    .string()
    .required(
      "Flyer Event Wajib Diupload!"
    ),
  type: yup
    .string()
    .required(
      "Tipe Event Wajib Dipilih!"
    ),
  date: yup
    .date()
    .required(
      "Tanggal Event Wajib Diisi!"
    ),
  start_time: yup
    .string()
    .required(
      "Waktu Mulai Event Wajib Diisi!"
    ),
  end_time: yup
    .string(
      "Waktu Perkiraan Event Selesai Wajib Diisi!"
    )
    .required(),
  is_published: yup.bool(),
});

const initialValues = {
  title: "",
  description: "",
  image: "",
  type: "",
  date: "",
  start_time: "",
  end_time: "",
  is_published: false,
};

const Form = () => {
  const isNonMobile = useMediaQuery(
    "(min-width:600px)"
  );
  const navigate = useNavigate();
  const { palette } = useTheme();

  const [createEvent, { isLoading }] =
    useCreateEventMutation();

  const postEvent = async (
    values,
    onSubmitProps
  ) => {
    // this allows us to send form info with image
    console.log(values);
    const formData = new FormData();
    for (let value in values) {
      formData.append(
        value,
        values[value]
      );
    }
    formData.append(
      "image",
      values.image.name
    );

    createEvent(formData);

    onSubmitProps.resetForm();

    if (!isLoading) {
      navigate("/acara");
    }
  };

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postEvent(
      values,
      onSubmitProps
    );
  };
  return (
    <Formik
      onSubmit={handleFormSubmit}
      initialValues={initialValues}
      validationSchema={eventSchema}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        resetForm,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            paddingTop="30px"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": {
                gridColumn: isNonMobile
                  ? undefined
                  : "span 4",
              },
            }}
          >
            <Box gridColumn="span 4">
              <TextField
                label="Judul Event"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.title}
                name="title"
                error={
                  Boolean(
                    touched.title
                  ) &&
                  Boolean(errors.title)
                }
                helperText={
                  touched.title &&
                  errors.title
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>
            <Box gridColumn="span 4">
              <BundledEditor
                value={
                  values.description
                }
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist",
                    "anchor",
                    "autolink",
                    "help",
                    "image",
                    "link",
                    "lists",
                    "searchreplace",
                    "table",
                    "wordcount",
                  ],
                  toolbar:
                    "undo redo | blocks | " +
                    "bold italic forecolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={(
                  stringifiedHtmlValue
                ) => {
                  setFieldValue(
                    "description",
                    stringifiedHtmlValue
                  );
                }}
                textareaName="description"
              />
            </Box>

            <Box
              gridColumn="span 4"
              border={`1px solid ${palette.neutral.medium}`}
              borderRadius="5px"
            >
              <Dropzone
                acceptedFiles=".jpg,.jpeg,.png"
                multiple={false}
                onDrop={(
                  acceptedFiles
                ) =>
                  setFieldValue(
                    "image",
                    acceptedFiles[0]
                  )
                }
              >
                {({
                  getRootProps,
                  getInputProps,
                }) => (
                  <Box
                    {...getRootProps()}
                    border={`2px dashed ${palette.primary.main}`}
                    p="1rem"
                    sx={{
                      "&:hover": {
                        cursor:
                          "pointer",
                      },
                    }}
                  >
                    <input
                      {...getInputProps()}
                    />
                    {!values.image ? (
                      <p>
                        Add image Here
                      </p>
                    ) : (
                      <FlexBetween>
                        <Typography>
                          {
                            values.image
                              .name
                          }
                        </Typography>
                        <EditOutlinedIcon />
                      </FlexBetween>
                    )}
                  </Box>
                )}
              </Dropzone>
            </Box>
            <Box gridColumn="span 4">
              <TextField
                label="Tipe"
                name="type"
                fullWidth
                select
                SelectProps={{
                  multiple: false,
                }}
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.type}
                error={
                  Boolean(
                    touched.type
                  ) &&
                  Boolean(errors.type)
                }
                helperText={
                  touched.type &&
                  errors.type
                }
              >
                <MenuItem
                  value="ZOOM"
                  sx={{
                    color:
                      palette
                        .secondary[200],
                  }}
                >
                  ZOOM
                </MenuItem>
                <MenuItem
                  value="OFFLINE"
                  sx={{
                    color:
                      palette
                        .secondary[200],
                  }}
                >
                  OFFLINE
                </MenuItem>
              </TextField>
            </Box>
            <LocalizationProvider
              dateAdapter={
                AdapterDateFns
              }
            >
              <Box gridColumn="span 4">
                <MobileDatePicker
                  label="Tanggal Event"
                  value={
                    new Date(
                      values.date
                    )
                  }
                  inputFormat="dd/MM/yyyy"
                  onChange={(value) =>
                    setFieldValue(
                      "date",
                      moment(value).toISOString(true),
                      true
                    )
                  }
                  renderInput={(
                    params
                  ) => (
                    <TextField
                      name="date"
                      fullWidth
                      margin="normal"
                      error={
                        Boolean(
                          touched.date
                        ) &&
                        Boolean(
                          errors.date
                        )
                      }
                      helperText={
                        touched.date &&
                        errors.date
                      }
                      {...params}
                    />
                  )}
                />
              </Box>
              <Box gridColumn="span 4">
                <MobileTimePicker
                  label="Dimulai Pukul"
                  inputFormat="HH:mm"
                  value={
                    new Date(
                      values.start_time
                    )
                  }
                  onChange={(value) => {
                    setFieldValue(
                      "start_time",
                      moment(value).toISOString(true),
                      true
                    );
                  }}
                  renderInput={(
                    params
                  ) => (
                    <TextField
                      name="start_time"
                      fullWidth
                      margin="normal"
                      error={
                        Boolean(
                          touched.start_time
                        ) &&
                        Boolean(
                          errors.start_time
                        )
                      }
                      helperText={
                        touched.start_time &&
                        errors.start_time
                      }
                      {...params}
                    />
                  )}
                />
              </Box>
              <Box gridColumn="span 4">
                <MobileTimePicker
                  label="Selesai Pukul"
                  inputFormat="HH:mm"
                  value={
                    new Date(
                      values.end_time
                    )
                  }
                  onChange={(value) => {
                    setFieldValue(
                      "end_time",
                      moment(value).toISOString(true),
                      true
                    );
                  }}
                  renderInput={(
                    params
                  ) => (
                    <TextField
                      name="end_time"
                      fullWidth
                      margin="normal"
                      error={
                        Boolean(
                          touched.end_time
                        ) &&
                        Boolean(
                          errors.end_time
                        )
                      }
                      helperText={
                        touched.end_time &&
                        errors.end_time
                      }
                      {...params}
                    />
                  )}
                />
              </Box>
            </LocalizationProvider>
            <Box gridColumn="span 4">
              <FormControlLabel
                control={
                  <Switch
                    checked={
                      values.is_published
                    }
                    onChange={
                      handleChange
                    }
                    name="is_published"
                  />
                }
                label="Publish"
              />
            </Box>
            {/* BUTTONS */}
            <Box>
              <Button
                fullWidth
                type="submit"
                sx={{
                  m: "2rem 0",
                  p: "1rem",
                  backgroundColor:
                    palette.secondary
                      .main,
                  color:
                    palette.background
                      .alt,
                  "&:hover": {
                    color:
                      palette.primary
                        .main,
                  },
                }}
              >
                SIMPAN
              </Button>
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default Form;
