import React from "react";
import {
  Box,
  useTheme,
} from "@mui/material";
import { useGetEventsQuery } from "state/api";
import Header from "components/Header";
import { DataGrid } from "@mui/x-data-grid";

const Events = () => {
  const theme = useTheme();
  const { data, isLoading } =
    useGetEventsQuery();
  console.log("data", data);

  const columns = [
    {
      field: "title",
      headerName: "Judul Materi",
      flex: 1,
    },
    {
      field: "is_published",
      headerName: "Rilis",
      flex: 0.4,
    },
    {
      field: "type",
      headerName: "Tipe",
      flex: 0.4,
    },
    {
      field: "date",
      headerName: "Tanggal",
      flex: 0.4,
    },
    {
      field: "start_time",
      headerName: "Mulai",
      flex: 0.4,
    },
    {
      field: "end_time",
      headerName: "Selesai",
      flex: 0.4,
    },
    {
      field: "author",
      headerName: "Di Upload Oleh",
      flex: 0.4,
    },
  ];

  return (
    <Box m="1.5rem 2.5rem">
      <Header
        title="Event Perusahaan"
        subtitle="Daftar Event"
        buttonTitle="Buat Event"
        navigatePath="/buat-acara"
      />
      <Box
        mt="40px"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .MuiDataGrid-columnHeaders":
            {
              backgroundColor:
                theme.palette.background
                  .alt,
              color:
                theme.palette
                  .secondary[100],
              borderBottom: "none",
            },
          "& .MuiDataGrid-virtualScroller":
            {
              backgroundColor:
                theme.palette.primary
                  .light,
            },
          "& .MuiDataGrid-footerContainer":
            {
              backgroundColor:
                theme.palette.background
                  .alt,
              color:
                theme.palette
                  .secondary[100],
              borderTop: "none",
            },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text":
            {
              color: `${theme.palette.secondary[200]} !important`,
            },
        }}
      >
        <DataGrid
          loading={isLoading || !data}
          getRowId={(row) => row.slug}
          rows={data || []}
          columns={columns}
        />
      </Box>
    </Box>
  );
};

export default Events;
