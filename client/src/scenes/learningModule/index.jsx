import React from "react";
import { Box, useTheme } from "@mui/material";
import { useGetModulesQuery } from "state/api";
import Header from "components/Header";
import { DataGrid } from "@mui/x-data-grid";

const Modules = () => {
  const theme = useTheme();
  const { data, isLoading } = useGetModulesQuery();
  console.log("data", data);

  const columns = [
    {
      field: "title",
      headerName: "Judul Materi",
      flex: 1,
    },
    {
      field: "author",
      headerName: "Di Upload Oleh",
      flex: 0.4,
    },
    {
      field: "created_at",
      headerName: "Dibuat Tanggal",
      flex: 0.4,
    },
    {
      field: "updated_at",
      headerName: "Diupdate Tanggal",
      flex: 0.4,
    },
  ];

  return (
    <Box m="1.5rem 2.5rem">
      <Header title="Materi Kelas" subtitle="Daftar Materi Kelas" buttonTitle="Buat Materi Baru" navigatePath="/buat-materi-belajar" />
      <Box
        mt="40px"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: theme.palette.background.alt,
            color: theme.palette.secondary[100],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: theme.palette.primary.light,
          },
          "& .MuiDataGrid-footerContainer": {
            backgroundColor: theme.palette.background.alt,
            color: theme.palette.secondary[100],
            borderTop: "none",
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${theme.palette.secondary[200]} !important`,
          },
        }}
      >
        <DataGrid
          loading={isLoading || !data}
          getRowId={(row) => row.slug}
          rows={data || []}
          columns={columns}
        />
      </Box>
    </Box>
  );
};

export default Modules;