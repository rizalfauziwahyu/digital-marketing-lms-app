import React from "react";
import {
    Box,
    Card,
    CardMedia,
    CardActions,
    CardContent,
    Button,
    Typography,
    useTheme,
    useMediaQuery,
} from "@mui/material";
import Header from "components/Header";
import { useGetClassroomsQuery } from "state/api";

const Classroom = ({ slug, title, description, image }) => {
    const theme = useTheme();

    return (
        <Card
            sx={{
                backgroundImage: "none",
                backgroundColor: theme.palette.background.alt,
                borderRadius: "0.55rem",
            }}
        >
            <CardMedia
                component="img"
                height="140"
                image={image}
            />
            <CardContent>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
                <Typography variant="body2">{title}</Typography>
            </CardContent>
            <CardActions>
                <Button
                    variant="primary"
                    size="small"
                >
                    Lihat
                </Button>
            </CardActions>
        </Card>
    );
};

const Classrooms = () => {
    const { data } = useGetClassroomsQuery();
    const isNonMobile = useMediaQuery("(min-width: 1000px)");

    return (
        <Box m="1.5rem 2.5rem">
            <Header
                title="KELAS DIGITAL MARKETING"
                subtitle="Lihat semua daftar kelas digital marketing."
                buttonTitle="Buat Kelas Baru"
                navigatePath="/buat-kelas"
            />

            <Box
                mt="20px"
                display="grid"
                gridTemplateColumns="repeat(4, minmax(0, 1fr))"
                justifyContent="space-between"
                rowGap="20px"
                columnGap="1.33%"
                sx={{
                    "& > div": {
                        gridColumn: isNonMobile ? undefined : "span 4",
                    },
                }}
            >
                {data &&
                    data.map(
                        ({ slug, title, description, image }) => (
                            <Classroom
                                key={slug}
                                id={slug}
                                title={title}
                                description={description}
                                image={image}
                            />
                        )
                    )}
            </Box>
        </Box>
    );
};

export default Classrooms;
