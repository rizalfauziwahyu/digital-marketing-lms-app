import React from "react";
import {
  Box,
  Button,
  Typography,
  TextField,
  MenuItem,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import BundledEditor from "components/BundledEditor";
import { Formik } from "formik";
import Dropzone from "react-dropzone";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import FlexBetween from "components/FlexBetween";
import {
  useGetModulesQuery,
  useCreateClassroomMutation,
} from "state/api";

const classroomSchema = yup
  .object()
  .shape({
    title: yup
      .string()
      .required("required"),
    description: yup
      .string()
      .required("required"),
    image: yup
      .string()
      .required("required"),
    moduleList: yup.array(),
  });

const initialValues = {
  title: "",
  description: "",
  image: "",
  moduleList: [],
};

const Form = () => {
  const isNonMobile = useMediaQuery(
    "(min-width:600px)"
  );
  const navigate = useNavigate();
  const { palette } = useTheme();
  const { data } = useGetModulesQuery();
  const [
    createClassroom,
    {
      isLoading:
        createClassroomIsLoading,
    },
  ] = useCreateClassroomMutation();

  const postClassroom = async (
    values,
    onSubmitProps
  ) => {
    const formData = new FormData();
    for (let value in values) {
      formData.append(
        value,
        values[value]
      );
    }
    formData.append(
      "moduleList",
      JSON.stringify(values.moduleList)
    );
    formData.append(
      "image",
      values.image.name
    );

    createClassroom(formData);
    onSubmitProps.resetForm();

    if (!createClassroomIsLoading) {
      navigate("/kelas");
    }
  };

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postClassroom(
      values,
      onSubmitProps
    );
  };
  return (
    <Formik
      onSubmit={handleFormSubmit}
      initialValues={initialValues}
      validationSchema={classroomSchema}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        resetForm,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            paddingTop="30px"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": {
                gridColumn: isNonMobile
                  ? undefined
                  : "span 4",
              },
            }}
          >
            <Box gridColumn="span 4">
              <TextField
                label="Judul Kelas"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.title}
                name="title"
                error={
                  Boolean(
                    touched.title
                  ) &&
                  Boolean(errors.title)
                }
                helperText={
                  touched.title &&
                  errors.title
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>
            <Box gridColumn="span 4">
              <BundledEditor
                value={
                  values.description
                }
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist",
                    "anchor",
                    "autolink",
                    "help",
                    "image",
                    "link",
                    "lists",
                    "searchreplace",
                    "table",
                    "wordcount",
                  ],
                  toolbar:
                    "undo redo | blocks | " +
                    "bold italic forecolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={(
                  stringifiedHtmlValue
                ) => {
                  setFieldValue(
                    "description",
                    stringifiedHtmlValue
                  );
                }}
                textareaName="description"
              />
            </Box>

            <Box
              gridColumn="span 4"
              border={`1px solid ${palette.neutral.medium}`}
              borderRadius="5px"
            >
              <Dropzone
                acceptedFiles=".jpg,.jpeg,.png"
                multiple={false}
                onDrop={(
                  acceptedFiles
                ) =>
                  setFieldValue(
                    "image",
                    acceptedFiles[0]
                  )
                }
              >
                {({
                  getRootProps,
                  getInputProps,
                }) => (
                  <Box
                    {...getRootProps()}
                    border={`2px dashed ${palette.primary.main}`}
                    p="1rem"
                    sx={{
                      "&:hover": {
                        cursor:
                          "pointer",
                      },
                    }}
                  >
                    <input
                      {...getInputProps()}
                    />
                    {!values.image ? (
                      <p>
                        Tambahkan Gambar
                        Thumbnail Kelas
                      </p>
                    ) : (
                      <FlexBetween>
                        <Typography>
                          {
                            values.image
                              .name
                          }
                        </Typography>
                        <EditOutlinedIcon />
                      </FlexBetween>
                    )}
                  </Box>
                )}
              </Dropzone>
            </Box>

            <Box gridColumn="span 4">
              <TextField
                label="Tambahkan Modul Pembelajaran"
                name="moduleList"
                fullWidth
                select
                SelectProps={{
                  multiple: true,
                  value:
                    values.moduleList,
                  onChange:
                    handleChange,
                  onBlur: handleBlur,
                }}
                error={
                  Boolean(
                    touched.moduleList
                  ) &&
                  Boolean(
                    errors.moduleList
                  )
                }
                helperText={
                  touched.moduleList &&
                  errors.moduleList
                }
              >
                {data &&
                  data.map(
                    ({ id, title }) => (
                      <MenuItem
                        value={id}
                        key={id}
                        sx={{
                          color:
                            palette
                              .secondary[200],
                        }}
                      >
                        {title}
                      </MenuItem>
                    )
                  )}
              </TextField>
            </Box>

            {/* BUTTONS */}
            <Box>
              <Button
                fullWidth
                type="submit"
                sx={{
                  m: "2rem 0",
                  p: "1rem",
                  backgroundColor:
                    palette.secondary
                      .main,
                  color:
                    palette.background
                      .alt,
                  "&:hover": {
                    color:
                      palette.primary
                        .main,
                  },
                }}
              >
                SIMPAN
              </Button>
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default Form;
