import React from "react";
import {
  Box,
  Button,
  Typography,
  TextField,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Formik } from "formik";
import Dropzone from "react-dropzone";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import FlexBetween from "components/FlexBetween";
import { useCreateContentCategoryMutation } from "state/api";

const categorySchema = yup
  .object()
  .shape({
    name: yup
      .string()
      .required("required"),
    description: yup
      .string()
      .required("required"),
    image: yup
      .string()
      .required("required"),
  });

const initialValues = {
  name: "",
  description: "",
  image: "",
};

const Form = () => {
  const isNonMobile = useMediaQuery(
    "(min-width:600px)"
  );
  const navigate = useNavigate();
  const { palette } = useTheme();

  const [
    createContentCategory,
    { isLoading },
  ] =
    useCreateContentCategoryMutation();

  const postCategory = async (
    values,
    onSubmitProps
  ) => {
    // this allows us to send form info with image
    console.log(values);
    const formData = new FormData();
    for (let value in values) {
      formData.append(
        value,
        values[value]
      );
    }
    formData.append(
      "image",
      values.image.name
    );

    createContentCategory(formData);

    onSubmitProps.resetForm();

    if (!isLoading) {
      navigate("/kategori-konten");
    }
  };

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postCategory(
      values,
      onSubmitProps
    );
  };
  return (
    <Formik
      onSubmit={handleFormSubmit}
      initialValues={initialValues}
      validationSchema={categorySchema}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        resetForm,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            paddingTop="30px"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": {
                gridColumn: isNonMobile
                  ? undefined
                  : "span 4",
              },
            }}
          >
            <Box gridColumn="span 4">
              <TextField
                label="Nama Kategori"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.name}
                name="name"
                error={
                  Boolean(
                    touched.name
                  ) &&
                  Boolean(errors.name)
                }
                helperText={
                  touched.name &&
                  errors.name
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>
            <Box gridColumn="span 4">
              <TextField
                label="Deskripsi"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={
                  values.description
                }
                name="description"
                error={
                  Boolean(
                    touched.description
                  ) &&
                  Boolean(
                    errors.description
                  )
                }
                helperText={
                  touched.description &&
                  errors.description
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>

            <Box
              gridColumn="span 4"
              border={`1px solid ${palette.neutral.medium}`}
              borderRadius="5px"
            >
              <Dropzone
                acceptedFiles=".jpg,.jpeg,.png"
                multiple={false}
                onDrop={(
                  acceptedFiles
                ) =>
                  setFieldValue(
                    "image",
                    acceptedFiles[0]
                  )
                }
              >
                {({
                  getRootProps,
                  getInputProps,
                }) => (
                  <Box
                    {...getRootProps()}
                    border={`2px dashed ${palette.primary.main}`}
                    p="1rem"
                    sx={{
                      "&:hover": {
                        cursor:
                          "pointer",
                      },
                    }}
                  >
                    <input
                      {...getInputProps()}
                    />
                    {!values.image ? (
                      <p>
                        Add image Here
                      </p>
                    ) : (
                      <FlexBetween>
                        <Typography>
                          {
                            values.image
                              .name
                          }
                        </Typography>
                        <EditOutlinedIcon />
                      </FlexBetween>
                    )}
                  </Box>
                )}
              </Dropzone>
            </Box>

            {/* BUTTONS */}
            <Box>
              <Button
                fullWidth
                type="submit"
                sx={{
                  m: "2rem 0",
                  p: "1rem",
                  backgroundColor:
                    palette.secondary
                      .main,
                  color:
                    palette.background
                      .alt,
                  "&:hover": {
                    color:
                      palette.primary
                        .main,
                  },
                }}
              >
                SIMPAN
              </Button>
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default Form;
