import React, { useState } from "react";
import {
    Box,
    Card,
    CardMedia,
    CardActions,
    CardContent,
    Collapse,
    Button,
    Typography,
    useTheme,
    useMediaQuery,
} from "@mui/material";
import Header from "components/Header";
import { useGetContentCategoriesQuery } from "state/api";

const ContentCategory = ({ id, slug, name, description, image }) => {
    const theme = useTheme();
    const [isExpanded, setIsExpanded] = useState(false);

    return (
        <Card
            sx={{
                backgroundImage: "none",
                backgroundColor: theme.palette.background.alt,
                borderRadius: "0.55rem",
            }}
        >
            <CardMedia
                component="img"
                height="140"
                image={image}
                alt="green iguana"
            />
            <CardContent>
                <Typography variant="h5" component="div">
                    {name}
                </Typography>
                <Typography variant="body2">{description}</Typography>
            </CardContent>
            <CardActions>
                <Button
                    variant="primary"
                    size="small"
                    onClick={() => setIsExpanded(!isExpanded)}
                >
                    Lihat
                </Button>
            </CardActions>
            <Collapse
                in={isExpanded}
                timeout="auto"
                unmountOnExit
                sx={{
                    color: theme.palette.neutral[300],
                }}
            >
                <CardMedia
                    component="img"
                    image={image}
                    alt="content category image"
                />
                <CardContent>
                    <Typography>id: {id}</Typography>
                    <Typography>slug: {slug}</Typography>
                </CardContent>
            </Collapse>
        </Card>
    );
};

const ContentCategories = () => {
    const { data, isLoading } = useGetContentCategoriesQuery();
    const isNonMobile = useMediaQuery("(min-width: 1000px)");

    return (
        <Box m="1.5rem 2.5rem">
            <Header
                title="KATEGORI KONTEN"
                subtitle="Lihat semua daftar kategori konten."
                buttonTitle="Buat Kategori"
                navigatePath="/buat-kategori-konten"
            />

            <Box
                mt="20px"
                display="grid"
                gridTemplateColumns="repeat(4, minmax(0, 1fr))"
                justifyContent="space-between"
                rowGap="20px"
                columnGap="1.33%"
                sx={{
                    "& > div": {
                        gridColumn: isNonMobile ? undefined : "span 4",
                    },
                }}
            >
                {data &&
                    data.map(
                        ({ id, slug, name, description, image }) => (
                            <ContentCategory
                                key={slug}
                                id={id}
                                name={name}
                                description={description}
                                slug={slug}
                                image={image}
                            />
                        )
                    )}
            </Box>
        </Box>
    );
};

export default ContentCategories;
