import React from "react";
import { Box, useTheme, Button, Typography } from "@mui/material";
import { useGetUsersQuery, useApproveUserMutation } from "state/api";
import Header from "components/Header";
import { DataGrid } from "@mui/x-data-grid";

const Users = () => {
    const theme = useTheme();
    const { data, isLoading } = useGetUsersQuery();
    const [approveUser, { isLoading: isApproveLoading }] =
        useApproveUserMutation();
    console.log("data", data);

    const columns = [
        {
            field: "name",
            headerName: "Nama",
            width: 220,
        },
        {
            field: "username",
            headerName: "Username",
            width: 180,
        },
        {
            field: "email",
            headerName: "Email",
            width: 180,
        },
        {
            field: "whatsapp",
            headerName: "Whatsapp",
            width: 120,
        },
        {
            field: "status",
            headerName: "Status Member",
            width: 180,
        },
        {
            field: "approved",
            headerName: "Status Verifikasi",
            width: 180,
        },
        {
            field: "id",
            headerName: "Verifikasi User",
            width: 180,
            renderCell: (cellValues) => (
                <Button
                    sx={{
                        backgroundColor: theme.palette.secondary[300],
                        color: theme.palette.primary[600],
                    }}
                    onClick={() => {
                        console.log(cellValues);
                        approveUser(cellValues.id)
                    }}
                >
                    <Typography>Verifikasi</Typography>
                </Button>
            ),
        },
    ];

    return (
        <Box m="1.5rem 2.5rem">
            <Header
                title="Daftar User BahagiAkademi"
                subtitle="Daftar User"
                buttonTitle="Buat Konten Baru"
                navigatePath="/buat-konten"
            />
            <Box
                mt="40px"
                height="75vh"
                sx={{
                    "& .MuiDataGrid-root": {
                        border: "none",
                    },
                    "& .MuiDataGrid-cell": {
                        borderBottom: "none",
                    },
                    "& .MuiDataGrid-columnHeaders": {
                        backgroundColor: theme.palette.background.alt,
                        color: theme.palette.secondary[100],
                        borderBottom: "none",
                    },
                    "& .MuiDataGrid-virtualScroller": {
                        backgroundColor: theme.palette.primary.light,
                    },
                    "& .MuiDataGrid-footerContainer": {
                        backgroundColor: theme.palette.background.alt,
                        color: theme.palette.secondary[100],
                        borderTop: "none",
                    },
                    "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                        color: `${theme.palette.secondary[200]} !important`,
                    },
                }}
            >
                <DataGrid
                    loading={isLoading || !data || isApproveLoading }
                    getRowId={(row) => row.id}
                    rows={data || []}
                    columns={columns}
                />
            </Box>
        </Box>
    );
};

export default Users;
