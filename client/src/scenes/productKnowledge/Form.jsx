import React from "react";
import {
  Box,
  Button,
  Typography,
  TextField,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import BundledEditor from "components/BundledEditor";
import { Formik } from "formik";
import Dropzone from "react-dropzone";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import FlexBetween from "components/FlexBetween";
import { useCreateProductKnowledgeMutation } from "state/api";

const contentSchema = yup
  .object()
  .shape({
    title: yup
      .string()
      .required(
        "Judul konten wajib di isi!"
      ),
    content: yup
      .string()
      .required(
        "Konten Product Knowledge Wajid Diisi!"
      ),
    image: yup.string(),
    video: yup.string(),
    attachment: yup.string(),
  });

const initialValues = {
  title: "",
  content: "",
  image: "",
  content_category_id: "",
};

const Form = () => {
  const isNonMobile = useMediaQuery(
    "(min-width:600px)"
  );
  const navigate = useNavigate();
  const { palette } = useTheme();

  const [
    createProductKnowledge,
    { isLoading },
  ] =
    useCreateProductKnowledgeMutation();

  const postProductKnowledge = async (
    values,
    onSubmitProps
  ) => {
    // this allows us to send form info with image
    console.log(values);
    const formData = new FormData();
    for (let value in values) {
      formData.append(
        value,
        values[value]
      );
    }

    createProductKnowledge(formData);
    onSubmitProps.resetForm();

    if (!isLoading) {
      navigate("/konten");
    }
  };

  const handleFormSubmit = async (
    values,
    onSubmitProps
  ) => {
    await postProductKnowledge(
      values,
      onSubmitProps
    );
  };
  return (
    <Formik
      onSubmit={handleFormSubmit}
      initialValues={initialValues}
      validationSchema={contentSchema}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            paddingTop="30px"
            gap="30px"
            gridTemplateColumns="repeat(4, minmax(0, 1fr))"
            sx={{
              "& > div": {
                gridColumn: isNonMobile
                  ? undefined
                  : "span 4",
              },
            }}
          >
            <Box gridColumn="span 4">
              <TextField
                label="Judul Konten"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.title}
                name="title"
                error={
                  Boolean(
                    touched.title
                  ) &&
                  Boolean(errors.title)
                }
                helperText={
                  touched.title &&
                  errors.title
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>

            <Box gridColumn="span 4">
              <TextField
                label="Video Youtube (Opsional)"
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.video}
                name="video"
                error={
                  Boolean(
                    touched.video
                  ) &&
                  Boolean(errors.video)
                }
                helperText={
                  touched.video &&
                  errors.video
                }
                sx={{
                  gridColumn: "span 2",
                }}
              />
            </Box>

            <Box gridColumn="span 4">
              <BundledEditor
                value={values.content}
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist",
                    "anchor",
                    "autolink",
                    "help",
                    "image",
                    "link",
                    "lists",
                    "searchreplace",
                    "table",
                    "wordcount",
                  ],
                  toolbar:
                    "undo redo | blocks | " +
                    "bold italic forecolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={(
                  stringifiedHtmlValue
                ) => {
                  setFieldValue(
                    "content",
                    stringifiedHtmlValue
                  );
                }}
                textareaName="content"
              />
            </Box>

            <Box
              gridColumn="span 4"
              border={`1px solid ${palette.neutral.medium}`}
              borderRadius="5px"
            >
              <Dropzone
                acceptedFiles=".jpg,.jpeg,.png"
                multiple={false}
                onDrop={(
                  acceptedFiles
                ) =>
                  setFieldValue(
                    "image",
                    acceptedFiles[0]
                  )
                }
              >
                {({
                  getRootProps,
                  getInputProps,
                }) => (
                  <Box
                    {...getRootProps()}
                    border={`2px dashed ${palette.primary.main}`}
                    p="1rem"
                    sx={{
                      "&:hover": {
                        cursor:
                          "pointer",
                      },
                    }}
                  >
                    <input
                      {...getInputProps()}
                    />
                    {!values.image ? (
                      <p>
                        Add image Here
                      </p>
                    ) : (
                      <FlexBetween>
                        <Typography>
                          {
                            values.image
                              .name
                          }
                        </Typography>
                        <EditOutlinedIcon />
                      </FlexBetween>
                    )}
                  </Box>
                )}
              </Dropzone>
            </Box>

            <Box
              gridColumn="span 4"
              border={`1px solid ${palette.neutral.medium}`}
              borderRadius="5px"
            >
              <Dropzone
                acceptedFiles="doc,docx,xls,xlsx,csv,pdf,png,ppt,pptx"
                multiple={false}
                onDrop={(
                  acceptedFiles
                ) =>
                  setFieldValue(
                    "attachment",
                    acceptedFiles[0]
                  )
                }
              >
                {({
                  getRootProps,
                  getInputProps,
                }) => (
                  <Box
                    {...getRootProps()}
                    border={`2px dashed ${palette.primary.main}`}
                    p="1rem"
                    sx={{
                      "&:hover": {
                        cursor:
                          "pointer",
                      },
                    }}
                  >
                    <input
                      {...getInputProps()}
                    />
                    {!values.attachment ? (
                      <p>
                        Add File Pdf Here
                      </p>
                    ) : (
                      <FlexBetween>
                        <Typography>
                          {
                            values
                              .attachment
                              .name
                          }
                        </Typography>
                        <EditOutlinedIcon />
                      </FlexBetween>
                    )}
                  </Box>
                )}
              </Dropzone>
            </Box>

            {/* BUTTONS */}
            <Box>
              <Button
                fullWidth
                type="submit"
                sx={{
                  m: "2rem 0",
                  p: "1rem",
                  backgroundColor:
                    palette.secondary
                      .main,
                  color:
                    palette.background
                      .alt,
                  "&:hover": {
                    color:
                      palette.primary
                        .main,
                  },
                }}
              >
                SIMPAN
              </Button>
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default Form;
