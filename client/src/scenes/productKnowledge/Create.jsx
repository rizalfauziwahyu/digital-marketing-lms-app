import React from "react";
import { Box } from "@mui/material";
import Header from "components/Header";
import Form from "./Form";

const CreateProductKnowledge = () => {
  return (
    <Box m="1.5rem 2.5rem">
      <Header title="Buat Product Knowledge Baru" />
      <Form />
    </Box>
  );
};

export default CreateProductKnowledge;
