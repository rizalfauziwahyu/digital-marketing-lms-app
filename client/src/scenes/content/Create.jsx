import React from "react";
import { Box } from "@mui/material";
import Header from "components/Header";
import Form from './Form';

const CreateContent= () => {
    return (
        <Box m="1.5rem 2.5rem">
            <Header title="Buat Konten Baru" />
            <Form />
        </Box>
    );
};

export default CreateContent;
