import React from "react";
import {
  Box,
  useTheme,
} from "@mui/material";
import { useGetContentsQuery } from "state/api";
import Header from "components/Header";
import { DataGrid } from "@mui/x-data-grid";

const Contents = () => {
  const theme = useTheme();
  const { data, isLoading } =
    useGetContentsQuery();
  console.log("data", data);

  const columns = [
    {
      field: "title",
      headerName: "Judul",
      width: 220,
    },
    {
      field: "category",
      headerName: "Kategori",
      width: 180,
    },
    {
      field: "is_published",
      headerName: "Rilis",
      width: 80,
    },
    {
      field: "author",
      headerName: "Di Upload Oleh",
      width: 120,
    },
    {
      field: "created_at",
      headerName: "Dibuat Tanggal",
      type: "date",
      width: 180,
    },
    {
      field: "updated_at",
      headerName: "Diupdate Tanggal",
      type: "date",
      width: 180,
    },
  ];

  return (
    <Box m="1.5rem 2.5rem">
      <Header
        title="Konten Marketing"
        subtitle="Daftar Konten"
        buttonTitle="Buat Konten Baru"
        navigatePath="/buat-konten"
      />
      <Box
        mt="40px"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .MuiDataGrid-columnHeaders":
            {
              backgroundColor:
                theme.palette.background
                  .alt,
              color:
                theme.palette
                  .secondary[100],
              borderBottom: "none",
            },
          "& .MuiDataGrid-virtualScroller":
            {
              backgroundColor:
                theme.palette.primary
                  .light,
            },
          "& .MuiDataGrid-footerContainer":
            {
              backgroundColor:
                theme.palette.background
                  .alt,
              color:
                theme.palette
                  .secondary[100],
              borderTop: "none",
            },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text":
            {
              color: `${theme.palette.secondary[200]} !important`,
            },
        }}
      >
        <DataGrid
          loading={isLoading || !data}
          getRowId={(row) => row.slug}
          rows={data || []}
          columns={columns}
        />
      </Box>
    </Box>
  );
};

export default Contents;
