import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const api = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BASE_URL,
        prepareHeaders: (headers, { getState }) => {
            const token = getState().global.token;
            if (token) {
                headers.set("Authorization", `Bearer ${token}`);
            }

            return headers;
        },
    }),
    reducerPath: "adminApi",
    tagTypes: [
        "User",
        "ContentCategories",
        "Content",
        "Contents",
        "Classroom",
        "Classrooms",
        "Module",
        "Modules",
        "Event",
        "Events",
        "Users",
        "ProductKnowledges",
        "ProductKnowledge",
    ],
    endpoints: (build) => ({
        getUser: build.query({
            query: () => `api/user/profile`,
            providesTags: ["User"],
        }),
        getUsers: build.query({
            query: () => `api/users`,
            transformResponse: (rawResult, meta) => {
                return rawResult.data.users;
            },
            providesTags: ["Users"],
        }),
        approveUser: build.mutation({
            query: (userId) => ({
                url: `api/user/${userId}/approve`,
                method: "POST",
            }),

            transformResponse: (rawResult, meta) => {
                return rawResult.data;
            },
            providesTags: ["Users"],
        }),
        getContentCategories: build.query({
            query: () => `api/contents/categories`,
            transformResponse: (rawResult, meta) => {
                return rawResult.data.categories;
            },
            providesTags: ["ContentCategories"],
        }),
        createContentCategory: build.mutation({
            query: (values) => ({
                url: "api/contents/categories",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["ContentCategories"],
        }),
        getContents: build.query({
            query: () => `api/contents`,
            transformResponse: (rawResult, meta) => {
                return rawResult.data.contents;
            },
            providesTags: ["Contents"],
        }),
        createContent: build.mutation({
            query: (values) => ({
                url: "api/contents",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["Contents"],
        }),
        getContentBySlug: build.query({
            query: (slug) => `api/contents/${slug}`,
            providesTags: ["Content"],
        }),
        getClassrooms: build.query({
            query: () => "api/classrooms",
            transformResponse: (rawResult, meta) => {
                return rawResult.data.classrooms;
            },
            providesTags: ["Classrooms"],
        }),
        getClassroomBySlug: build.query({
            query: (slug) => `api/classrooms/${slug}`,
            providesTags: ["Classroom"],
        }),
        createClassroom: build.mutation({
            query: (values) => ({
                url: "api/classrooms",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["Classrooms"],
        }),
        getModules: build.query({
            query: () => "api/modules",
            transformResponse: (rawResult, meta) => {
                return rawResult.data.modules;
            },
            providesTags: ["Modules"],
        }),
        getModuleBySlug: build.query({
            query: (slug) => `api/modules/${slug}`,
            providesTags: ["Module"],
        }),
        createModule: build.mutation({
            query: (values) => ({
                url: "api/modules",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["Modules"],
        }),
        getEvents: build.query({
            query: () => "api/events",
            transformResponse: (rawResult, meta) => {
                return rawResult.data.events;
            },
            providesTags: ["Events"],
        }),
        getEventBySlug: build.query({
            query: (slug) => `api/events/${slug}`,
            providesTags: ["Event"],
        }),
        createEvent: build.mutation({
            query: (values) => ({
                url: "api/events",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["Events"],
        }),
        getProductKnowledges: build.query({
            query: () => "api/product-knowledges",
            transformResponse: (rawResult, meta) => {
                return rawResult.data.productKnowledges;
            },
            providesTags: ["ProductKnowledges"],
        }),
        getProductKnowledge: build.query({
            query: (slug) => `api/product-knowledges/${slug}`,
            transformResponse: (rawResult, meta) => {
                return rawResult.data;
            },
            providesTags: ["ProductKnowledge"],
        }),
        createProductKnowledge: build.mutation({
            query: (values) => ({
                url: "api/product-knowledges",
                method: "POST",
                body: values,
            }),
            invalidatesTags: ["ProductKnowledges"],
        }),
    }),
});

export const {
    useGetUserQuery,
    useGetUsersQuery,
    useApproveUserMutation,
    useGetContentCategoriesQuery,
    useCreateContentCategoryMutation,
    useGetContentBySlugQuery,
    useGetContentsQuery,
    useCreateContentMutation,
    useGetClassroomBySlugQuery,
    useGetClassroomsQuery,
    useCreateClassroomMutation,
    useGetModuleBySlugQuery,
    useGetModulesQuery,
    useCreateModuleMutation,
    useGetEventBySlugQuery,
    useGetEventsQuery,
    useCreateEventMutation,
    useGetProductKnowledgesQuery,
    useGetProductKnowledgeQuery,
    useCreateProductKnowledgeMutation,
} = api;
