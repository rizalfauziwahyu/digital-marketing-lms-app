import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    mode: "dark",
    isSidebarOpen: true,
    user: null,
    token: null,
};

export const globalSlice = createSlice({
    name: "global",
    initialState,
    reducers: {
        setMode: (state) => {
            state.mode = state.mode === "light" ? "dark" : "light";
        },
        setIsSidebarOpen: (state) => {
            state.isSidebarOpen = !state.isSidebarOpen;
        },
        setLogin: (state, action) => {
            state.user = action.payload.user;
            state.token = action.payload.user.token;
        },
        setLogout: (state) => {
            state.user = null;
            state.token = null;
        },
    },
});

export const { setMode, setIsSidebarOpen, setLogin, setLogout } = globalSlice.actions;

export default globalSlice.reducer;
