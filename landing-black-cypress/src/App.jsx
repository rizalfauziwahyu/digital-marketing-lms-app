import { useState } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import Header from "./components/Header";
import Hero from "./components/Hero";
import FeatureArea from "./components/FeatureArea";
import ContentArea1 from "./components/ContentArea1";
import ContentArea2 from "./components/ContentArea2";
import ContentArea3 from "./components/ContentArea3";
import ContentArea4 from "./components/ContentArea4";
import CtaArea from "./components/CtaArea";
import Footer from "./components/Footer";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Header />
              <Hero />
              <FeatureArea />
              <ContentArea1 />
              <ContentArea2 />
              <ContentArea3 />
              <CtaArea />
              <Footer />
            </div>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
