import React from "react";

const ContentArea2 = () => {
  return (
    <div className="content-area-l-17-2">
      <div className="container">
        <div className="row mb-5">
          <div class="content text-center">
            <h1>
              Sering nyeri sendi padahal masih muda ?
            </h1>
          </div>
        </div>
        <div className="row align-items-center justify-content-center">
          <div className="col-xl-10 col-lg-12">
            <div className="row align-items-center justify-content-center">
              <div
                className="col-xxl-5 col-xl-6 col-lg-6 col-md-8 order-lg-1 order-1"
                data-aos="fade-left"
                data-aos-duration="800"
                data-aos-once="true"
              >
                <div className="content section-heading-11">
                  <h2>
                    Nyeri sendi dapat menyerang siapa saja tanpa mengenal usia !!!
                  </h2>
                  <p>
                    NYERI SENDI <br />
                    NYERI PINGGANG  <br />
                    KESEMUTAN <br />
                    REUMATIK <br />
                    SAKIT BAHU <br />
                    SAKIT LEHER <br />
                    KEBAS KAKI DAN TANGAN <br />
                    LUMPUH SEPARUH BADAN <br />
                    GOUT / RADANG SENDI <br />
                    SYARAF KEJEPIT <br />
                    ASAM URAT <br />
                    <br />
                    <strong>Saatnya kamu butuh Black Cypress</strong>
                  </p>
                </div>
              </div>
              <div
                className="offset-xxl-1 col-xxl-6 col-xl-6 col-lg-6 col-md-8 order-lg-1 order-0"
                data-aos="fade-right"
                data-aos-duration="800"
                data-aos-once="true"
              >
                <div className="content-img position-relative">
                  <div className="image-1">
                    <img
                      src="assets/image/l8/model-1.jpg"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea2;
