import React from "react";

const Hero = () => {
  return (
    <div class="hero-area-l-17 position-relative">
      <div class="container">
        <div class="row position-relative justify-content-center">
          <div
            class="col-xxl-6 col-xl-7 col-lg-8 col-md-11 order-lg-1 order-1"
            data-aos="fade-right"
            data-aos-duration="800"
            data-aos-once="true"
          >
            <div class="content text-center">
              <h1>
                BLACK CYPRESS
              </h1>
              <p>
                Black Cypress adalah Produk Herbal yang diramu secara tradisional dengan 100% berbahan alami dan halal. <br /> Terbukti dapat membantu menjaga kesehatan serta pemulihan berbagai macam keluhan penyakit ringan, sedang maupun berat terutama yang berhubungan dengan Sendi dan Saraf
              </p>
            </div>
          </div>
          <div class="col-xl-8 col-lg-9 order-lg-1 order-0">
            <div class="hero-area-image">
              <img
                src="assets/image/l8/hero-photo.jpg"
                alt=""
                class="w-100"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
