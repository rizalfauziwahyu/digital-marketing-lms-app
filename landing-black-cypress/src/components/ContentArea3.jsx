import React from "react";

const ContentArea3 = () => {
  return (
    <div class="testimonial-area-l-17">
      <div class="container">
      <div className="row mb-5">
          <div class="content text-center">
            <h1>
              Testimonial
            </h1>
          </div>
        </div>
        <div class="row justify-content-center no-gutters border-collapse-1">
          <div class="col-lg-4 col-md-6 col-sm-9 p-0">
            <div class="testimonial-card border">
              <img
                src="assets/image/l8/testimoni-1.jpg"
                alt=""
                className="w-100"
              />
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-9 p-0">
            <div class="testimonial-card border">
              <img
                src="assets/image/l8/testimoni-2.jpg"
                alt=""
                class="mb-12 w-100"
              />

            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-9 p-0">
            <div class="testimonial-card border h-100">
              <img
                src="assets/image/l8/testimoni-3.jpg"
                alt=""
                class="mb-12 w-100"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea3;
