import React, {
  useState,
  useEffect,
} from "react";
import { useSearchParams } from "react-router-dom";

const ContentArea1 = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <div className="content-area-l-17-1">
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-xl-10 col-lg-12">
            <div className="row align-items-center justify-content-center">
              <div
                className="col-lg-6 col-md-8"
                data-aos="fade-right"
                data-aos-duration="800"
                data-aos-once="true"
              >
                <div className="content-img position-relative">
                  <div className="image-1">
                    <img
                      src="assets/image/l8/sendi-image-2.jpg"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div
                className="offset-xxl-1 col-xxl-5 col-xl-6 col-lg-6 col-md-8"
                data-aos="fade-left"
                data-aos-duration="800"
                data-aos-once="true"
              >
                <div className="content section-heading-11">
                  <h2>
                    Apakah anda memiliki
                    masalah nyeri sendi
                    dan saraf ?
                  </h2>
                  <p>
                    Tenang, ga usah
                    khawatir, sekarang
                    sudah ada Black
                    Cypress. Produk ini
                    adalah supplement
                    yang mempunyai
                    khasiat untuk
                    merawat masalah
                    persendian
                  </p>
                  {user && (
                    <a
                      href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20konsultasi%20produk%20Black%20Cypress%20`}
                      className="btn
                      focus-reset"
                    >
                      Konsultasi
                      Sekarang!
                    </a>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentArea1;
