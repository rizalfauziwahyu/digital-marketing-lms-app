import React, {
  useState,
  useEffect,
} from "react";
import { useSearchParams } from "react-router-dom";

const CtaArea = () => {
  const [search, setSearch] =
    useSearchParams();
  const [user, setUser] =
    useState(null);
  const getUser = async () => {
    const response = await fetch(
      `https://api.bahagiakademi.com/api/username/${search.get(
        "u"
      )}`,
      {
        method: "GET",
      }
    );
    const data = await response.json();
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div class="cta-area-l-17">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="d-md-flex justify-content-between text-align-lg-start text-center align-items-center">
              <h2>
                Dapatkan Black Cypress
                Hanya dengan Rp. 235000
              </h2>
              {user && (
                <a
                  href={`https://wa.me/${user.data.whatsapp}?text=Halo%2C%20saya%20tertarik%20untuk%20membeli%20produk%20Black%20Cypress%20Bahagia`}
                  class="btn"
                >
                  Beli Sekarang
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CtaArea;
