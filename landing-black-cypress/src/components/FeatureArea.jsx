import React from "react";

const FeatureArea = () => {
  return (
    <div className="feature-area-l-17 position-relative">
      <div className="container">
        <div className="row mb-5">
          <div class="content text-center">
            <h1>
              KANDUNGAN BLACK CYPRESS
            </h1>
          </div>
        </div>
        <div className="row feature-area-l-17-items justify-content-center text-center">
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <img
                src="assets/image/l8/habbatussauda.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Habbatussauda</h4>
              <p>
                Melancarkan pencernaan.
                <br />
                Menghilangkan sakit
                kepala.
                <br />
                Meredakan gejala asma.
                <br />
                Menjaga tekanan darah.
                <br />
                Mengatasi kolesterol
                tinggi. <br />
                Mengotrol kadar gula
                darah dalam tubuh.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features single-border position-relative">
              <img
                src="assets/image/l8/delima.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Delima</h4>
              <p>
                Mencegah penyakit
                jantung.
                <br />
                Mencegah kanker. <br />
                Mengurangi kolesterol
                jahat. <br />
                Mengurangi risiko
                penyakit radang sendi.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
            <img
                src="assets/image/l8/buahtin.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Buah Tin</h4>
              <p>
                Menyehatkan pencernaan.{" "}
                <br />
                Mencegah kanker. <br />
                Menjaga kesehatan
                jantung. <br />
                Membantu mengontrol gula
                darah. <br />
                Meningkatkan kepadatan
                tulang. <br />
                Mengatasi disfungsi
                ereksi. <br />
                Menjaga kesehatan kulit
                dan rambut.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
            <img
                src="assets/image/l8/zafaran.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Za'faran</h4>
              <p>
                Antioksidan. <br />
                Memperbaiki suasana hati
                dan gejala depresi.{" "}
                <br />
                Potensi melawan kanker.{" "}
                <br />
                Mengurangi gejala
                premenstrual syndrome
                (PMS). <br />
                Meningkatkan gairah
                seksual. <br />
                Mengurangi nafsu makan.
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
            <img
                src="assets/image/l8/maduyaman.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Madu Yaman</h4>
              <p>
                Mengobati liver. <br />
                Memperkuat jantung.{" "}
                <br />
                Mengatasi masalah di
                perut seperti : asam
                lambung, mag, diare.{" "}
                <br />
                Memperbagus tensi darah.{" "}
                <br />
                Meningkatkan hemoglobin.{" "}
                <br />
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-9">
            <div className="single-features">
            <img
                src="assets/image/l8/kurmaajwa.png"
                alt=""
                class="w-50 mb-3"
              />
              <h4>Kurma Ajwa</h4>
              <p>
                Berpotensi mencegah
                tumor. <br />
                Melancarkan saluran
                pencernaan. <br />
                Meningkatkan daya tahan
                tubuh. <br />
                Menstabilkan tekanan
                darah. <br />
                Menjaga kesehatan
                jantung. <br />
                Mencegah Diabetes.{" "}
                <br />
                Meningkatkan Fertilitas.{" "}
                <br />
                Membantu ibu hamil dan
                menyusui.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureArea;
