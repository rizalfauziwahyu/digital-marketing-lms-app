<?php

namespace App\Traits;

trait Authorization {
    protected function isSuperAdmin(): bool
    {
        return auth()->user()->hasRole('Super Admin');
    }

    protected function isStaff(): bool
    {
        return auth()->user()->hasRole('Staff');
    }

    protected function isMember(): bool
    {
        return auth()->user()->hasRole('Member');
    }
}