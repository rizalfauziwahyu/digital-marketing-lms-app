<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements JWTSubject, HasMedia
{
    use HasFactory, Notifiable, HasRoles, InteractsWithMedia;

    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'whatsapp',
        'status',
        'address',
        'approved'
    ];

    protected $visible = [
        'id',
        'name',
        'email',
        'username',
        'whatsapp',
        'status',
        'address',
        'approved',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFiltered(array $filters)
    {
        return $this->when(array_key_exists('offset', $filters), function ($q) use ($filters) {
            $q->offset($filters['offset'])->limit($filters['limit']);
        })->get();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function contents(): HasMany
    {
        return $this->hasMany(Content::class, 'author_id', 'id');
    }

    public function modules(): HasMany
    {
        return $this->hasMany(LearningModule::class, 'author_id', 'id');
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class, 'author_id', 'id');
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'orders', 'product_id', 'user_id')
            ->withPivot('quantity', 'total')
            ->withTimestamps();
    }

    public function classroomMembers(): BelongsToMany
    {
        return $this->belongsToMany(Classroom::class, 'classroom_members', 'classroom_id', 'user_id')
            ->withTimestamps();
    }

    public function eventRegistrations(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'event_registrations')
            ->withTimestamps();
    }
}
