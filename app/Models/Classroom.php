<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Classroom extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'slug',
        'title',
        'description',
    ];

    protected $visible = [
        'title',
        'description',
    ];

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function modules(): BelongsToMany
    {
        return $this->belongsToMany(LearningModule::class, 'classroom_modules');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'classroom_members')->withTimestamps();
    }

    public function getFiltered(array $filters)
    {
        return $this->when(array_key_exists('offset', $filters), function ($q) use ($filters) {
            $q->offset($filters['offset'])->limit($filters['limit']);
        })->get();
    }

    public function hasUser($userId)
    {
        return $this->users()->where('user_id', $userId)->exists();
    }

    public function setTitleAttribute(string $title): void
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title . '-' . Str::random(5));
    }
}
