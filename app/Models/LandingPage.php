<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class LandingPage extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'title',
        'url',
        'description',
        'is_published',
        'author_id',
    ];

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function setTitleAttribute(string $title): void
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title . '-' . Str::random(5));
    }

    public function getFiltered(array $filters)
    {
        return $this->when(array_key_exists('offset', $filters), function ($q) use ($filters) {
            $q->offset($filters['offset'])->limit($filters['limit']);
        })->get();
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }
}
