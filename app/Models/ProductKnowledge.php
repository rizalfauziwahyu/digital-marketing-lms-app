<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class ProductKnowledge extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $table = 'product_knowledges';

    protected $fillable = [
        'title',
        'content',
        'video',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setTitleAttribute(string $title): void
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title . '-' . Str::random(5));
    }
}
