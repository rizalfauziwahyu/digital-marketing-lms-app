<?php

namespace App\Http\Requests\ProductKnowledge;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string|max:255',
            'attachment' => 'sometimes|mimes:doc,docx,xls,xlsx,csv,pdf,png|max:25600',
        ];
    }
}
