<?php

namespace App\Http\Requests\ProductKnowledge;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|string|max:255',
            'content' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string|max:255',
            'attachment' => 'sometimes|string|max:255',
        ];
    }
}
