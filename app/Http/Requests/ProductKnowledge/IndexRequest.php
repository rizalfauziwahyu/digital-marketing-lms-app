<?php

namespace App\Http\Requests\ProductKnowledge;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'sometimes|integer',
            'offset' => 'sometimes|integer'
        ];
    }
}
