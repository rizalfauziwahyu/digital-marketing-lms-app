<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class UpdateRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|string|max:255',
            'content' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string',
            'videofile' => 'sometimes|mimes:mp4|max:25600',
            'download_link' => 'sometimes|string',
            'is_published' => 'sometimes|boolean',
            'content_category_id' => 'sometimes|integer'
        ];
    }
}
