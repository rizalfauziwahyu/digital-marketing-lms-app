<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class StoreRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string',
            'videofile' => 'sometimes|mimes:mp4|max:25600',
            'download_link' => 'sometimes|string',
            'is_published' => 'sometimes|boolean',
            'content_category_id' => 'required|integer'
        ];
    }
}
