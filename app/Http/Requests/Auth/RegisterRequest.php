<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'username' => 'required|string|max:50|unique:users,username',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required',
            'changepassword' => 'sometimes|required',
            'whatsapp' => 'sometimes|string',
            'status' => 'sometimes|string'
        ];
    }

    public function passedValidation()
    {
        $this->merge(['password' => Hash::make($this->input('password'))]);
    }
}
