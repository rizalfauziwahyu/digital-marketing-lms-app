<?php

namespace App\Http\Requests\LandingPage;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class StoreRequest extends FormRequest
{
    use Authorization;
    public function authorize()
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'sometimes|string',
            'url' => 'required|string',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'is_published' => 'sometimes|boolean',
        ];
    }
}
