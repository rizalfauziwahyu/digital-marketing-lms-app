<?php

namespace App\Http\Requests\LandingPage;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class DestroyRequest extends FormRequest
{
    use Authorization;
    public function authorize()
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            
        ];
    }
}
