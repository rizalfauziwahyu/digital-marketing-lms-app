<?php

namespace App\Http\Requests\LandingPage;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class UpdateRequest extends FormRequest
{
    use Authorization;
    public function authorize()
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|string|max:255',
            'description' => 'sometimes|string',
            'url' => 'sometimes|string',
            'is_published' => 'sometimes|boolean',
        ];
    }
}
