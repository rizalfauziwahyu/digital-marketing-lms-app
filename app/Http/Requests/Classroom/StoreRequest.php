<?php

namespace App\Http\Requests\Classroom;

use App\Traits\ApiResponser;
use App\Traits\Authorization;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    use Authorization, ApiResponser;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'moduleList' => 'sometimes|string'
        ];
    }
}
