<?php

namespace App\Http\Requests\Classroom;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class UpdateRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }


    public function rules(): array
    {
        return [
            'title' => 'sometimes|string|max:255',
            'description' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'moduleList' => 'sometimes|array'
        ];
    }
}
