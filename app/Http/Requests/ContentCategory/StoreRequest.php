<?php

namespace App\Http\Requests\ContentCategory;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class StoreRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'description' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024'
        ];
    }
}
