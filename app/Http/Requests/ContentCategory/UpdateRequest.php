<?php

namespace App\Http\Requests\ContentCategory;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class UpdateRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'name' => 'sometimes|string|max:100',
            'description' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png,jpg|max:1024'
        ];
    }
}
