<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => 'required|string',
            'changepassword' => 'required|string',
        ];
    }

    public function passedValidation()
    {
        $this->merge(['password' => Hash::make($this->input('password'))]);
    }
}
