<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class IndexRequest extends FormRequest
{
    use Authorization;
    
    public function authorize()
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules()
    {
        return [
            'limit' => 'sometimes|integer',
            'offset' => 'sometimes|integer'
        ];
    }
}
