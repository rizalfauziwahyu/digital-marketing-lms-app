<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string|max:100',
            'username' => 'sometimes|string|max:50|unique:users,username',
            'email' => 'sometimes|email|max:255|unique:users,email',
            'password' => 'sometimes',
            'whatsapp' => 'sometimes|string',
            'status' => 'sometimes|string',
            'image' => 'sometimes|mimes:jpg,jpeg,png|max:1024',
            'address' => 'sometimes|string|max:255',
            'userId' => 'sometimes',
            '_method' => 'sometimes'
        ];
    }
}
