<?php

namespace App\Http\Requests\LearningModule;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class UpdateRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules(): array
    {
        return [
            'title' => 'sometimes|string|max:255',
            'content' => 'sometimes|string|max:2048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string|max:255',
            'attachment' => 'sometimes|string|max:255',
        ];
    }
}
