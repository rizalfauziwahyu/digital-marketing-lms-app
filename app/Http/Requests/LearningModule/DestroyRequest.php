<?php

namespace App\Http\Requests\LearningModule;

use Illuminate\Foundation\Http\FormRequest;

class DestroyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->route('module')->author->id === auth()->id();
    }

    public function rules(): array
    {
        return [
            //
        ];
    }
}
