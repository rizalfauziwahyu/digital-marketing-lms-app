<?php

namespace App\Http\Requests\LearningModule;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class StoreRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'video' => 'sometimes|string|max:255',
            'attachment' => 'sometimes|mimes:doc,docx,xls,xlsx,csv,pdf,png,ppt,pptx|max:25600',
        ];
    }
}
