<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class DestroyRequest extends FormRequest
{
    public function authorize()
    {
        return $this->route('event')->author->id === auth()->id();
    }

    public function rules()
    {
        return [
            
        ];
    }
}
