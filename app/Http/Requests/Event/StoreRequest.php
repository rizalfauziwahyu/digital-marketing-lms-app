<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Authorization;

class StoreRequest extends FormRequest
{
    use Authorization;
    public function authorize(): bool
    {
        return $this->isSuperAdmin() || $this->isStaff();
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:4048',
            'image' => 'sometimes|mimes:jpeg,png|max:1024',
            'type' => 'required|string',
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'is_published' => 'sometimes',
        ];
    }
}
