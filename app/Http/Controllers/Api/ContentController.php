<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Content\DestroyRequest;
use App\Http\Requests\Content\IndexRequest;
use App\Http\Requests\Content\StoreRequest;
use App\Http\Requests\Content\UpdateRequest;
use App\Http\Resources\ContentCollection;
use App\Http\Resources\ContentResource;
use App\Models\ContentCategory;
use App\Models\Content;
use App\Models\User;

class ContentController extends Controller
{
    protected ContentCategory $category;
    protected Content $content;
    protected User $user;

    public function __construct(ContentCategory $category, Content $content, User $user)
    {
        $this->category = $category;
        $this->content = $content;
        $this->user = $user;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            $this->contentCollection($request->validated()),
            'Get data berhasil!'
        );
    }

    public function show(Content $content)
    {
        return $this->successResponse(
            $this->contentResource($content),
        );
    }

    public function download(Content $content)
    {
        return $content->getFirstMedia('image');
    }

    public function downloadVideo(Content $content)
    {
        return $content->getFirstMedia('videofile');
    }

    public function store(Request $request)
    {
        $content = auth()->user()->contents()->create($request->all());
        if ($request->hasFile('image')) {
            $content->addMediaFromRequest('image')->toMediaCollection('image');
        }

        if ($request->hasFile('videofile')) {
            $content->addMediaFromRequest('videofile')->toMediaCollection('videofile');
        }

        return $this->successResponse(
            $this->contentResource($content),
            'Konten baru berhasil dibuat!'
        );
    }

    public function update(Content $content, UpdateRequest $request)
    {
        $content->update($request->validated());

        if ($request->hasFile('image')) {
            $content->clearMediaCollection('image');
            $content->addMediaFromRequest('image')->toMediaCollection('image');
        }

        if ($request->hasFile('videofile')) {
            $content->clearMediaCollection('videofile');
            $content->addMediaFromRequest('videofile')->toMediaCollection('videofile');
        }

        return $this->successResponse(
            $this->contentResource($content),
            'Update konten berhasil!'
        );
    }

    public function destroy(DestroyRequest $request, Content $content)
    {
        $content->delete();

        return $this->successResponse(
            null,
            'Konten berhasil dihapus!'
        );
    }

    private function contentResource(Content $content)
    {
        return new ContentResource($content->load('author', 'category'));
    }

    private function contentCollection(array $values): ContentCollection
    {
        return new ContentCollection($this->content->getFiltered($values));
    }
}
