<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LearningModule\DestroyRequest;
use App\Http\Requests\LearningModule\IndexRequest;
use App\Http\Requests\LearningModule\StoreRequest;
use App\Http\Requests\LearningModule\UpdateRequest;
use App\Http\Resources\LearningModuleCollection;
use App\Http\Resources\LearningModuleResource;
use App\Models\LearningModule;

class LearningModuleController extends Controller
{
    protected LearningModule $module;

    public function __construct(LearningModule $module)
    {
        $this->module = $module;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new LearningModuleCollection($this->module->getFiltered($request->validated()))
        );
    }

    public function show(LearningModule $module)
    {
        return $this->successResponse(
            $this->moduleResource($module)
        );
    }

    public function store(StoreRequest $request)
    {
        $module = auth()->user()->modules()->create($request->validated());
        if ($request->hasFile('image')) {
            $module->addMediaFromRequest('image')->toMediaCollection('image');
        }
        if ($request->hasFile('attachment')) {
            $module->addMediaFromRequest('attachment')->toMediaCollection('attachment');
        }

        return $this->successResponse(
            $this->moduleResource($module),
            'Modul belajar berhasil dibuat!'
        );
    }

    public function update(LearningModule $module, UpdateRequest $request)
    {
        $module->update($request->validated());
        if ($request->hasFile('image')) {
            $module->clearMediaCollection('image');
            $module->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            $this->moduleResource($module),
            'Update modul belajar berhasil!'
        );
    }

    public function destroy(LearningModule $module, DestroyRequest $request)
    {
        $module->delete();

        return $this->successResponse(
            null,
            'Modul belajar berhasil dihapus!'
        );
    }

    private function moduleResource(LearningModule $module)
    {
        return new LearningModuleResource($module->load('author'));
    }
}
