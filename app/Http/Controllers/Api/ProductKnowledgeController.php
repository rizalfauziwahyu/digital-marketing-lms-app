<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductKnowledge\DestroyRequest;
use Illuminate\Http\Request;
use App\Http\Requests\ProductKnowledge\IndexRequest;
use App\Http\Requests\ProductKnowledge\StoreRequest;
use App\Http\Requests\ProductKnowledge\UpdateRequest;
use App\Http\Resources\ProductKnowledgeCollection;
use App\Http\Resources\ProductKnowledgeResource;
use App\Models\ProductKnowledge;

class ProductKnowledgeController extends Controller
{
    protected ProductKnowledge $productKnowledge;

    public function __construct(ProductKnowledge $productKnowledge)
    {
        $this->productKnowledge = $productKnowledge;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new ProductKnowledgeCollection($this->productKnowledge->get())
        );
    }

    public function show(ProductKnowledge $productKnowledge)
    {
        return $this->successResponse(
            new ProductKnowledgeResource($productKnowledge)
        );
    }

    public function downloadAttachment(ProductKnowledge $productKnowledge)
    {
        return $productKnowledge->getFirstMedia('attachment');
    }

    public function store(Request $request)
    {
        $productKnowledge = $this->productKnowledge->create($request->all());

        if ($request->hasFile('image')) {
            $productKnowledge->addMediaFromRequest('image')->toMediaCollection('image');
        }

        if ($request->hasFile('attachment')) {
            $productKnowledge->addMediaFromRequest('attachment')->toMediaCollection('attachment');
        }

        return $this->successResponse(
            new ProductKnowledgeResource($productKnowledge),
            'Product knowledge berhasil dibuat!'
        );
    }

    public function update(ProductKnowledge $productKnowledge, UpdateRequest $request)
    {
        $productKnowledge->update($request->validated());

        if ($request->hasFile('image')) {
            $productKnowledge->clearMediaCollection('image');
            $productKnowledge->addMediaFromRequest('image')->toMediaCollection('image');
        }

        if ($request->hasFile('attachment')) {
            $productKnowledge->clearMediaCollection('attachment');
            $productKnowledge->addMediaFromRequest('attachment')->toMediaCollection('attachment');
        }

        return $this->successResponse(
            new ProductKnowledgeResource($productKnowledge),
            'Update product knowledge berhasil!'
        );
    }

    public function destroy(ProductKnowledge $productKnowledge, DestroyRequest $request)
    {
        $productKnowledge->delete();

        return $this->successResponse(
            null,
            'Product knowledge berhasil dihapus!'
        );
    }
}
