<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LandingPage\DestroyRequest;
use App\Http\Requests\LandingPage\IndexRequest;
use App\Http\Requests\LandingPage\StoreRequest;
use App\Http\Requests\LandingPage\UpdateRequest;
use App\Http\Resources\LandingPageCollection;
use App\Http\Resources\LandingPageResource;
use App\Models\LandingPage;

class LandingPageController extends Controller
{
    protected LandingPage $landingPage;

    public function __construct(LandingPage $landingPage)
    {
        $this->landingPage = $landingPage;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new LandingPageCollection($this->landingPage->getFiltered($request->validated())),
            'get data berhasil!'
        );
    }

    public function show(LandingPage $landingPage)
    {
        return $this->successResponse(
            new LandingPageResource($landingPage)
        );
    }

    public function store(StoreRequest $request)
    {
        $landingPage = $this->landingPage->create($request->validated());

        if ($request->hasFile('image')) {
            $landingPage->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            new LandingPageResource($landingPage),
            'Landing Page berhasil dibuat!'
        );
    }

    public function update(LandingPage $landingPage, UpdateRequest $request)
    {
        $landingPage->update($request->validated());

        if ($request->hasFile('image'))
        {
            $landingPage->clearMediaCollection('image');
            $landingPage->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            new LandingPageResource($landingPage),
            'Update landing page berhasil!'
        );
    }

    public function destroy(LandingPage $landingPage, DestroyRequest $request)
    {
        $landingPage->delete();

        return $this->successResponse(
            null,
            'Landing page berhasil dihapus!'
        );
    }
}
