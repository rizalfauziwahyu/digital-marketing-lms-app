<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\User\DestroyRequest;
use App\Http\Requests\User\IndexRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Http\Resources\ClassroomCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
    protected User $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new UserCollection($this->user->getFiltered(($request->validated()))),
            'get data berhasil!'
        );
    }

    public function show(User $user)
    {
        return $this->successResponse(
            new UserResource($user)
        );
    }

    public function store(RegisterRequest $request)
    {
    }

    public function updateProfile(User $user, Request $request)
    {
        $user->update($request->all());

        if ($request->hasFile('image')) {
            $user->clearMediaCollection('image');
            $user->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            new UserResource($user),
            'Update profile berhasil!'
        );
    }

    public function approveUser(User $user, DestroyRequest $request)
    {
        $user->update([
            'approved' => true
        ]);

        $sendMessage = Http::post('https://api.watzap.id/v1/send_message', [
            'api_key' => env('WHATZAP_APIKEY'),
            'number_key' => env('WHATZAP_NUMBERKEY'),
            'phone_no' => $user->whatsapp,
            'message' => 'Selamat, akun anda ' . $user->email . ' sudah terverifikasi. Silahkan mengunjungi https://ufaceskincare.com untuk login dan menikmati fitur digital marketing yang kami sediakan.'
        ]);

        return $this->successResponse(
            'Pesan whatsapp berhasil terkirim',
            $sendMessage->json()
        );
    }

    public function updatePassword(User $user, UpdatePasswordRequest $request)
    {
        auth()->user()->update($request->all());

        return $this->successResponse(
            new UserResource($user),
            'Update password berhasil!'
        );
    }

    public function destroy(User $user, DestroyRequest $request)
    {
        $user->delete();

        return $this->successResponse(
            null,
            'User berhasil dihapus!'
        );
    }

    public function myProfile()
    {
        return $this->successResponse(
            new UserResource(auth()->user()),
        );
    }

    public function myClassrooms()
    {
        return $this->successResponse(
            new ClassroomCollection(auth()->user()->classroomMembers()->get()),
        );
    }

    public function getUserByUsername(String $username)
    {
        $user = User::where('username', $username)->get()->first();

        return $this->successResponse((new UserResource($user)
        ));
    }
}
