<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Event\DestroyRequest;
use App\Http\Requests\Event\IndexRequest;
use App\Http\Requests\Event\StoreRequest;
use App\Http\Requests\Event\UpdateRequest;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventResource;
use App\Models\Event;

class EventController extends Controller
{
    protected Event $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new EventCollection($this->event->getFiltered($request->validated()))
        );
    }

    public function show(Event $event)
    {
        return $this->successResponse(
            $this->eventResource($event)
        );
    }

    public function store(StoreRequest $request)
    {
        $event = auth()->user()->events()->create($request->validated());
        if ($request->hasFile('image')) {
            $event->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            $this->eventResource($event),
            'Event berhasil dibuat!'
        );
    }

    public function update(Event $event, UpdateRequest $request)
    {
        $event->update($request->validated());
        if ($request->hasFile('image')) {
            $event->clearMediaCollection('image');
            $event->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            $this->eventResource($event),
            'Update event berhasil!'
        );
    }

    public function destroy(Event $event, DestroyRequest $request)
    {
        $event->delete();

        return $this->successResponse(
            null,
            'Event berhasil dihapus!'
        );
    }

    public function registerEvent(Event $event)
    {
        $event->eventRegistrations()->attach(auth()->id());
        
        return $this->successResponse(
            null,
            'Pendaftaran berhasil!'
        );
    }

    private function eventResource(Event $event)
    {
        return new EventResource($event->load('author', 'eventRegistrations'));
    }
}
