<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Classroom\DestroyRequest;
use App\Http\Requests\Classroom\IndexRequest;
use App\Http\Requests\Classroom\StoreRequest;
use App\Http\Requests\Classroom\UpdateRequest;
use App\Http\Resources\ClassroomCollection;
use App\Http\Resources\ClassroomResource;
use App\Models\Classroom;
use App\Services\ClassroomService;

class ClassroomController extends Controller
{
    protected Classroom $classroom;
    protected ClassroomService $classroomService;

    public function __construct(Classroom $classroom, ClassroomService $classroomService)
    {
        $this->classroom = $classroom;
        $this->classroomService = $classroomService;
    }

    public function index(IndexRequest $request)
    {
        return $this->successResponse(
            new ClassroomCollection($this->classroom->getFiltered($request->validated())),
            'get data berhasil!'
        );
    }

    public function show(Classroom $classroom)
    {
        return $this->successResponse(
            $this->classroomResource($classroom)
        );
    }

    public function store(StoreRequest $request)
    {
        $classroom = $this->classroom->create($request->validated());
        if ($request->hasFile('image')) {
            $classroom->addMediaFromRequest('image')->toMediaCollection('image');
        }

        $this->syncModules($classroom, $request->safe()->only(['moduleList']));

        return $this->successResponse(
            $this->classroomResource($classroom),
            'Ruang kelas berhasil dibuat!'
        );
    }

    public function update(Classroom $classroom, UpdateRequest $request)
    {
        return $request->validated();
        $classroom->update($request->validated());
        if ($request->hasFile('image')) {
            $classroom->clearMediaCollection('image');
            $classroom->addMediaFromRequest('image')->toMediaCollection('image');
        }

        if ($request->safe()->only(['moduleList'])) {
            $this->syncModules($classroom, $request->safe()->only(['moduleList']) ?? []);
        }


        return $this->successResponse(
            $this->classroomResource($classroom),
            'Update ruang kelas berhasil!'
        );
    }

    public function destroy(Classroom $classroom, DestroyRequest $request)
    {
        $classroom->delete();

        return $this->successResponse(
            null,
            'Ruang kelas berhasil dihapus!'
        );
    }

    public function enroll(Classroom $classroom)
    {
        $classroom->users()->attach(auth()->id());

        return $this->successResponse(
            $this->classroomResource($classroom),
            'Berhasil mengikuti kelas!'
        );
    }

    private function classroomResource(Classroom $classroom)
    {
        return new ClassroomResource($classroom->load('modules'));
    }

    private function syncModules(Classroom $classroom, array $modules): void
    {
        $this->classroomService->syncModules($classroom, $modules);
    }
}
