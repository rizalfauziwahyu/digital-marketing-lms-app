<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContentCategoryResource;
use Illuminate\Http\Request;
use App\Http\Requests\ContentCategory\StoreRequest;
use App\Http\Requests\ContentCategory\UpdateRequest;
use App\Http\Resources\ContentCategoryCollection;
use App\Http\Resources\ContentCollection;
use App\Models\ContentCategory;

class ContentCategoryController extends Controller
{
    protected ContentCategory $category;

    public function __construct(ContentCategory $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        return $this->successResponse(
            new ContentCategoryCollection($this->category->get()),
        );
    }

    public function show(ContentCategory $category)
    {
        return $this->successResponse(
            new ContentCategoryResource($category)
        );
    }

    public function getContentsByCategory(ContentCategory $category)
    {
        return $this->successResponse(
            new ContentCollection($category->contents()->get())
        );
    }

    public function store(StoreRequest $request)
    {
        $category = $this->category->create($request->validated());
        if ($request->hasFile('image')) {
            $category->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            new ContentCategoryResource($category),
            'Kategori konten berhasil dibuat!'
        );
    }

    public function update(ContentCategory $category, UpdateRequest $request)
    {
        $category->update($request->validated());
        if ($request->hasFile('image')) {
            $category->clearMediaCollection('image');
            $category->addMediaFromRequest('image')->toMediaCollection('image');
        }

        return $this->successResponse(
            new ContentCategoryResource($category),
            'Update kategori konten berhasil!'
        );
    }

    public function destroy(ContentCategory $category, Request $request)
    {
        $category->delete();

        return $this->successResponse(
            null,
            'Kategori konten berhasil dihapus!'
        );
    }
}
