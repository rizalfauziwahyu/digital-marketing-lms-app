<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class AuthController extends Controller
{
    protected User $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(LoginRequest $request)
    {

        if ($token = AUTH::attempt($request->validated())) {
            $userData  = $this->userData($token);
            if (!$userData['user']['approved'])
                return $this->errorResponse('Akun anda tidak aktif, anda akan mendapatkan pemberitahuan aktifasi akun melalui whatsapp jika anda terverifikasi sebagai member BOB!', 422);

            return $this->successResponse(
                $userData,
                'Login berhasil!'
            );
        }

        return $this->errorResponse('Email atau password salah!', 401);
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->user->create($request->all());
        if ($user)
            $user->assignRole('Member');

        return $this->successResponse(
            'Pendaftaran berhasil!'
        );
    }

    public function refresh()
    {
        return $this->successResponse($this->userData(AUTH::refresh()));
    }

    private function userData(string $jwtToken): array
    {
        return ['user' => ['token' => $jwtToken] + AUTH::user()->toArray()];
    }
}
