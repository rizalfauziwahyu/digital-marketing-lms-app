<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ContentCategoryCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'categories' => $this->collection
        ];
    }
}
