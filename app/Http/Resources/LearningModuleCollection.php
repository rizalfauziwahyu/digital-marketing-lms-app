<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LearningModuleCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'modules' => $this->collection,
            'modulesCount' => $this->count()
        ];
    }
}
