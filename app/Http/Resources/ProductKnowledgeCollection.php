<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductKnowledgeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'productKnowledges' => $this->collection,
            'modulesKnowledgesCount' => $this->count()
        ];
    }
}
