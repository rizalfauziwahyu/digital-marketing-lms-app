<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductKnowledgeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'content' => $this->content,
            'image' => $this->getFirstMediaUrl('image'),
            'video' => $this->video,
            'attachment' => [
                'url' => $this->getFirstMediaUrl('attachment'),
                'filename' => $this->getFirstMedia('attachment') ? $this->getFirstMedia('attachment')->file_name : null,
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
