<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LandingPageCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'landingPages' => $this->collection,
            'landingPagesCount' => $this->collection->count(),
        ];
    }
}
