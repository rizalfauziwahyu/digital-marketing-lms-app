<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EventCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'events' => $this->collection,
            'eventCounts' => $this->count(),
        ];
    }
}
