<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LearningModuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'content' => $this->content,
            'image' => $this->getFirstMediaUrl('image'),
            'video' => $this->video,
            'attachment' => [
                'url' => $this->getFirstMediaUrl('attachment'),
                'filename' => $this->getFirstMedia('attachment') ? $this->getFirstMedia('attachment')->file_name : null,
            ],
            'author' => $this->author->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
