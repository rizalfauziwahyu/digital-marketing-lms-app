<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassroomResource extends JsonResource
{
    public static $wrap = 'classroom';
    public function toArray($request): array
    {
        return [
            'slug' => $this->slug,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->getFirstMediaUrl('image'),
            'moduleList' => $this->modules->map(function ($module) {
                return [
                    'title' => $module->title,
                    'slug' => $module->slug
                ];
            }),
            'enrolled' => $this->hasUser(auth()->id()),
        ];
    }
}
