<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'slug' => $this->slug,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->getFirstMediaUrl('image'),
            'type' => $this->type,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'date' => $this->date,
            'is_published' => $this->is_published,
            'author' => $this->author->name,
            'registered' => $this->hasUser(auth()->id()),
            'registrants' => [
                'user' => $this->eventRegistrations->pluck('name'),
                'total' => $this->eventRegistrations->count(),
            ]
        ];
    }
}
