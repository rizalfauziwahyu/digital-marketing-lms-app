<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    public static $wrap = 'content';
    public function toArray($request)
    {
        return [
            'slug' => $this->slug,
            'title' => $this->title,
            'content' => $this->content,
            'category' => $this->category->name,
            'image' => [
                'url' => $this->getFirstMediaUrl('image'),
                'filename' => $this->getFirstMedia('image') ? $this->getFirstMedia('image')->file_name : null,
            ],
            'video' => $this->video,
            'videofile' =>  $this->getFirstMedia('videofile') ? $this->getFirstMedia('videofile')->file_name : null,
            'download_link' => $this->download_link,
            'is_published' => $this->is_published ? "Ya" : "Tidak",
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author' => $this->author->name,
        ];
    }
}
