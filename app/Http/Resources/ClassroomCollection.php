<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClassroomCollection extends ResourceCollection
{
    public static $wrap = '';
    public function toArray($request): array
    {
        return [
            'classrooms' => $this->collection,
            'classroomCounts' => $this->count()
        ];
    }
}
