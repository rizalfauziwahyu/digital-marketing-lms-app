<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ContentCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'contents' => $this->collection,
            'contentsCount' => $this->count()
        ];
    }
}
