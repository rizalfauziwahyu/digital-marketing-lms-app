<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        $response = $this->handleException($request, $exception);

        return $response;
    }

    public function handleException($request, Throwable $exception)
    {
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse(
                'This action is denied!',
                403
            );
        } else if ($exception instanceof JWTException) {
            return $this->errorResponse(
                'Token is not provided',
                500
            );
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse(
                'The specified method for the request is invalid',
                405
            );
        } else if ($exception instanceof NotFoundHttpException)
        {
            return $this->errorResponse(
                'The specified URL cannot be found',
                404
            );
        } else if ($exception instanceof HttpException)
        {
            return $this->errorResponse(
                $exception->getMessage(),
                $exception->getStatusCode()
            );
        } else if (config('app.debug')) {
            return parent::render($request, $exception);
        }

        return $this->errorResponse(
            'Unexpected Exception. Try Later',
            500
        );
    }
}
