<?php
namespace App\Services;

use App\Models\Classroom;
use App\Models\LearningModule;

class ClassroomService
{
    protected Classroom $classroom;
    protected LearningModule $module;

    public function __construct(Classroom $classroom, LearningModule $module)
    {
        $this->classroom = $classroom;
        $this->module = $module;
    }

    public function syncModules(Classroom $classroom, array $modules)
    {
        $classroom->modules()->sync(json_decode($modules['moduleList']) ?? []);
    }
}